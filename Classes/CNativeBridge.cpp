//
//  CNativeBridge.cpp
//  FlConv
//
//  Created by shogo yanuma on 2014/04/27.
//
//

#include "CNativeBridge.h"
#include "CocosUtil.h"
#include "PDDictionary.h"
#include "PDValue.h"
#include "ServiceModel.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#import "NativeBridge_ios.h"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#import "NativeBridge_mac.h"
#endif

USING_NS_CC;

CNativeBridge *g_CNativeBridge_instance = NULL;

//static
CNativeBridge *CNativeBridge::getInstance()
{
	if (g_CNativeBridge_instance == NULL) {
		g_CNativeBridge_instance = CNativeBridge::create();
	}
	return g_CNativeBridge_instance;
}

CNativeBridge::CNativeBridge()
{
}

CNativeBridge::~CNativeBridge()
{
}

bool CNativeBridge::init()
{
	return true;
}

std::string CNativeBridge::getDeviceData(ENUM_DEVICE_DATA_TYPE type, const std::string &param)
{
	std::string ret = "";

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

	ret = __getDeviceData_iOS(type, param);
//	NSString *p = [NSString stringWithUTF8String:param.c_str()];
//	NSString *str = [NativeBridge_ios getDeviceData:type param:p];
//	if (str != nil) {
//		ret = [str UTF8String];
//	}
	
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)

	ret = __getDeviceData_mac(type, param);
//	NSString *p = [NSString stringWithUTF8String:param.c_str()];
//	NSString *str = [NativeBridge_mac getDeviceData:type param:p];
//	if (str != nil) {
//		ret = [str UTF8String];
//	}
	
#endif
	
	return ret;
}

void CNativeBridge::callDeviceFunc(ENUM_DEVICE_FUNC func, const std::string &param)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

	__callDeviceFunc_iOS(func, param);
//	NSString *p = [NSString stringWithUTF8String:param.c_str()];
//	[NativeBridge_ios callDeviceFunc:func param:p];

#elif (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)

	__callDeviceFunc_mac(func, param);
//	NSString *p = [NSString stringWithUTF8String:param.c_str()];
//	[NativeBridge_mac callDeviceFunc:func param:p];

#endif
}

std::string CNativeBridge::getCPPData(ENUM_CPP_DATA_TYPE type, const std::string &param)
{
	return "";
}

void CNativeBridge::callCPPFunc(ENUM_CPP_FUNC func, const std::string &param)
{
//	cocos2d::Ref *obj = CocosUtil::jsonStringToCocosObject(param);
	Json::Value jsonVal = CocosUtil::jsonStringToJsonValue( param );
	switch (func)
	{
		case kCPPFunc_URL_RESPONSE: {
			int tag = jsonVal["tag"].asInt();
			bool success = jsonVal["success"].asBool();
			std::string response = jsonVal["response"].asString();
//			_urlResponse( tag, success, response );
			if (success) {
				ServiceModel::getInstance()->onSuccessRequest(tag, response);
			}
			else {
				ServiceModel::getInstance()->onFailRequest(tag, response);
			}
		} break;

		default:
			break;
	}
}

//PDArray *CNativeBridge::enumlateDirectoryItems(const std::string &path)
//{
//	PDArray *ret = PDArray::create();
//
//	std::string param = StringUtils::format("{\"path\":\"%s\", \"recursive\":0}", path.c_str());
//	std::string res = getDeviceData(kDeviceDataType_GET_DIR_FILES, param);
//
//	PDArray *arr = (PDArray*)CocosUtil::jsonStringToCocosObject(res);
//	for (int i=0; i<arr->count(); i++)
//	{
//		PDDictionary *dic = (PDDictionary*)arr->objectAtIndex(i);
//		std::string type = dic->valueAsString("type");
//		int size = dic->valueAsInt("size");
//		std::string name = dic->valueAsString("name");
//		
//		CFileInfoVO *vo = CFileInfoVO::create();
//		vo->set_name( name );
//		vo->set_size( size );
//		if (type == "file") {
//			vo->set_type( CFileInfoVO::kInfoType_FILE );
//		}
//		else if (type == "dir") {
//			vo->set_type( CFileInfoVO::kInfoType_DIRECTORY );
//		}
//		else {
//			CCLOG("WARN: unknown directory item type. - %s", type.c_str());
//		}
//
//		ret->addObject(vo);
//	}
//	
//	return ret;
//}

//void CNativeBridge::_urlResponse(int tag, bool success, const std::string &response)
//{
//	
//}
