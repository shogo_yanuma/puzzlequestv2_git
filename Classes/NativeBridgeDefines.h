//
//  NativeBridgeDefines.h
//  FlConv
//
//  Created by shogo yanuma on 2014/04/27.
//
//

#ifndef FlConv_NativeBridgeDefines_h
#define FlConv_NativeBridgeDefines_h

typedef enum {
	kDeviceDataType_UNKNOWN	= -1,
	kDeviceDataType_APP_DIR,
	kDeviceDataType_RESOURCE_DIR,
	kDeviceDataType_DOC_DIR,
	kDeviceDataType_CACHE_DIR,
	kDeviceDataType_TEMP_DIR,
	kDeviceDataType_GET_DIR_FILES,
		// param = { "path":"", "recursive":0/1 }
		// return [{"type":"file", "name":"xxx.png", "size":100, "createtime":0, "updatetime":0}, {"type":"dir", "name":"directory", "createtime":0, "updatetime":0 }, ...]
	kDeviceDataType_READ_FILE,
		// param = { "path":"" }
		// return { "success":true/false, "data":"xxx" }
	kDeviceDataType_WRITE_FILE,
		// param = { "path":"", "data":"xxx" }
		// return { "success":true/false }
} ENUM_DEVICE_DATA_TYPE;


typedef enum {
	kDeviceFunc_UNKNOWN		= -1,
	kDeviceFunc_URL_REQUEST,
		// parm = { "tag":int, "url":"http://xxx.com/test.php", param:"{"param1":, "param2":, ...}" }
	kDeviceFunc_MAKE_DIR,
		// param = { "path":"/path/to/create" }
	kDeviceFunc_SAVE_FILE,
		// param = { "path":"/path/to/savefilename", "data":TEXT }
} ENUM_DEVICE_FUNC;


typedef enum {
	kCPPDataType_UNKNOWN	= -1,
} ENUM_CPP_DATA_TYPE;


typedef enum {
	kCPPFunc_UNKNOWN		= -1,
	kCPPFunc_URL_RESPONSE,
		// param = { "tag":int, "success":true/false, "response":"" }
} ENUM_CPP_FUNC;

#endif
