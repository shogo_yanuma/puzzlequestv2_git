//
//  StepUI.h
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/11.
//
//

#ifndef StepUI_h
#define StepUI_h

#include "AppDefines.h"

USING_NS_CC;

class StepUI : public Node
{
public:
	CC_SYNTHESIZE(ENUM_STEP_DIR, stepDir_, _stepDir);
	
public:
	CREATE_FUNC(StepUI);

	StepUI() {
		stepDir_ = kStepDir_UNKNOWN;
	}
	virtual ~StepUI() {}
	virtual bool init();
};

#endif /* StepUI_h */
