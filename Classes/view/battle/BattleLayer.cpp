//
//  BattleLayer.cpp
//  PuzzleQuest
//
//  Created by shogo yanuma on 2016/04/08.
//
//

#include "BattleLayer.h"
#include "ScreenModel.h"
#include "ResourceModel.h"
#include "CocosUtil.h"

#include "cocostudio/CocoStudio.h"
#include "cocosGUI.h"
using namespace cocostudio;
using namespace timeline;

bool BattleLayer::init()
{
	if (!Layer::init()) {
		return false;
	}

//	auto rootNode = CSLoader::createNode("battle/BattleScene.csb");
	auto rootNode = CSLoader::createNode("BattleScene.csb");
	ResourceModel::setToScreenCenter(rootNode);
	addChild(rootNode);
	
	ui::Button *btn;
	btn = (ui::Button*)(rootNode->getChildByName("header@gv_ct")->getChildByName("btnBack"));
//	ResourceModel::autoLayout(btn, kAutoLayout_TL);
	ResourceModel::autoLayout(btn, kAutoLayout_TR);

	btn->addClickEventListener([this](Ref *sender) {
		ScreenModel::getInstance()->dispHome();
	});
	
	makeDummyMusic();

	startBattle();
	
	return true;
}

void BattleLayer::makeDummyMusic()
{
	MusicInfoVO *vo = MusicInfoVO::create();

//	std::string path = ResourceModel::localFilePath("ja/data/battle.txt");
	std::string path = "ja/data/battle.txt";
	std::string json = FileUtils::getInstance()->getStringFromFile(path);
	Json::Value val = CocosUtil::jsonStringToJsonValue(json);
	vo->setData(val);

	set_musicInfo( vo );
}

void BattleLayer::startBattle()
{
//	currentPhase_ = kPhase_PLAY;
	setCurrentPhase( kPhase_READY );
	
	schedule([this](float dt){
		this->mainLoop(dt);
	}, 1.f/60.f, -1, 0, "MAIN_LOOP");
}

void BattleLayer::setCurrentPhase( ENUM_BATTLE_PHASE phase )
{
	ENUM_BATTLE_PHASE oldPhase = currentPhase_;
	if (oldPhase != phase) {
		phaseWillEnd(oldPhase);
		phasePassSec_ = 0;
		phaseDidStart( phase );
	}
	currentPhase_ = phase;
}

void BattleLayer::phaseWillEnd(ENUM_BATTLE_PHASE phase)
{
}

void BattleLayer::phaseDidStart(ENUM_BATTLE_PHASE phase)
{
	switch (phase) {
		case kPhase_READY:
			showReadyEfx();
			break;
		case kPhase_START:
			showStartEfx();
			break;
		case kPhase_PLAY:
			lastStepIndex_ = 0;
			break;
		case kPhase_CLEAR:
			showClearEfx();
			break;
		case kPhase_GAMEOVER:
			showGameoverEfx();
			break;
		default:
			break;
	}
}

void BattleLayer::mainLoop(float dt)
{
	phasePassSec_ += dt;

	//TODO: scroll and hit test.
	switch (currentPhase_)
	{
		case kPhase_READY: {
			if (phasePassSec_ > 1.0) {
				setCurrentPhase( kPhase_START );
			}
		} break;

		case kPhase_START: {
			if (phasePassSec_ > 2.0) {
				setCurrentPhase( kPhase_PLAY );
			}
		} break;

		case kPhase_PLAY: {
			// ゲームのループ
			progressMusic(dt);
		} break;

		case kPhase_CLEAR: {
		} break;
			
		case kPhase_GAMEOVER: {
		} break;

		default:
			break;
	}
}

void BattleLayer::progressMusic(float dt)
{
	//TODO: ノートグループの移動
	//TODO: 画面外に出たノートをプールに戻す
	//TODO: 新しいノートの追加

//	lastProgressSec_ += dt;
}

void BattleLayer::showReadyEfx()
{
	auto eff = CSLoader::createNode("effect/mcReady.csb");
	ActionTimeline *act = CSLoader::createTimeline("effect/mcReady.csb");
	act->setAnimationEndCallFunc("start", [this,act](){
		CCLOG("start done");
		act->play("end", false);
	});
	act->setAnimationEndCallFunc("end", [this,eff](){
		CCLOG("all done");
		eff->removeFromParentAndCleanup(true);
	});
	addChild(eff);
	eff->runAction( act );
	act->play("start", false);
}

void BattleLayer::showStartEfx()
{
}

void BattleLayer::showGameoverEfx()
{
}

void BattleLayer::showClearEfx()
{
}
