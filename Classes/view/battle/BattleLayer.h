//
//  BattleLayer.h
//  PuzzleQuest
//
//  Created by shogo yanuma on 2016/04/08.
//
//

#ifndef BattleLayer_h
#define BattleLayer_h

#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "MusicInfoVO.h"

USING_NS_CC;
using namespace cocostudio::timeline;

class BattleLayer : public Layer
{
public:
	typedef enum {
		kPhase_UNKNOWN = 0,
		kPhase_READY,
		kPhase_START,
		kPhase_PLAY,
		kPhase_GAMEOVER,
		kPhase_CLEAR,
	} ENUM_BATTLE_PHASE;
	
public:
	CC_SYNTHESIZE(ENUM_BATTLE_PHASE, currentPhase_, _currentPhase);
	CC_SYNTHESIZE(float, phasePassSec_, _phasePassSec);
	CC_SYNTHESIZE_RETAIN(MusicInfoVO*, musicInfo_, _musicInfo);

//	CC_SYNTHESIZE(float, lastProgressSec_, _lastProgressSec);
	CC_SYNTHESIZE(int, lastStepIndex_, _lastStepIndex);

public:
	CREATE_FUNC(BattleLayer);

	BattleLayer() {
		currentPhase_ = kPhase_UNKNOWN;
		musicInfo_ = NULL;
	}
	virtual ~BattleLayer() {
		set_musicInfo(NULL);
	}

	virtual bool init();

	void makeDummyMusic();
	void startBattle();

	void setCurrentPhase( ENUM_BATTLE_PHASE phase );
	void phaseWillEnd(ENUM_BATTLE_PHASE phase);
	void phaseDidStart(ENUM_BATTLE_PHASE phase);
	void mainLoop(float dt);

	void progressMusic(float dt);
	
	void showReadyEfx();
	void showStartEfx();
	void showGameoverEfx();
	void showClearEfx();
};

#endif /* BattleLayer_h */
