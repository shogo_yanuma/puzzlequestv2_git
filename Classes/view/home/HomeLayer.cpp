//
//  HomeLayer.cpp
//  PuzzleQuest
//
//  Created by shogo yanuma on 2016/04/08.
//
//

#include "HomeLayer.h"
#include "ScreenModel.h"
#include "ResourceModel.h"

#include <spine/spine-cocos2dx.h>

bool HomeLayer::init()
{
	if (!Layer::init()) {
		return false;
	}

	auto rootNode = CSLoader::createNode("home/HomeScene.csb");
	ResourceModel::setToScreenCenter(rootNode);
	addChild(rootNode);

//	Node *pos;
//	pos = rootNode->getChildByName("footer@gv_cb")->getChildByName("btnBattle");
//	ResourceModel::autoLayout(pos, kAutoLayout_ML);
	
	ui::Button *btn;
	btn = (ui::Button*)(rootNode->getChildByName("footer@gv_cb")->getChildByName("btnBattle"));
	ResourceModel::autoLayout(btn, kAutoLayout_BC);
	
	btn->addClickEventListener([this](Ref *sender) {
		ScreenModel::getInstance()->dispBattle();
	});

	
	std::string jsonPath = ResourceModel::localFilePath("ja/anim/dragon/dragon.json");
	std::string atlasPath = ResourceModel::localFilePath("ja/anim/dragon/dragon.atlas");
	CCLOG("json : %s", jsonPath.c_str());
	CCLOG("atlas : %s", atlasPath.c_str());
	auto spine = spine::SkeletonAnimation::createWithFile( jsonPath, atlasPath );
	spine->setAnimation(0, "flying", true);
	spine->setPosition(Point(320,480));
	addChild(spine);
	
	return true;
}
