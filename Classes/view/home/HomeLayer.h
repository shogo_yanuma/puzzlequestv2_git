//
//  HomeLayer.h
//  PuzzleQuest
//
//  Created by shogo yanuma on 2016/04/08.
//
//

#ifndef HomeLayer_h
#define HomeLayer_h

#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace cocostudio::timeline;

class HomeLayer : public Layer
{
public:
	CREATE_FUNC(HomeLayer);

	HomeLayer() {}
	virtual ~HomeLayer() {}

	virtual bool init();
};

#endif /* HomeLayer_h */
