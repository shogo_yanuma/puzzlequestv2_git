//
//  PopupBase.h
//  PuzzleQuest
//
//  Created by shogo yanuma on 2014/08/11.
//
//

#ifndef __PuzzleQuest__PopupBase__
#define __PuzzleQuest__PopupBase__

#include "cocos2d.h"

class PopupBase;

class PopupBaseDelegate
{
public:
	virtual bool popupWillClose(PopupBase *popup, int buttonIndex) { return true; }
	virtual void popupDidClose(PopupBase *popup, int buttonIndex) {}
};

class PopupBase : public cocos2d::Node
{
public:
	CC_SYNTHESIZE(PopupBaseDelegate*, delegate_, _delegate);
	CC_SYNTHESIZE(cocos2d::Point, centerPos_, _centerPos);
	CC_SYNTHESIZE(std::function<void (PopupBase *popup, int buttonIndex)>, callback_, _callback);
	
public:
	PopupBase() {
		delegate_ = NULL;
		callback_ = nullptr;
	}
	virtual ~PopupBase() {}
	virtual bool init() { return true; }

	virtual void closePopup(int buttonIndex, bool animated=true);

	virtual void popupWillAppear() {}
	virtual void popupDidAppear() {}
	virtual void popupWillDisappear() {}
	virtual void popupDidDisappear() {}
};

#endif /* defined(__PuzzleQuest__PopupBase__) */
