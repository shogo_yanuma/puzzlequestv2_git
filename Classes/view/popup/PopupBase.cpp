//
//  PopupBase.cpp
//  PuzzleQuest
//
//  Created by shogo yanuma on 2014/08/11.
//
//

#include "PopupBase.h"
#include "ScreenModel.h"

void PopupBase::closePopup(int buttonIndex, bool animated)
{
	if (delegate_ != NULL) {
		bool res = delegate_->popupWillClose(this, buttonIndex);
		if (!res) {
			// popupWillCloseのチェックでポップアップのクローズがキャンセルされた
			CCLOG("WARN: cancel to close popup by popupWillClose check.");
			return;
		}
	}

	this->retain();

	ScreenModel::getInstance()->closePopup(this, animated);

	if (delegate_ != NULL) {
		delegate_->popupDidClose(this, buttonIndex);
	}
	if (callback_ != nullptr) {
		callback_(this, buttonIndex);
	}

	this->autorelease();
}
