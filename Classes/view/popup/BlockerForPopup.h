//
//  BlockerForPopup.h
//  PuzzleQuest
//
//  Created by shogo yanuma on 2014/08/12.
//
//

#ifndef __PuzzleQuest__BlockerForPopup__
#define __PuzzleQuest__BlockerForPopup__

#include "cocos2d.h"

class BlockerForPopup : public cocos2d::Node
{
public:
	CREATE_FUNC(BlockerForPopup);

	BlockerForPopup() {}
	virtual ~BlockerForPopup() {}
	virtual bool init();

	virtual void onEnter();
	virtual void onExit();
};

#endif /* defined(__PuzzleQuest__BlockerForPopup__) */
