//
//  AlertPopup.cpp
//  PuzzleQuest
//
//  Created by shogo yanuma on 2014/08/11.
//
//

#include "AlertPopup.h"
#include "ResourceModel.h"
//#include "ui/CocosGUI.h"
#include "extensions/cocos-ext.h"
//#include "extensions/GUI/CCControlExtension/CCScale9Sprite.h"

#define ALERT_WIDTH			286.f*2
#define ALERT_MAX_HEIGHT	460.f*2
#define ALERT_MIN_HEIGHT	150.f*2
#define ALERT_TITLE_HEIGHT	80.f*2
#define ALERT_NO_TITLE_HEIGHT	20.f*2
#define ALERT_BUTTON_HEIGHT	40.f*2
#define ALERT_BUTTON_GAP	5.f*2
#define ALERT_WIDTH_MAGIN	20.f*2	// 左右のマージン
#define ALERT_HEIGHT_GAP	20.f*2	// ボタンとメッセージの隙間
#define ALERT_BOTTOM_MAGIN	20.f*2	// 下段のマージン

USING_NS_CC;
USING_NS_CC_EXT;

//static
AlertPopup *AlertPopup::createWith( const std::string &title, const std::string &message, PopupBaseDelegate *delegate, const std::string &cancelTitle, const char *buttonsTitle, ... )
{
	AlertPopupInfoVO *vo = AlertPopupInfoVO::create();
	vo->set_title(title);
	vo->set_message(message);

	vo->set_cancelBtnTitle( cancelTitle );

	if (buttonsTitle != NULL)
	{
		std::vector<std::string> newArr;
		
		newArr.push_back(buttonsTitle);
		
		va_list vl;
		va_start( vl, buttonsTitle );
		const char *arg = va_arg(vl, const char *);
		while (arg != NULL)
		{
			CCLOG("arg = %s", arg);
			std::string str = arg;
			newArr.push_back(str);
			arg = va_arg(vl, const char *);
			CCLOG("cnt : %d", (int)newArr.size());
		}
		
		vo->set_buttonsTitle(newArr);
		
		va_end( vl );
	}
	
	AlertPopup *popup = AlertPopup::create();
	popup->set_delegate(delegate);
	popup->setPopupInfo(vo);
	return popup;
}

//static
AlertPopup *AlertPopup::createWith( const std::string &title, const std::string &message, std::function<void(PopupBase *popup, int buttonIndex)> callback, const std::string &cancelTitle, const char *buttonsTitle, ... )
{
	AlertPopupInfoVO *vo = AlertPopupInfoVO::create();
	vo->set_title(title);
	vo->set_message(message);
	
	vo->set_cancelBtnTitle( cancelTitle );
	
	if (buttonsTitle != NULL)
	{
		std::vector<std::string> newArr;
		
		newArr.push_back(buttonsTitle);
		
		va_list vl;
		va_start( vl, buttonsTitle );
		const char *arg = va_arg(vl, const char *);
		while (arg != NULL)
		{
			CCLOG("arg = %s", arg);
			std::string str = arg;
			newArr.push_back(str);
			arg = va_arg(vl, const char *);
			CCLOG("cnt : %d", (int)newArr.size());
		}
		
		vo->set_buttonsTitle(newArr);
		
		va_end( vl );
	}
	
	AlertPopup *popup = AlertPopup::create();
	popup->set_callback(callback);
	popup->setPopupInfo(vo);
	return popup;
}

bool AlertPopup::init()
{
	// ポップアップの中心位置を指定する
	set_centerPos( Point(ALERT_WIDTH/2, ALERT_MIN_HEIGHT/2) );

	set_buttons( PDArray::create() );
	return true;
}

void AlertPopup::setPopupInfo(AlertPopupInfoVO *vo)
{
	set_infoVO(vo);
//	_requildLayout();

	_buildLayout();
	
	lblTitle_->setString( vo->get_title().c_str() );
	lblMessage_->setString( vo->get_message().c_str() );
}

PDArray *AlertPopup::allButtons()
{
	PDArray *ret = PDArray::create();
	if (btnCancel_) {
		ret->addObject(btnCancel_);
	}
	ret->addObjects(buttons_);
	return ret;
}

//void AlertPopup::_buttonCallback(cocos2d::Ref* obj)
void AlertPopup::_buttonCallback(cocos2d::Ref *sender, cocos2d::ui::Widget::TouchEventType ev)
{
	int buttonIndex = 0;
	PDArray *arr = allButtons();
	for (int i=0; i<arr->count(); i++) {
//		CCButtonExt *btn = (CCButtonExt*)arr->objectAtIndex(i);
		ui::Button *btn = (ui::Button*)arr->objectAtIndex(i);
//		if (btn == obj) {
		if (btn == sender) {
			buttonIndex = i;
			break;
		}
	}
	CCLOG("_buttonCallback - buttonIndex:%d", buttonIndex);
	
//	popupButtonDidPress( buttonIndex );
//	closePopup();
	closePopup(buttonIndex, true);
}

void AlertPopup::_buildLayout()
{
	this->removeAllChildren();
	
	AlertPopupInfoVO *vo = infoVO_;
	
	
	Label *lbl;
	
	lbl = ResourceModel::boldLabelTTF(vo->get_title(), 34);
	lbl->setAnchorPoint(Point(0.5, 0.5));
	lbl->setColor(Color3B(0xc4, 0x83, 0x83));
	//	lbl->setFontFillColor(ccc3(0xc4, 0x83, 0x83));
	set_lblTitle(lbl);
	
	lbl = ResourceModel::defaultLabelTTF(vo->get_message(), 30);
	lbl->setVerticalAlignment(kCCVerticalTextAlignmentCenter);
//	lbl->setVerticalAlignment(TextHAlignment::CENTER);
	lbl->setColor(Color3B(0x2f, 0x2f, 0x2f));
	//	lbl->setFontFillColor(ccc3(0x2f, 0x2f, 0x2f));
//	((LabelTTF*)lbl)->setDimensions(CCSizeMake(ALERT_WIDTH-2*ALERT_WIDTH_MAGIN, 0));
	lbl->setDimensions(ALERT_WIDTH-2*ALERT_WIDTH_MAGIN, 0);
	set_lblMessage(lbl);
	
	
	// voのボタンやテキストからレイアウトを作る
	
	// ボタンの数による高さ
	int buttonsHeight = 0;
	LayerColor *buttonsBase = LayerColor::create( Color4B(0xff,0,0,0), 30, 60 );
	PDArray *buttons = PDArray::create();
	{
		std::vector<std::string> titles;
		if (vo->get_cancelBtnTitle()!="") {
			titles.push_back( vo->get_cancelBtnTitle() );
		}
		for (int i=0; i<vo->get_buttonsTitle().size(); i++) {
			titles.push_back( vo->get_buttonsTitle()[i] );
		}
		int btnCount = titles.size();
		
		int btnWidth = 0;
		bool isVertical = false;
		
		int buttonSX = 0;
		int buttonSY = 0;
		
		if (btnCount <= 2) {
			// ボタンが2つ以下(横並び)
			btnWidth = (ALERT_WIDTH-2*ALERT_WIDTH_MAGIN) / btnCount;
			isVertical = false;
			buttonsHeight = ALERT_BUTTON_HEIGHT + ALERT_BOTTOM_MAGIN + ALERT_HEIGHT_GAP;
			
			buttonSX = (btnCount<2 ? 0 : -(btnWidth+ALERT_BUTTON_GAP)/2);
		}
		else {
			// ボタンが3つ以上(縦並び)
			btnWidth = ALERT_WIDTH - 2*ALERT_WIDTH_MAGIN;
			isVertical = true;
			buttonsHeight = ALERT_BUTTON_HEIGHT * btnCount + ALERT_BUTTON_GAP * (btnCount-1) + ALERT_BOTTOM_MAGIN + ALERT_HEIGHT_GAP;
		}
		
		for (int i=0; i<titles.size(); i++) {
			std::string btnTitle = titles[i];
			bool isCancel = (i==0 && vo->get_cancelBtnTitle()!="");
			bool isOK = (vo->get_cancelBtnTitle()=="" ? i==0 : i==1);

			std::string frameImg;
//			CCScale9Sprite *btnBG = NULL;
			CCRect capInset;
			if (isCancel) {
//				capInset = Rect(34, 34, 6, 6);
//				frameImg = ResourceModel::localFilePath("image/common/buttons/btn_tamago_cancel@2x.png");
				capInset = Rect(16, 16, 24, 22);
				frameImg = ResourceModel::localFilePath("image/common/buttons/btn_cancel_tamago@2x.png");
//				btnBG = CCScale9Sprite::create( ResourceModel::localFilePath("image2/common/buttons/btn_tamago_cancel@2x.png").c_str(), CCRectZero, capInset);
			}
			else if (isOK) {
//				capInset = Rect(34, 34, 6, 6);
//				frameImg = ResourceModel::localFilePath("image/common/buttons/btn_tamago_ok@2x.png");
				capInset = Rect(16, 16, 24, 22);
				frameImg = ResourceModel::localFilePath("image/common/buttons/btn_ok_tamago@2x.png");
//				btnBG = CCScale9Sprite::create( ResourceModel::localFilePath("image2/common/buttons/btn_tamago_ok@2x.png").c_str(), CCRectZero, capInset);
			}
			else {
//				capInset = Rect(34, 34, 6, 6);
//				frameImg = ResourceModel::localFilePath("image/common/buttons/btn_tamago_other@2x.png");
				capInset = Rect(16, 16, 24, 22);
				frameImg = ResourceModel::localFilePath("image/common/buttons/btn_other_tamago@2x.png");
//				btnBG = CCScale9Sprite::create( ResourceModel::localFilePath("image2/common/buttons/btn_tamago_other@2x.png").c_str(), CCRectZero, capInset);
			}

			ui::Button *btn = ui::Button::create();
			btn->setScale9Enabled(true);
			btn->setCapInsetsNormalRenderer( capInset );
//			btn->loadTextureNormal( frameImg );
			btn->loadTextures(frameImg, frameImg, "");
			btn->setSize( Size(btnWidth,ALERT_BUTTON_HEIGHT) );
			btn->setTitleFontName(ResourceModel::labelFontName(kLabelFontType_DEFAULT));
			btn->setTitleFontSize(26);
			btn->setTitleText(btnTitle);
			btn->setTitleColor( Color3B(0xff,0xff,0xff) );
			btn->addTouchEventListener(CC_CALLBACK_2(AlertPopup::_buttonCallback, this));
//			CCButtonExt *btn = CCButtonExt::createWithLabel(
//															btnTitle.c_str(), ccc3(0xff,0xff,0xff), ccc3(0x90,0x90,0x90),
//															btnBG,
//															Rect(4,4,btnWidth-8,ALERT_BUTTON_HEIGHT-8),
//															Size(btnWidth,ALERT_BUTTON_HEIGHT),
//															this, callfuncO_selector(AlertPopup::_buttonCallback));
//			btn->setTouchPriority(ResourceModel::kTouchPriority_BUTTON_ON_POPUP);
			
			if (isVertical) {
				// 縦並び
				btn->setPosition( Point(buttonSX, buttonSY-(ALERT_BUTTON_HEIGHT+ALERT_BUTTON_GAP)*i) );
			}
			else {
				// 横並び
				btn->setPosition( Point(buttonSX+i*(btnWidth + ALERT_BUTTON_GAP), buttonSY) );
			}
			buttons->addObject(btn);
			if (isCancel) {
				set_btnCancel(btn);
			}
			else {
				buttons_->addObject(btn);
			}
			buttonsBase->addChild( btn );
		}
	}
	
	int titleHeight = (vo->get_title()=="" ? ALERT_NO_TITLE_HEIGHT : ALERT_TITLE_HEIGHT);
	
	int messageHeight = lblMessage_->getContentSize().height;
	
	int alertHeight = titleHeight + buttonsHeight + messageHeight;
	if (alertHeight < ALERT_MIN_HEIGHT)
	{
		alertHeight = ALERT_MIN_HEIGHT;
		messageHeight = alertHeight - titleHeight - buttonsHeight;
	}
	else if (alertHeight > ALERT_MAX_HEIGHT)
	{
		alertHeight = ALERT_MAX_HEIGHT;
		messageHeight = alertHeight - titleHeight - buttonsHeight;
	}
	
	
	lblTitle_->setPosition(Point(ALERT_WIDTH/2, alertHeight-titleHeight/2));
	lblMessage_->setPosition(Point(ALERT_WIDTH/2, buttonsHeight+messageHeight/2));
//	((LabelTTF*)lblMessage_)->setDimensions(Size(ALERT_WIDTH-2*ALERT_WIDTH_MAGIN, messageHeight));
	lbl->setDimensions(ALERT_WIDTH-2*ALERT_WIDTH_MAGIN, messageHeight);
	
//	buttonsBase->setPosition(ccp(ALERT_WIDTH/2, buttonsHeight-ALERT_BUTTON_HEIGHT/2-ALERT_HEIGHT_GAP));
	buttonsBase->setPosition(Point(ALERT_WIDTH/2, buttonsHeight-ALERT_BUTTON_HEIGHT/2-ALERT_HEIGHT_GAP));
	
	
	CCLOG("alertHeight:%d, titleHeight:%d, messageHeight:%d, buttonHeight:%d", alertHeight, (int)titleHeight, messageHeight, buttonsHeight);
	
	
	
	// Background
//	LayerColor *bg = LayerColor::create(Color4B(0xff,0xff,0xff,0xff), ALERT_WIDTH, alertHeight);
//	CCScale9Sprite *bg = CCScale9Sprite::create( ResourceModel::localFilePath("image2/common/bg/bg_popup@2x.png").c_str(), CCRectZero, CCRectMake(22, 24, 8, 8) );
//	bg->setContentSize( CCSizeMake(ALERT_WIDTH, alertHeight) );
	Scale9Sprite *bg = Scale9Sprite::create(Rect(12, 12, 26, 26), ResourceModel::localFilePath("image/common/popup/bg_alertpopup@2x.png"));
	bg->setContentSize( Size(ALERT_WIDTH, alertHeight) );
	bg->setAnchorPoint(Point(0,0));
	bg->setPosition(Point(0, 0));
	addChild(bg);
	
	// Title Background
	if (vo->get_title() != "") {
//		LayerColor *titleBG = LayerColor::create(Color4B(0xaa,0xaa,0xaa,0xff), ALERT_WIDTH-2*ALERT_WIDTH_MAGIN, 80);
//		CCScale9Sprite *titleBG = CCScale9Sprite::create(ResourceModel::resolveResourcePath("image2/common/bg/bg_sublist1@2x.png").c_str(), CCRectZero, CCRectMake(22, 24, 8, 8));
//		titleBG->setContentSize( CCSizeMake( ALERT_WIDTH-2*ALERT_WIDTH_MAGIN, 80) );
		Scale9Sprite *titleBG = Scale9Sprite::create(Rect(12, 12, 26, 26), ResourceModel::localFilePath("image/common/popup/bg_title@2x.png"));
		titleBG->setContentSize( Size(ALERT_WIDTH-2*ALERT_WIDTH_MAGIN, 80) );
		titleBG->setAnchorPoint(Point(0.5, 0.5));
		titleBG->setPosition(Point(ALERT_WIDTH/2, alertHeight-80));
		addChild(titleBG);
	}
	
	
	addChild(buttonsBase);
	
	addChild(lblTitle_);
	addChild(lblMessage_);
	
	
	{
//		CCLayerColor *lc = CCLayerColor::create(ccc4(0xff,0,0,0x0), 20, messageHeight);
		CCLayerColor *lc = CCLayerColor::create(Color4B(0xff,0,0,0x0), 20, messageHeight);
		lc->setPosition(lblMessage_->getPosition());
		addChild(lc);
	}
	
	
	// コンテンツサイズ＆センター位置を改めて設定
	setContentSize(Size(ALERT_WIDTH, alertHeight));
	set_centerPos( Point(ALERT_WIDTH/2, alertHeight/2) );
}

//void AlertPopup::_requildLayout()
//{
//	removeAllChildren();
//
//	int w = POPUP_WIDTH;
//	int h = POPUP_HEIGHT;
//	
//	LayerColor *lc = LayerColor::create(Color4B(0xff,0,0,0xff), POPUP_WIDTH, POPUP_HEIGHT);
//	addChild(lc);
//
//	ui::Button *btn = ui::Button::create( ResourceModel::localFilePath("image/btn_smaill_start@2x.png").c_str() );
//	addChild(btn);
//	
//	set_centerPos(Point(w/2, h/2));
//}
