//
//  AlertPopup.h
//  PuzzleQuest
//
//  Created by shogo yanuma on 2014/08/11.
//
//

#ifndef __PuzzleQuest__AlertPopup__
#define __PuzzleQuest__AlertPopup__

#include "PopupBase.h"
#include "ui/CocosGUI.h"
#include "PDArray.h"

class AlertPopupInfoVO : public cocos2d::Ref
{
public:
	CC_SYNTHESIZE(std::string, title_, _title);
	CC_SYNTHESIZE(std::string, message_, _message);
	CC_SYNTHESIZE(std::string, cancelBtnTitle_, _cancelBtnTitle);
	CC_SYNTHESIZE(std::vector<std::string>, buttonsTitle_, _buttonsTitle);
	
public:
	CREATE_FUNC(AlertPopupInfoVO);

	AlertPopupInfoVO() {}
	virtual ~AlertPopupInfoVO() {}
	virtual bool init() { return true; }
};

class AlertPopup : public PopupBase
{
public:
	CC_SYNTHESIZE_RETAIN(AlertPopupInfoVO*, infoVO_, _infoVO);

	CC_SYNTHESIZE_RETAIN(cocos2d::Label*, lblTitle_, _lblTitle);
	CC_SYNTHESIZE_RETAIN(cocos2d::Label*, lblMessage_, _lblMessage);
	
	CC_SYNTHESIZE_RETAIN(cocos2d::ui::Button*, btnCancel_, _btnCancel);
	CC_SYNTHESIZE_RETAIN(PDArray*, buttons_, _buttons);	// Array of ui::Button
	
public:
	CREATE_FUNC(AlertPopup);

	static AlertPopup *createWith( const std::string &title, const std::string &message, PopupBaseDelegate *delegate, const std::string &cancelTitle, const char *buttonsTitle, ... );
	static AlertPopup *createWith( const std::string &title, const std::string &message, std::function<void(PopupBase *popup, int buttonIndex)> callback, const std::string &cancelTitle, const char *buttonsTitle, ... );
	
	AlertPopup() {
		infoVO_ = NULL;
		lblTitle_ = NULL;
		lblMessage_ = NULL;
		btnCancel_ = NULL;
		buttons_ = NULL;
	}
	virtual ~AlertPopup() {
		set_infoVO(NULL);
		set_lblTitle(NULL);
		set_lblMessage(NULL);
		set_btnCancel(NULL);
		set_buttons(NULL);
	}
	virtual bool init();

	void setPopupInfo(AlertPopupInfoVO *vo);
	PDArray *allButtons();

private:
	void _buttonCallback(cocos2d::Ref *sender, cocos2d::ui::Widget::TouchEventType ev);
//	void _buttonCallback(cocos2d::Ref* obj);
	void _buildLayout();
//	void _requildLayout();
};

#endif /* defined(__PuzzleQuest__AlertPopup__) */
