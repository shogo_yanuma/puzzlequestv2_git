//
//  BlockerForLoading.cpp
//  PuzzleQuest
//
//  Created by shogo yanuma on 2014/08/12.
//
//

#include "BlockerForLoading.h"
#include "ResourceModel.h"

USING_NS_CC;

bool BlockerForLoading::init()
{
	Size winSize = Director::getInstance()->getWinSize();
	LayerColor *lc = LayerColor::create(Color4B(0xff,0,0,128), winSize.width, winSize.height);
	addChild(lc);

	Label *lbl;
	lbl = ResourceModel::boldLabelTTF("Loading", 20);
	lbl->setAnchorPoint(Point(0.5, 0.5));
	lbl->setPosition(Point(winSize.width/2, winSize.height/2));
	addChild(lbl);
	set_lblMessage(lbl);
	
	return true;
}

void BlockerForLoading::onEnter()
{
	Director *director = Director::getInstance();
	Node::onEnter();
}

void BlockerForLoading::onExit()
{
	Director *director = Director::getInstance();
	Node::onExit();
}

void BlockerForLoading::setMessage(const std::string &msg)
{
	message_ = msg;
	if (lblMessage_ != NULL) {
		lblMessage_->setString(msg);
	}
}
