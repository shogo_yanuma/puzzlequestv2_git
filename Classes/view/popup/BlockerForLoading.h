//
//  BlockerForLoading.h
//  PuzzleQuest
//
//  Created by shogo yanuma on 2014/08/12.
//
//

#ifndef __PuzzleQuest__BlockerForLoading__
#define __PuzzleQuest__BlockerForLoading__

#include "cocos2d.h"

class BlockerForLoading : public cocos2d::Node
{
protected:
	std::string message_;

public:
	CC_SYNTHESIZE_RETAIN(cocos2d::Label*, lblMessage_, _lblMessage);

public:
	CREATE_FUNC(BlockerForLoading);

	BlockerForLoading() {
		lblMessage_ = NULL;
	}
	virtual ~BlockerForLoading() {
		set_lblMessage(NULL);
	}
	virtual bool init();

	virtual void onEnter();
	virtual void onExit();

	void setMessage(const std::string &msg);
};

#endif /* defined(__PuzzleQuest__BlockerForLoading__) */
