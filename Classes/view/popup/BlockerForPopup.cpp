//
//  BlockerForPopup.cpp
//  PuzzleQuest
//
//  Created by shogo yanuma on 2014/08/12.
//
//

#include "BlockerForPopup.h"

USING_NS_CC;

bool BlockerForPopup::init()
{
	Size winSize = Director::getInstance()->getWinSize();
	LayerColor *lc = LayerColor::create(Color4B(0,0,0,128), winSize.width, winSize.height);
	addChild(lc);
	return true;
}

void BlockerForPopup::onEnter()
{
	Director *director = Director::getInstance();
	Node::onEnter();
}

void BlockerForPopup::onExit()
{
	Director *director = Director::getInstance();
	Node::onExit();
}
