//
//  OverlayerBase.cpp
//  PuzzleQuest
//
//  Created by shogo yanuma on 2014/09/20.
//
//

#include "OverlayerBase.h"
#include "ScreenModel.h"

#pragma mark - OverlayerBase
bool OverlayerBase::closeOverlayer(bool animate, bool forceClose)
{
	if (!forceClose) {
		if (delegate_ != NULL) {
			bool res = delegate_->overlayerWillClose(this);
			if (!res) {
				return false;
			}
		}
	}
	
	retain();
	
	ScreenModel::getInstance()->closeOverlayer(this, animate);

	if (delegate_ != NULL) {
		delegate_->overlayerDidClose(this);
	}

	autorelease();
	
	return true;
}
