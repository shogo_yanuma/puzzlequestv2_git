//
//  OverlayerBase.h
//  PuzzleQuest
//
//  Created by shogo yanuma on 2014/09/20.
//
//

#ifndef __PuzzleQuest__OverlayerBase__
#define __PuzzleQuest__OverlayerBase__

#include "cocos2d.h"

#pragma mark - OverlayerBaseDelegate
class OverlayerBase;
class OverlayerBaseDelegate
{
public:
	virtual bool overlayerWillClose(OverlayerBase *overlayer) { return true; }
	virtual void overlayerDidClose(OverlayerBase *overlayer) { }
};


#pragma mark - OverlayerBase
class OverlayerBase : public cocos2d::Node
{
public:
	CC_SYNTHESIZE(OverlayerBaseDelegate*, delegate_, _delegate);
	CC_SYNTHESIZE_RETAIN(cocos2d::Ref*, userInfo_, _userInfo);
	
public:
	OverlayerBase() {
		delegate_ = NULL;
		userInfo_ = NULL;
	}
	virtual ~OverlayerBase() {
		set_delegate(NULL);
		set_userInfo(NULL);
	}

	bool closeOverlayer(bool animate=true, bool forceClose=false);
};

#endif /* defined(__PuzzleQuest__OverlayerBase__) */
