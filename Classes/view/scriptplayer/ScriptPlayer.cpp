//
//  ScriptPlayer.cpp
//  PuzzleQuest
//
//  Created by shogo yanuma on 2016/04/15.
//
//

#include "ScriptPlayer.h"
#include "ScreenModel.h"
#include "ResourceModel.h"

bool ScriptPlayer::init()
{
	if (!Layer::init()) {
		return false;
	}
	
//	//	Size winSize = Director::getInstance()->getWinSize();
//	
//	auto rootNode = CSLoader::createNode("title/TitleScene.csb");
//	ResourceModel::setToScreenCenter(rootNode);
//	//	rootNode->setAnchorPoint( Point(0.5, 0.5) );
//	//	rootNode->setPosition( Point(winSize.width/2, winSize.height/2) );
//	//	Rect rc = rootNode->getBoundingBox();
//	
//	addChild(rootNode);
//	
//	ui::Button *btn;
//	btn = (ui::Button*)(rootNode->getChildByName("footer@gv_cb")->getChildByName("btnStart"));
//	ResourceModel::autoLayout(btn, kAutoLayout_BC);
//	
//	btn->addClickEventListener([this](Ref *sender) {
//		ScreenModel::getInstance()->dispHome();
//	});
	
	return true;
}

void ScriptPlayer::runScript(ScriptFileVO *scriptVO, const std::string &start)
{
	scriptStack_.clear();

	scriptStack_.pushBack(scriptVO);
	if (start != "") {
		int startPos = scriptVO->getCmdIndexWithLabel(start);
		if (startPos < 0) {
			CCLOG("WARN: unknown label[%s] in script[%s]", start.c_str(), scriptVO->get_filename().c_str());
		}
		else {
			scriptVO->set_currentCmdPos( startPos );
		}
	}
	
}

void ScriptPlayer::pushScript(ScriptFileVO *scriptVO, const std::string &start)
{
}

void ScriptPlayer::popScript()
{
}

void ScriptPlayer::doNextCommand()
{
}
