//
//  ScriptValues.cpp
//  PuzzleQuest
//
//  Created by shogo yanuma on 2016/04/17.
//
//

#include "ScriptValues.h"

bool ScriptValues::init()
{
	PDDictionary *valueMap = PDDictionary::create();
	set_valueMap(valueMap);
	
	return true;
}

void ScriptValues::setValue(const std::string &key, PDValue *val)
{
	valueMap_->setObject(key, val);
}

PDValue *ScriptValues::getValue(const std::string &key)
{
	PDValue *ret = (PDValue*)valueMap_->objectForKey(key);
	return ret;
}

void ScriptValues::clearValue(const std::string &key)
{
	valueMap_->removeObjectWithKey(key);
}

void ScriptValues::clearAllValue()
{
	valueMap_->removeAllObjects();
}
