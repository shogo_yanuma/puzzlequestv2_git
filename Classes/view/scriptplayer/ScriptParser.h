//
//  ScriptParser.h
//  PuzzleQuest
//
//  Created by shogo yanuma on 2016/04/15.
//
//

#ifndef ScriptParser_h
#define ScriptParser_h

class ScriptParser : public cocos2d::Ref
{
public:
	CC_SYNTHESIZE(std::string, srcScript_, _srcScript);
	CC_SYNTHESIZE(char*, srcIndicator_, _srcIndicator);
	CC_SYNTHESIZE(int, curPos_, _curPos);
	CC_SYNTHESIZE(int, dataSize_, _dataSize);
	CC_SYNTHESIZE(int, curLineCount_, _curLineCount);
	
public:
	CREATE_FUNC(ScriptParser);

	ScriptParser() {
		//CCLOG("ScriptParser::alloc - %p", this);
	}
	virtual ~ScriptParser() {
		//CCLOG("ScriptParser::dealloc - %p", this);
	}
	
	virtual bool init();

	bool initWithScript(const std::string &script);
	bool isEOF();
	char getAt(int idx);
	int getCurrentLineCount();
	std::string getNextToken();
	std::string getNextString();
	std::string getNextLine();
	std::string readUntil(const std::string expects);
	void skipUntil(const std::string expects);
	void skipBlanks();
	
private:
	inline bool _isMatch(char c, const char *matches) {
		while (*matches && (c!=*matches)) {
			matches ++;
		}
		return (*matches != 0x00);
	}
};

#endif /* ScriptParser_h */
