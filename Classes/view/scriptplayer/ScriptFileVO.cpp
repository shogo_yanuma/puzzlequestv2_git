//
//  ScriptFileVO.cpp
//  PuzzleQuest
//
//  Created by shogo yanuma on 2016/04/15.
//
//

#include "ScriptFileVO.h"

//static
ScriptCmd *ScriptCmd::createWith(ScriptFileVO *owner, ENUM_SCRIPT_CMD cmd)
{
	ScriptCmd *ret = ScriptCmd::create();
	ret->set_owner(owner);
	ret->set_cmd(cmd);
	return ret;
}

bool ScriptFileVO::initWithScript(const std::string &script, const std::string &basePath, const std::string filename)
{
	set_basePath(basePath);
	set_filename(filename);
	
	ScriptParser *parser = ScriptParser::create();
	parser->initWithScript(script);
	
	// スクリプトのパース
	_parseScript( parser );
	
	return true;
}

int ScriptFileVO::getCmdIndexWithLabel(const std::string &label)
{
	int idx = -1;
	for (int i=0; i<commandList_.size(); i++) {
		ScriptCmd *cmd = commandList_.at(i);
		if (cmd->get_cmd() != ScriptCmd::kCmd_LABEL) {
			continue;
		}
		if (cmd->get_params() == NULL) {
			continue;
		}
		
		PDValue *val = (PDValue*)(cmd->get_params()->objectForKey("label"));
		if (val->stringValue() == label) {
			idx = i;
			break;
		}
	}
	return idx;
}

ScriptCmd *ScriptFileVO::fetchNextCmd()
{
	if (currentCmdPos_ >= commandList_.size()) {
		return NULL;
	}
	
	ScriptCmd *cmd = commandList_.at(currentCmdPos_);
	if (cmd->get_cmd() == ScriptCmd::kCmd_INCLUDE) {
		ScriptFileVO *fileVO = (ScriptFileVO*)cmd->get_params()->objectForKey("filevo");
		if (fileVO != NULL) {
			cmd = fileVO->fetchNextCmd();
		}
	}
	return cmd;
}

bool ScriptFileVO::_parseScript(ScriptParser *parser)
{
	if (parser == NULL) {
		return false;
	}
	
	commandList_.clear();
	
	ScriptCmd *lastText = NULL;
	
	while (!(parser->isEOF()))
	{
		parser->skipBlanks();
		
		int firstPos = parser->get_curPos();
		std::string line = parser->getNextLine();

		//int lineEndPos = parser->get_curPos();
		
		ScriptParser *parserForLine = ScriptParser::create();
		parserForLine->initWithScript(line);
		
		std::string lineToken = parserForLine->getNextToken();
		
		// 空行
		if (lineToken.size() == 0) {
			continue;
		}
		// コメント行
		else if (lineToken.at(0) == ';') {
			parser->set_curPos(firstPos);
			std::string line = parser->getNextLine();
			lastText = NULL;
		}
		// ラベル行
		else if (lineToken.at(0) == '*') {
			ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_LABEL );
			commandList_.pushBack( cmd );

			PDDictionary *params = PDDictionary::create();
			cmd->set_params( params );
			
			params->setObject("CMD", PDValue::createAsString("label"));
			params->setObject("label", PDValue::createAsString(lineToken));
			
			lastText = NULL;
		}
		else {
			//TODO: 小文字にする
			std::string smallToken = lineToken;

			if (smallToken == "log") {
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_LOG );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );

				parser->set_curPos( firstPos+smallToken.size()+1 );

				std::string str = parser->getNextString();

				params->setObject("CMD", PDValue::createAsString("log"));
				params->setObject("text", PDValue::createAsString(str));
				lastText = NULL;
			}
			else if (smallToken == "cls") {
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_CLS );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );
				
				parser->set_curPos( firstPos+smallToken.size()+1 );

				params->setObject("CMD", PDValue::createAsString("cls"));
				lastText = NULL;
			}
			else if (smallToken == "include") {
				// includeはスクリプトパース時点で取り込んでしまう
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_INCLUDE );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );
				
				parser->set_curPos( firstPos+smallToken.size()+1 );

				std::string fileName = parser->getNextString();
				std::string filePath = basePath_ + fileName;

				params->setObject("CMD", PDValue::createAsString("include"));
				params->setObject("file", PDValue::createAsString(fileName));
				
				bool isExist =FileUtils::getInstance()->isFileExist( filePath );
				if (!isExist) {
					// ファイルが見つからない
					CCLOG("WARN: file did not exist. - file:%s", filePath.c_str());
				}
				else {
					std::string script = FileUtils::getInstance()->getStringFromFile( filePath );
					ScriptFileVO *fileVO = ScriptFileVO::create();
					fileVO->initWithScript( script );
					
					// 取り込み元のVOも保持しておく(※読み込まれたcmdが読み込み元ファイル情報として参照しているため）
					params->setObject("filevo", fileVO);
					
					// 別スクリプトの取り込み
					for (int i=0; i<fileVO->get_commandList().size(); i++) {
						ScriptCmd *cmd = fileVO->get_commandList().at(i);
						commandList_.pushBack(cmd);
					}
				}

				// include終端を入れておく(主にテスト用)
				ScriptCmd *endCmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_INCLUDE_END );
				commandList_.pushBack( endCmd );

				PDDictionary *endParams = PDDictionary::create();
				endCmd->set_params( endParams );

				endParams->setObject("CMD", PDValue::createAsString("include_end"));
				
				lastText = NULL;
			}
			else if (smallToken == "script") {
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_SCRIPT );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );
				
				parser->set_curPos( firstPos+smallToken.size()+1 );
				
				std::string file = parser->getNextString();

				params->setObject("CMD", PDValue::createAsString("script"));
				params->setObject("file", PDValue::createAsString( file ));
				
				lastText = NULL;
			}
			else if (smallToken == "ld") {
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_LD );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );
				
				parser->set_curPos( firstPos+smallToken.size()+1 );

				std::string pos = parser->getNextToken();
				parser->getNextToken();	// "," 読み飛ばし
				std::string src = parser->getNextString();

				params->setObject("CMD", PDValue::createAsString("ld"));
				params->setObject("pos", PDValue::createAsString(pos));
				params->setObject("src", PDValue::createAsString(src));
				
				lastText = NULL;
			}
			else if (smallToken == "cl") {
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_CL );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );
				
				parser->set_curPos( firstPos+smallToken.size()+1 );
				
				std::string pos = parser->getNextToken();

				params->setObject("CMD", PDValue::createAsString("cl"));
				params->setObject("pos", PDValue::createAsString(pos));
				
				lastText = NULL;
			}
			else if (smallToken == "eff") {
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_EFF );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );
				
				parser->set_curPos( firstPos+smallToken.size()+1 );
				
				std::string pos = parser->getNextToken();
				parser->getNextToken();	// "," 読み飛ばし
				std::string src = parser->getNextToken();
				
				params->setObject("CMD", PDValue::createAsString("eff"));
				params->setObject("pos", PDValue::createAsString(pos));
				params->setObject("src", PDValue::createAsString(src));

				lastText = NULL;
			}
			else if (smallToken == "bg") {
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_BG );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );
				
				parser->set_curPos( firstPos+smallToken.size()+1 );
				
				std::string src = parser->getNextString();
				
				params->setObject("CMD", PDValue::createAsString("bg"));
				params->setObject("src", PDValue::createAsString(src));
				
				lastText = NULL;
			}
			else if (smallToken == "clbg") {
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_CLBG );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );
				
				parser->set_curPos( firstPos+smallToken.size()+1 );
				
				params->setObject("CMD", PDValue::createAsString("bg"));

				lastText = NULL;
			}
			else if (smallToken == "set") {
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_SET );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );
				
				parser->set_curPos( firstPos+smallToken.size()+1 );
				
				std::string key = parser->getNextToken();
				parser->getNextToken();	// "," 読み飛ばし
				int tmpPos = parser->get_curPos();
				std::string val = parser->getNextToken();	//TODO: getNextToken()時に数値か文字列か判定できるようにする必要がある
				if (val == "\"") {
					// 文字列
					parser->set_curPos(tmpPos);
					val = parser->getNextString();
				}
				else {
					//TODO: ""で囲まれていない場合、数値かどうかのチェックをしてvalの型を決定する
					//	valに含まれいてる文字が、"0123456789.-"のみの場合は数値と判断する
				}
				
				params->setObject("CMD", PDValue::createAsString("set"));
				params->setObject("key", PDValue::createAsString(key));
				params->setObject("val", PDValue::createAsString(val));
				
				lastText = NULL;
			}
			else if (smallToken == "add") {
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_ADD );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );
				
				parser->set_curPos( firstPos+smallToken.size()+1 );
				
				params->setObject("CMD", PDValue::createAsString("add"));
				
				lastText = NULL;
			}
			else if (smallToken == "sub") {
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_SUB );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );
				
				parser->set_curPos( firstPos+smallToken.size()+1 );
				
				params->setObject("CMD", PDValue::createAsString("sub"));
				
				lastText = NULL;
			}
			else if (smallToken == "mult") {
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_MULT );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );
				
				parser->set_curPos( firstPos+smallToken.size()+1 );
				
				params->setObject("CMD", PDValue::createAsString("mult"));
				
				lastText = NULL;
			}
			else if (smallToken == "div") {
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_DIV );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );
				
				parser->set_curPos( firstPos+smallToken.size()+1 );
				
				params->setObject("CMD", PDValue::createAsString("div"));
				
				lastText = NULL;
			}
			else if (smallToken == "rand") {
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_RAND );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );
				
				parser->set_curPos( firstPos+smallToken.size()+1 );
				
				params->setObject("CMD", PDValue::createAsString("rand"));
				
				lastText = NULL;
			}
			else if (smallToken == "select") {
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_SELECT );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );
				
				parser->set_curPos( firstPos+smallToken.size()+1 );

				std::string key = parser->getNextToken();
				parser->getNextToken();	// "," 読み飛ばし

				PDArray *list = PDArray::create();

				while (true) {
					std::string choiceText = parser->getNextString();
					list->addObject( PDValue::createAsString(choiceText) );

					int tmpPos = parser->get_curPos();
					std::string camma = parser->getNextToken();	// "," 読み飛ばし
					if (camma != ",") {
						// もし次のトークンがカンマでなければ、選択肢の終了と判断する
						parser->set_curPos(tmpPos);
						break;
					}
				}
				
				params->setObject("CMD", PDValue::createAsString("select"));
				params->setObject("key", PDValue::createAsString(key));
				params->setObject("list", list);
				
				lastText = NULL;
			}
			else if (smallToken == "gosub") {
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_GOSUB );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );
				
				parser->set_curPos( firstPos+smallToken.size()+1 );

				std::string label = parser->getNextToken();
				
				params->setObject("CMD", PDValue::createAsString("gosub"));
				params->setObject("label", PDValue::createAsString(label));
				
				lastText = NULL;
			}
			else if (smallToken == "goto") {
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_GOTO );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );
				
				parser->set_curPos( firstPos+smallToken.size()+1 );
				
				std::string label = parser->getNextToken();
				
				params->setObject("CMD", PDValue::createAsString("goto"));
				params->setObject("label", PDValue::createAsString(label));
				
				lastText = NULL;
			}
			else if (smallToken == "ret") {
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_RET );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );
				
				parser->set_curPos( firstPos+smallToken.size()+1 );
				
				std::string label = parser->getNextToken();
				
				params->setObject("CMD", PDValue::createAsString("ret"));
				
				lastText = NULL;
			}
			else if (smallToken == "if") {
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_IF );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );
				
				parser->set_curPos( firstPos+smallToken.size()+1 );
				
				std::string label = parser->getNextToken();
				
				params->setObject("CMD", PDValue::createAsString("if"));
				
				lastText = NULL;
			}
			else if (smallToken == "switch") {
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_SWITCH );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );
				
				parser->set_curPos( firstPos+smallToken.size()+1 );
				
				std::string label = parser->getNextToken();
				
				params->setObject("CMD", PDValue::createAsString("switch"));
				
				lastText = NULL;
			}
			else if (smallToken == "end") {
				ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_END );
				commandList_.pushBack( cmd );

				PDDictionary *params = PDDictionary::create();
				cmd->set_params( params );
				
				parser->set_curPos( firstPos+smallToken.size()+1 );
				
				std::string label = parser->getNextToken();
				
				params->setObject("CMD", PDValue::createAsString("end"));

				lastText = NULL;
			}
			// テキスト（連続したテキストは改行していても連結させる）
			else {
				if (lastText == NULL) {
					ScriptCmd *cmd = ScriptCmd::createWith( this, ScriptCmd::kCmd_TEXT );
					commandList_.pushBack( cmd );
					lastText = cmd;
				}

				if (lastText->get_params() == NULL) {
					PDDictionary *params = PDDictionary::create();
					params->setObject("CMD", PDValue::createAsString("txt"));
					lastText->set_params( params );
				}

				//NOTE: lastTextに追記する
				std::string txt;
				PDValue *val = (PDValue*)lastText->get_params()->objectForKey("txt");
				if (val != NULL) {
					txt = val->stringValue();
				}
				txt += lineToken;
				lastText->get_params()->setObject("txt", PDValue::createAsString(txt));

				int len = lineToken.size();
				std::string test = lineToken.substr(0,len);
				parser->set_curPos( firstPos+len+1 );
			}
		}
		
	}

	return true;
}
