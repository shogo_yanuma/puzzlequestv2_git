//
//  ScriptParser.cpp
//  PuzzleQuest
//
//  Created by shogo yanuma on 2016/04/15.
//
//

#include "ScriptParser.h"

#define k_Blanks			" \r\n\t"
#define k_NewLines			"\r\n"
#define k_Delimiters		"\"(){},"
#define k_TextDelimiters	"\""

bool ScriptParser::init()
{
	curPos_ = 0;
	dataSize_ = 0;
	curLineCount_ = 0;
	return true;
}

bool ScriptParser::initWithScript(const std::string &script)
{
	if (!init()) {
		return false;
	}

	srcScript_ = script;
	dataSize_ = (int)srcScript_.length();
	return true;
}

bool ScriptParser::isEOF()
{
	return (curPos_ >= dataSize_);
}

char ScriptParser::getAt(int idx)
{
	if (idx<0 || idx>=dataSize_) { return 0; }
	return srcScript_[idx];
}

int ScriptParser::getCurrentLineCount()
{
	return curLineCount_;
}

std::string ScriptParser::getNextToken()
{
	skipBlanks();
	
	int len = 0;
	bool bIsDelimiterEnd = false;
	for (int i=curPos_; i<dataSize_; i++) {
		char ch = srcScript_[i];
		if (_isMatch(ch, k_Delimiters)) {
			bIsDelimiterEnd = true;
			break;
		}
		if (_isMatch(ch, k_NewLines)) { curLineCount_++; }
		if (_isMatch(ch, k_Blanks)) { break; }
		len ++;
	}
	
	if (bIsDelimiterEnd && (len==0)) {
		len = 1;
	}
	
	std::string token = srcScript_.substr(curPos_, len);
	curPos_ += len;
	return token;
}

std::string ScriptParser::getNextString()
{
	skipBlanks();
	
	char ch = srcScript_[curPos_];
	if (_isMatch(ch, k_TextDelimiters)) {
		curPos_ ++;
	}
	else {
		return getNextToken();
	}
	
	return readUntil(k_TextDelimiters);
}

std::string ScriptParser::getNextLine()
{
	skipBlanks();
	
	int len = 0;
	for (int i=curPos_; i<dataSize_; i++) {
		char ch = srcScript_[i];
		if (_isMatch(ch, k_NewLines)) { curLineCount_++; break; }
		len ++;
	}
	
	std::string token = srcScript_.substr(curPos_, len);
	curPos_ += len;
	return token;
}

std::string ScriptParser::readUntil(const std::string expects)
{
	int len = 0;
	for (int i=curPos_; i<dataSize_; i++) {
		char ch = srcScript_[i];
		if (_isMatch(ch, expects.c_str())) { break; }
		len ++;
	}
	
	std::string token = srcScript_.substr(curPos_, len);
	curPos_ += (len + 1);
	return token;
}

void ScriptParser::skipUntil(const std::string expects)
{
	//TODO:
}

void ScriptParser::skipBlanks()
{
	int i;
	for (i=curPos_; i<dataSize_; i++) {
		char ch = srcScript_[i];
		if (_isMatch(ch, k_Blanks)) {
			continue;
		}
		break;
	}
	curPos_ = i;
}
