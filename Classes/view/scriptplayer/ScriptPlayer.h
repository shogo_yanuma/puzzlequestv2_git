//
//  ScriptPlayer.h
//  PuzzleQuest
//
//  Created by shogo yanuma on 2016/04/15.
//
//

#ifndef ScriptPlayer_h
#define ScriptPlayer_h

#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

#include "ScriptFileVO.h"
#include "ScriptValues.h"

USING_NS_CC;
using namespace cocostudio::timeline;

class ScriptPlayerDelegate;
class ScriptPlayer : public Layer
{
public:
	CC_SYNTHESIZE(ScriptPlayerDelegate*, delegate_, _delegate);
	CC_SYNTHESIZE(ScriptValues*, scriptValues_, _scriptValues);
	CC_SYNTHESIZE(cocos2d::Vector<ScriptFileVO*>, scriptStack_, _scriptStack);
	
public:
	CREATE_FUNC(ScriptPlayer);
	
	ScriptPlayer() {
		delegate_ = NULL;
		scriptValues_ = NULL;
	}
	virtual ~ScriptPlayer() {
		set_scriptValues(NULL);
	}
	
	virtual bool init();

	void runScript(ScriptFileVO *scriptVO, const std::string &start="");
	void pushScript(ScriptFileVO *scriptVO, const std::string &start="");
	void popScript();

	void doNextCommand();
//	void startScript();
};

class ScriptPlayerDelegate
{
public:
	virtual bool scriptPlayer_willDoCommand(ScriptPlayer *player, ScriptCmd *cmd) { return false; }
	virtual void scriptPlayer_didFinishScript(ScriptPlayer *player, ScriptFileVO *script) {}
};

#endif /* ScriptPlayer_h */
