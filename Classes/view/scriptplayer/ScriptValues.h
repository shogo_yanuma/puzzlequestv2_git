//
//  ScriptValues.h
//  PuzzleQuest
//
//  Created by shogo yanuma on 2016/04/17.
//
//

#ifndef ScriptValues_h
#define ScriptValues_h

#include "PDValue.h"
#include "PDDictionary.h"

USING_NS_CC;

class ScriptValues : public Ref
{
public:
	CC_SYNTHESIZE(PDDictionary*, valueMap_, _valueMap);
	
public:
	CREATE_FUNC(ScriptValues);

	ScriptValues() {
		valueMap_ = NULL;
	}
	virtual ~ScriptValues() {
		set_valueMap(NULL);
	}

	virtual bool init();

	void setValue(const std::string &key, PDValue *val);
	PDValue *getValue(const std::string &key);
	void clearValue(const std::string &key);
	void clearAllValue();
};

#endif /* ScriptValues_h */
