//
//  ScriptFileVO.h
//  PuzzleQuest
//
//  Created by shogo yanuma on 2016/04/15.
//
//

#ifndef ScriptFileVO_h
#define ScriptFileVO_h

#include "PDDictionary.h"
#include "ScriptParser.h"

USING_NS_CC;

class ScriptFileVO;
class ScriptCmd : public Ref
{
public:
	typedef enum {
		kCmd_NOP = 0,
		kCmd_LOG,		// ログ
			// log [ログ文字列]
		kCmd_TEXT,		// テキスト
			// [テキスト文字列]
		kCmd_CLS,		// メッセージ消去
			// cls
		kCmd_INCLUDE,	// インクルード
			// include [インクルードファイル名]
		kCmd_INCLUDE_END,	// インクルード終了
		kCmd_SCRIPT,	// 他のスクリプトファイル実行
			// script [スクリプトファイル名]
		kCmd_LABEL,	// ラベル
			// *label1
		kCmd_LD,		// キャラ表示
			// ld [l:左/c:中央/r:右], [Spineファイル名#アクション名>ループアクション名]
		kCmd_CL,		// キャラ消去
			// cl [l:左/c:中央/r:右/a:全て]
		kCmd_EFF,		// エフェクト(1回再生すると消滅）
			// eff [X座標], [Y座標], [Spineファイル名#アクション名]
		kCmd_BG,		// 背景表示
			// bg [Spineファイル名#アクション名>ループアクション名]
		kCmd_CLBG,	// 背景消去
			// clbg
		kCmd_SET,		// 変数に値をセット
			// set [値代入先の変数名], [変数名/値1]
		kCmd_ADD,		// 加算
			// add [結果代入先の変数名], [変数名/値1], [変数名/値2]
		kCmd_SUB,		// 減算
			// sub [結果代入先の変数名], [変数名/値1], [変数名/値2]
		kCmd_MULT,	// 乗算
			// mult [結果代入先の変数名], [変数名/値1], [変数名/値2]
		kCmd_DIV,		// 除算
			// div [結果代入先の変数名], [変数名/値1], [変数名/値2]
		kCmd_RAND,	// 乱数
			// rand [結果代入先の変数名], [最小値], [最大値]
		kCmd_SELECT,	// 選択
			// select [変数名], [選択肢1], [選択肢2], ...
		kCmd_GOSUB,	// サブルーチンにジャンプ
			// gosub [ジャンプ先ラベル名]
		kCmd_GOTO,	// ジャンプ
			// goto [ジャンプ先ラベル名]
		kCmd_RET,		// リターン
			// ret
		kCmd_IF,		// 条件分岐
			// if [数値/変数名] [==,!=,>=,<=] [数値/変数名] [goto/gosub] [ラベル名]
		kCmd_SWITCH,	// 分岐
			// switch [変数名], [値1], [ジャンプ先ラベル1], [値2], [ジャンプ先ラベル2], ...
		kCmd_END,		// スクリプト終了
			// end [返り値]
	} ENUM_SCRIPT_CMD;
	
public:
#ifdef DEBUG
	CC_SYNTHESIZE(int, lineno_, _lineno);
#endif
	CC_SYNTHESIZE(ScriptFileVO*, owner_, _owner);
	CC_SYNTHESIZE(ENUM_SCRIPT_CMD, cmd_, _cmd);
	CC_SYNTHESIZE_RETAIN(PDDictionary*, params_, _params);
	
public:
	static ScriptCmd *createWith(ScriptFileVO *owner, ENUM_SCRIPT_CMD cmd);
	
	CREATE_FUNC(ScriptCmd);

	ScriptCmd() {
		//CCLOG("ScriptCmd::alloc - %p", this);
		owner_ = NULL;
		cmd_ = kCmd_NOP;
		params_ = NULL;
	}
	virtual ~ScriptCmd() {
		//CCLOG("ScriptCmd::dealloc - %p", this);
		set_params(NULL);
	}

	virtual bool init() { return true; }
};

class ScriptFileVO : public Ref
{
public:
	CC_SYNTHESIZE(std::string, basePath_, _basePath);
	CC_SYNTHESIZE(std::string, filename_, _filename);
//	CC_SYNTHESIZE_RETAIN(ScriptParser*, parser_, _parser);
	
	CC_SYNTHESIZE(int, currentCmdPos_, _currentCmdPos);
	CC_SYNTHESIZE(cocos2d::Vector<ScriptCmd*>, commandList_, _commandList);
	CC_SYNTHESIZE(std::vector<int>, cmdPosStack_, _cmdPosStack);	// gosubした時のスタック
	
public:
	CREATE_FUNC(ScriptFileVO);
	
	ScriptFileVO() {
		//CCLOG("ScriptFileVO::alloc - %p", this);
//		parser_ = NULL;
		currentCmdPos_ = 0;
	}
	virtual ~ScriptFileVO() {
		//CCLOG("ScriptFileVO::dealloc - %p", this);
//		set_parser(NULL);
	}
	
	virtual bool init() { return true; }

	bool initWithScript(const std::string &script, const std::string &basePath="", const std::string filename="");

	int getCmdIndexWithLabel(const std::string &label);
	
	ScriptCmd *fetchNextCmd();

private:
	bool _parseScript(ScriptParser *parser);
};

#endif /* ScriptFileVO_h */
