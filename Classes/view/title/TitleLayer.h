//
//  TitleLayer.h
//  PuzzleQuest
//
//  Created by shogo yanuma on 2016/04/08.
//
//

#ifndef TitleLayer_h
#define TitleLayer_h

#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace cocostudio::timeline;

class TitleLayer : public Layer
{
public:
	CREATE_FUNC(TitleLayer);

	TitleLayer() {}
	virtual ~TitleLayer() {}

	virtual bool init();

	void _scriptTest();
};

#endif /* TitleLayer_h */
