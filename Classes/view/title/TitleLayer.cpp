//
//  TitleLayer.cpp
//  PuzzleQuest
//
//  Created by shogo yanuma on 2016/04/08.
//
//

#include "TitleLayer.h"
#include "ScreenModel.h"
#include "ResourceModel.h"

#include "CocosUtil.h"
#include "ScriptFileVO.h"

bool TitleLayer::init()
{
	if (!Layer::init()) {
		return false;
	}

//	Size winSize = Director::getInstance()->getWinSize();
	
	auto rootNode = CSLoader::createNode("title/TitleScene.csb");
	ResourceModel::setToScreenCenter(rootNode);
//	rootNode->setAnchorPoint( Point(0.5, 0.5) );
//	rootNode->setPosition( Point(winSize.width/2, winSize.height/2) );
//	Rect rc = rootNode->getBoundingBox();
	
	addChild(rootNode);
	
	ui::Button *btn;
	btn = (ui::Button*)(rootNode->getChildByName("footer@gv_cb")->getChildByName("btnStart"));
	ResourceModel::autoLayout(btn, kAutoLayout_BC);

	btn->addClickEventListener([this](Ref *sender) {
		ScreenModel::getInstance()->dispHome();
	});

	btn = (ui::Button*)(rootNode->getChildByName("footer@gv_cb")->getChildByName("btnTest"));
	ResourceModel::autoLayout(btn, kAutoLayout_BC);

	btn->addClickEventListener([this](Ref *sender) {
		this->_scriptTest();
	});

	return true;
}

void TitleLayer::_scriptTest()
{
	std::string path = "ja/scripts/";
//	std::string file = "test0.txt";
	std::string file = "test0_1.txt";
	
	std::string str = FileUtils::getInstance()->getStringFromFile( path+file );
	CCLOG("SCRIPT : %s", str.c_str());

	ScriptFileVO *vo = ScriptFileVO::create();
//	vo->retain();
	vo->initWithScript( str, path, file );

	for (int i=0; i<vo->get_commandList().size(); i++) {
		ScriptCmd *cmd = vo->get_commandList().at(i);
		std::string params;
		if (cmd->get_params() != NULL) {
			params = CocosUtil::cocosObjectToJsonString( cmd->get_params() );
		}
		CCLOG("list[%d] - cmd:%d, params:%s", i, cmd->get_cmd(), params.c_str());
	}
}
