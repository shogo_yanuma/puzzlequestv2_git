//
//  StartBattleResVO.cpp
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/06.
//
//

#include "StartBattleResVO.h"


void BattleCardBaseVO::setData(const Json::Value &val)
{
	set_cid( val["cid"].asInt() );
	set_lv( val["lv"].asInt() );
	set_hp( val["hp"].asInt() );
	set_atk( val["atk"].asInt() );
	set_def( val["def"].asInt() );
	set_speed( val["speed"].asInt() );
	set_luck( val["luck"].asInt() );
}

Json::Value BattleCardBaseVO::getData()
{
	Json::Value val;
	val["cid"] = cid_;
	val["lv"] = lv_;
	val["hp"] = hp_;
	val["atk"] = atk_;
	val["def"] = def_;
	val["speed"] = speed_;
	val["luck"] = luck_;
	return val;
}


void BattlePlayerVO::setData(const Json::Value &val)
{
	BattleCardBaseVO::setData(val);

}

Json::Value BattlePlayerVO::getData()
{
	Json::Value val = BattleCardBaseVO::getData();
	//val[""]
	return val;
}


void BattleEnemyVO::setData(const Json::Value &val)
{
	BattleCardBaseVO::setData(val);

}

Json::Value BattleEnemyVO::getData()
{
	Json::Value val = BattleCardBaseVO::getData();
	//val[""]
	return val;
}


void BattleWaveVO::setData(const Json::Value &val)
{
	set_waveType( val["waveType"].asString() );
	set_bgm( val["bgm"].asString() );
	{
		enemies_.clear();
		const Json::Value &arr = val["enemies"];
		for (int i=0; i<arr.size(); i++) {
			BattleEnemyVO *vo = BattleEnemyVO::create();
			vo->setData( arr[i] );
			enemies_.pushBack(vo);
		}
	}
}

Json::Value BattleWaveVO::getData()
{
	Json::Value val;
	val["waveType"] = waveType_;
	val["bgm"] = bgm_;
	{
		Json::Value arr(Json::arrayValue);
		for (int i=0; i<enemies_.size(); i++) {
			BattleEnemyVO *vo = enemies_.at(i);
			const Json::Value &v = vo->getData();
			arr[i] = v;
		}
		val["enemies"] = arr;
	}
	return val;
}


void StartBattleResVO::setData(const Json::Value &val)
{
}

Json::Value StartBattleResVO::getData()
{
	Json::Value val;
	return val;
}
