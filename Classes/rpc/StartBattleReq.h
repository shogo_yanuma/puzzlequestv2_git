//
//  StartBattleReq.h
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/06.
//
//

#ifndef StartBattleReq_h
#define StartBattleReq_h

USING_NS_CC;

class StartBattleReq : public Ref
{
public:

public:
	CREATE_FUNC(StartBattleReq);

	StartBattleReq() {}
	virtual ~StartBattleReq() {}
	virtual bool init() { return true; }
};

#endif /* StartBattleReq_h */
