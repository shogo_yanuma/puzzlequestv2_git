//
//  StartBattleResVO.h
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/06.
//
//

#ifndef StartBattleRes_h
#define StartBattleRes_h

#include "CocosUtil.h"

USING_NS_CC;


class BattleCardBaseVO : public Ref
{
public:
	CC_SYNTHESIZE(int, cid_, _cid);
	CC_SYNTHESIZE(int, lv_, _lv);
	CC_SYNTHESIZE(int, hp_, _hp);
	CC_SYNTHESIZE(int, atk_, _atk);
	CC_SYNTHESIZE(int, def_, _def);
	CC_SYNTHESIZE(int, speed_, _speed);
	CC_SYNTHESIZE(int, luck_, _luck);
	
	virtual void setData(const Json::Value &val);
	virtual Json::Value getData();
};

class BattlePlayerVO : public BattleCardBaseVO
{
public:

public:
	CREATE_FUNC(BattlePlayerVO);

	BattlePlayerVO() {}
	virtual ~BattlePlayerVO() {}
	virtual bool init() { return true; }

	virtual void setData(const Json::Value &val);
	virtual Json::Value getData();
};


class BattleEnemyVO : public BattleCardBaseVO
{
public:
//	CC_SYNTHESIZE();
	
public:
	CREATE_FUNC(BattleEnemyVO);

	BattleEnemyVO() {}
	virtual ~BattleEnemyVO() {}
	virtual bool init() { return true; }

	virtual void setData(const Json::Value &val);
	virtual Json::Value getData();
};


class BattleWaveVO : public Ref
{
public:
	CC_SYNTHESIZE(std::string, waveType_, _waveType);
	CC_SYNTHESIZE(std::string, bgm_, _bgm);
	CC_SYNTHESIZE(cocos2d::Vector<BattleEnemyVO*>, enemies_, _enemies);
	
public:
	CREATE_FUNC(BattleWaveVO);

	BattleWaveVO() {}
	virtual ~BattleWaveVO() {}
	virtual bool init() { return true; }

	void setData(const Json::Value &val);
	Json::Value getData();
};


class StartBattleResVO : public Ref
{
public:
	CC_SYNTHESIZE(cocos2d::Vector<BattlePlayerVO*>, players_, _players);
	CC_SYNTHESIZE(cocos2d::Vector<StartBattleResVO*>, waves_, _waves);
	
public:
	CREATE_FUNC(StartBattleResVO);
	
	StartBattleResVO() {}
	virtual ~StartBattleResVO() {}
	virtual bool init() { return true; }

	void setData(const Json::Value &val);
	Json::Value getData();
};

#endif /* StartBattleResVO_h */
