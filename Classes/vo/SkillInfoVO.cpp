//
//  SkillInfoVO.cpp
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/06.
//
//

#include "SkillInfoVO.h"

void SkillInfoVO::setData(const Json::Value &val)
{
	set_skillid( val["skillid"].asInt() );
	set_name( LangTextVO::createWith( val["name"] ) );
	set_desc( LangTextVO::createWith( val["desc"] ) );
}

Json::Value SkillInfoVO::getData()
{
	Json::Value val;
	val["skillid"] = skillid_;
	if (name_) {
		val["name"] = name_->getData();
	}
	if (desc_) {
		val["desc"] = desc_->getData();
	}
	return val;
}
