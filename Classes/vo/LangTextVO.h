//
//  LangTextVO.h
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/06.
//
//

#ifndef LangTextVO_h
#define LangTextVO_h

#include "CocosUtil.h"

USING_NS_CC;

class LangTextVO : public Ref
{
public:
	CC_SYNTHESIZE(std::string, jp_, _jp);
	CC_SYNTHESIZE(std::string, fr_, _fr);
	
public:
	CREATE_FUNC(LangTextVO);

	static LangTextVO *createWith(const Json::Value &val);
	
	LangTextVO() {}
	virtual ~LangTextVO() {}
	virtual bool init() { return true; }
	
	void setData(const Json::Value &val);
	Json::Value getData();
};

#endif /* LangTextVO_h */
