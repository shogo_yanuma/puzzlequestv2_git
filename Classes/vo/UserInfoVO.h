//
//  UserInfoVO.h
//  PuzzleQuest
//
//  Created by shogo yanuma on 2016/04/11.
//
//

#ifndef UserInfoVO_h
#define UserInfoVO_h

#include "CocosUtil.h"
#include "SupportItemInfoVO.h"

USING_NS_CC;

class UserInfoVO : public Ref
{
public:
	CC_SYNTHESIZE(int, rank_, _rank);
	CC_SYNTHESIZE(std::string, nickname_, _nickname);
	CC_SYNTHESIZE(std::string, user_type_, _user_type);	// N:通常/S:システムユーザ/T:テスター
	CC_SYNTHESIZE(int, exp_, _exp);
	CC_SYNTHESIZE(int, min_exp_, _min_exp);
	CC_SYNTHESIZE(int, max_exp_, _max_exp);
	CC_SYNTHESIZE(int, gold_, _gold);
	CC_SYNTHESIZE(int, paid_coin_, _paid_coin);
	CC_SYNTHESIZE(int, free_coin_, _free_coin);

	CC_SYNTHESIZE(cocos2d::Vector<SupportItemInfoVO*>, items_, _items);

public:
	CREATE_FUNC(UserInfoVO);

	UserInfoVO() {}
	virtual ~UserInfoVO() {}
	virtual bool init() { return true; }

	void setData(const Json::Value &val);
	Json::Value getData();
};

#endif /* UserInfoVO_h */
