//
//  CardMsgVO.cpp
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/05.
//
//

#include "CardMsgVO.h"

void CardMsgVO::setData(const Json::Value &val)
{
	set_msg( LangTextVO::createWith( val["msg"] ) );
}

Json::Value CardMsgVO::getData()
{
	Json::Value val;
	if (msg_) {
		val["msg"] = msg_->getData();
	}
	return val;
}
