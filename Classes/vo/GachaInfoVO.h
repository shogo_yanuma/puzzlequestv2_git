//
//  GachaInfoVO.h
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/05.
//
//

#ifndef GachaInfoVO_h
#define GachaInfoVO_h

#include "CocosUtil.h"
#include "CardInfoVO.h"

USING_NS_CC;

class GachaInfoVO : public Ref
{
public:
	CC_SYNTHESIZE(int, series_id_, _series_id);
	CC_SYNTHESIZE(int, gid_, _gid);
	CC_SYNTHESIZE(int, category_, _category);	// 0:通常
	CC_SYNTHESIZE(std::string, name_, _name);
	CC_SYNTHESIZE(long long, start_time_, _start_time);
	CC_SYNTHESIZE(long long, end_time_, _end_time);

	CC_SYNTHESIZE(cocos2d::Vector<CardInfoVO*>, cards_, _cards);

public:
	CREATE_FUNC(GachaInfoVO);
	
	GachaInfoVO() {}
	virtual ~GachaInfoVO() {}
	
	virtual bool init() { return true; }
	
	void setData(const Json::Value &val);
	Json::Value getData();
};


#endif /* GachaInfoVO_h */
