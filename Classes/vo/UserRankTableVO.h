//
//  UserRankTableVO.h
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/06.
//
//

#ifndef UserRankTableVO_h
#define UserRankTableVO_h

#include "CocosUtil.h"

USING_NS_CC;

class UserRankTableVO : public Ref
{
public:
	CC_SYNTHESIZE(int, rank_, _rank);
	CC_SYNTHESIZE(int, minexp_, _minexp);
	CC_SYNTHESIZE(int, maxexp_, _maxexp);
	
public:
	CREATE_FUNC(UserRankTableVO);
	
	UserRankTableVO() {}
	virtual ~UserRankTableVO() {}
	virtual bool init() { return true; }
	
	void setData(const Json::Value &val);
	Json::Value getData();
};

#endif /* UserRankTableVO_h */
