//
//  MusicInfoVO.cpp
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/10.
//
//

#include "MusicInfoVO.h"

#pragma mark - MusicInfo_SummaryVO
void MusicInfo_SummaryVO::setData(const Json::Value &val)
{
	set_bpm( val["bpm"].asInt() );
	set_pixel_per_measure( val["pixel_per_measure"].asInt() );
	set_beats( val["beats"].asInt() );
}

Json::Value MusicInfo_SummaryVO::getData()
{
	Json::Value val;
	val["bpm"] = bpm_;
	val["pixel_per_measure"] = pixel_per_measure_;
	val["beats"] = beats_;
	return val;
}


#pragma mark - MusicInfo_SoundVO
void MusicInfo_SoundVO::setData(const Json::Value &val)
{
	set_sid( val["sid"].asInt() );
	set_se( val["se"].asString() );
}

Json::Value MusicInfo_SoundVO::getData()
{
	Json::Value val;
	val["sid"] = sid_;
	val["se"] = se_;
	return val;
}


#pragma mark - MusicInfo_MovieVO
void MusicInfo_MovieVO::setData(const Json::Value &val)
{
	set_movid( val["movid"].asInt() );
	set_mov( val["mov"].asString() );
	set_fps( val["fps"].asInt() );
}

Json::Value MusicInfo_MovieVO::getData()
{
	Json::Value val;
	val["movid"] = movid_;
	val["mov"] = mov_;
	val["fps"] = fps_;
	return val;
}


#pragma mark - MusicInfo_StepVO
void MusicInfo_StepVO::setData(const Json::Value &val)
{
	set_n( val["n"].asString() );
	set_s( val["s"].asInt() );
	set_sids( val["sids"].asString() );
	if (val["movid"].isNull()) { set_movid(0); }
	else { set_movid(val["movid"].asInt()); }
	
	if (n_.size() > 0) {
		for (int i=0; i<4; i++) {
			_notes[i] = n_[i];
		}
	}
	else {
		for (int i=0; i<4; i++) {
			_notes[i] = '.';
		}
	}
	
	if (sids_.size() > 0) {
		std::vector<std::string> arr = CocosUtil::split(sids_, '-');
		for (int i=0; i<4; i++) {
			_soundIDs[i] = atoi( arr[i].c_str() );
		}
	}
	else {
		for (int i=0; i<4; i++) {
			_soundIDs[i] = 0;
		}
	}
}

Json::Value MusicInfo_StepVO::getData()
{
	Json::Value val;
	val["n"] = n_;
	val["s"] = s_;
	val["sids"] = sids_;
	val["movid"] = movid_;
	return val;
}


#pragma mark - MusicInfoVO
void MusicInfoVO::setData(const Json::Value &val)
{
	{
		MusicInfo_SummaryVO *vo = MusicInfo_SummaryVO::create();
		vo->setData( val["summary"] );
		set_summary( vo );
	}

	{
		sounds_.clear();
		const Json::Value &arr = val["sounds"];
		for (int i=0; i<arr.size(); i++) {
			MusicInfo_SoundVO *vo = MusicInfo_SoundVO::create();
			vo->setData(arr[i]);
			sounds_.pushBack(vo);
		}
	}

	{
		movies_.clear();
		const Json::Value &arr = val["movies"];
		for (int i=0; i<arr.size(); i++) {
			MusicInfo_MovieVO *vo = MusicInfo_MovieVO::create();
			vo->setData(arr[i]);
			movies_.pushBack(vo);
		}
	}

	{
		steps_.clear();
		const Json::Value &arr = val["steps"];
		for (int i=0; i<arr.size(); i++) {
			MusicInfo_StepVO *vo = MusicInfo_StepVO::create();
			if (vo->get_n().size() == 0) {
				continue;
			}

			vo->setData(arr[i]);
			steps_.pushBack(vo);
		}
	}

	setupAllSoundAndMovieIds();
}

Json::Value MusicInfoVO::getData()
{
	Json::Value val;

	if (summary_) {
		val["summary"] = summary_->getData();
	}
	
	{
		Json::Value arr(Json::arrayValue);
		for (int i=0; i<sounds_.size(); i++) {
			MusicInfo_SoundVO *vo = sounds_.at(i);
			const Json::Value &v = vo->getData();
			arr[i] = v;
		}
		val["sounds"]  = arr;
	}

	{
		Json::Value arr(Json::arrayValue);
		for (int i=0; i<movies_.size(); i++) {
			MusicInfo_MovieVO *vo = movies_.at(i);
			const Json::Value &v = vo->getData();
			arr[i] = v;
		}
		val["movies"]  = arr;
	}

	{
		Json::Value arr(Json::arrayValue);
		for (int i=0; i<steps_.size(); i++) {
			MusicInfo_StepVO *vo = steps_.at(i);
			const Json::Value &v = vo->getData();
			arr[i] = v;
		}
		val["steps"]  = arr;
	}

	return val;
}

void MusicInfoVO::setupAllSoundAndMovieIds()
{
	int lastSIDs[4] = {0,0,0,0};
	int lastMovID = 0;
	for (int i=0; i<steps_.size(); i++) {
		MusicInfo_StepVO *vo = steps_.at(i);
		for (int p=0; p<4; p++) {
			if (vo->_soundIDs[p] <= 0) {
				vo->_soundIDs[p] = lastSIDs[p];
			}
			else {
				lastSIDs[p] = vo->_soundIDs[p];
			}
		}

		if (vo->get_movid() <= 0) {
			vo->set_movid(lastMovID);
		}
		else {
			lastMovID = vo->get_movid();
		}
	}
}
