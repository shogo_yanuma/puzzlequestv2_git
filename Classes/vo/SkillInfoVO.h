//
//  SkillInfoVO.h
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/06.
//
//

#ifndef SkillInfoVO_h
#define SkillInfoVO_h

#include "CocosUtil.h"
#include "LangTextVO.h"

USING_NS_CC;

class SkillInfoVO : public Ref
{
public:
	CC_SYNTHESIZE(int, skillid_, _skillid);
	CC_SYNTHESIZE_RETAIN(LangTextVO*, name_, _name);
	CC_SYNTHESIZE_RETAIN(LangTextVO*, desc_, _desc);
	
public:
	CREATE_FUNC(SkillInfoVO);
	
	SkillInfoVO() {
		name_ = NULL;
		desc_ = NULL;
	}
	virtual ~SkillInfoVO() {
		set_name(NULL);
		set_desc(NULL);
	}
	virtual bool init() { return true; }
	
	void setData(const Json::Value &val);
	Json::Value getData();
};

#endif /* SkillInfoVO_h */
