//
//  SupportItemInfoVO.h
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/05.
//
//

#ifndef SupportItemInfoVO_h
#define SupportItemInfoVO_h

#include "CocosUtil.h"

USING_NS_CC;

class SupportItemInfoVO : public Ref
{
public:
	CC_SYNTHESIZE(int, itemno_, _itemno);
	CC_SYNTHESIZE(std::string, itemname_, _itemname);
	CC_SYNTHESIZE(std::string, detail_, _detail);
	CC_SYNTHESIZE(int, cnt_, _cnt);
	CC_SYNTHESIZE(int, max_cnt_, _max_cnt);
	CC_SYNTHESIZE(long long, expire_time_, _expire_time);
	
public:
	CREATE_FUNC(SupportItemInfoVO);

	SupportItemInfoVO() {}
	virtual ~SupportItemInfoVO() {}
	virtual bool init() { return true; }

	void setData(const Json::Value &val);
	Json::Value getData();
};

#endif /* SupportItemInfoVO_h */
