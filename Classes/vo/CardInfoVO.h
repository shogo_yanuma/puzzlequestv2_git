//
//  CardInfoVO.h
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/05.
//
//

#ifndef CardInfoVO_h
#define CardInfoVO_h

#include "CocosUtil.h"
#include "LangTextVO.h"
#include "CardMsgVO.h"

USING_NS_CC;

class CardInfoVO : public Ref
{
public:
	CC_SYNTHESIZE(int, cid_, _cid);
	CC_SYNTHESIZE(std::string, attr_, _attr);	// R/G/B/L/D
	CC_SYNTHESIZE(int, rare_, _rare);
	CC_SYNTHESIZE_RETAIN(LangTextVO*, name_, _name);
	CC_SYNTHESIZE_RETAIN(LangTextVO*, desc_, _desc);
	CC_SYNTHESIZE(int, max_lv_, _max_lv);
	CC_SYNTHESIZE(int, min_hp_, _min_hp);
	CC_SYNTHESIZE(int, max_hp_, _max_hp);
	CC_SYNTHESIZE(int, min_atk_, _min_atk);
	CC_SYNTHESIZE(int, max_atk_, _max_atk);
	CC_SYNTHESIZE(int, min_def_, _min_def);
	CC_SYNTHESIZE(int, max_def_, _max_def);
	CC_SYNTHESIZE(int, speed_, _speed);
	CC_SYNTHESIZE(int, luck_, _luck);

	CC_SYNTHESIZE(cocos2d::Vector<CardMsgVO*>, home_msg_, _home_msg);
	CC_SYNTHESIZE(cocos2d::Vector<CardMsgVO*>, start_msg_, _start_msg);
	CC_SYNTHESIZE(cocos2d::Vector<CardMsgVO*>, atk_msg_, _atk_msg);
	CC_SYNTHESIZE(cocos2d::Vector<CardMsgVO*>, dmg_msg_, _dmg_msg);
	CC_SYNTHESIZE(cocos2d::Vector<CardMsgVO*>, dead_msg_, _dead_msg);
	CC_SYNTHESIZE(cocos2d::Vector<CardMsgVO*>, win_msg_, _win_msg);
	CC_SYNTHESIZE(cocos2d::Vector<CardMsgVO*>, lvup_msg_, _lvup_msg);
	CC_SYNTHESIZE(cocos2d::Vector<CardMsgVO*>, evo_msg_, _evo_msg);
	
public:
	CREATE_FUNC(CardInfoVO);

	CardInfoVO() {
		name_ = NULL;
		desc_ = NULL;
	}
	virtual ~CardInfoVO() {
		set_name( NULL );
		set_desc( NULL );
	}
	virtual bool init() { return true; }

	void setData(const Json::Value &val);
	Json::Value getData();
};

#endif /* CardInfoVO_h */
