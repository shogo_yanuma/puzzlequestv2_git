//
//  UserRankTableVO.cpp
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/06.
//
//

#include "UserRankTableVO.h"

void UserRankTableVO::setData(const Json::Value &val)
{
	set_rank( val["rank"].asInt() );
	set_minexp( val["minexp"].asInt() );
	set_maxexp( val["maxexp"].asInt() );
}

Json::Value UserRankTableVO::getData()
{
	Json::Value val;
	val["rank"] = rank_;
	val["minexp"] = minexp_;
	val["maxexp"] = maxexp_;
	return val;
}
