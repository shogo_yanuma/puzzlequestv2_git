//
//  LangTextVO.cpp
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/06.
//
//

#include "LangTextVO.h"

LangTextVO *LangTextVO::createWith(const Json::Value &val)
{
	LangTextVO *vo = LangTextVO::create();
	vo->setData(val);
	return vo;
}

void LangTextVO::setData(const Json::Value &val)
{
	set_jp( val["jp"].asString() );
	set_fr( val["fr"].asString() );
}

Json::Value LangTextVO::getData()
{
	Json::Value val;
	val["jp"] = jp_;
	val["fr"] = fr_;
	return val;
}
