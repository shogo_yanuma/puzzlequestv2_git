//
//  CardInfoVO.cpp
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/05.
//
//

#include "CardInfoVO.h"

void CardInfoVO::setData(const Json::Value &val)
{
	set_cid( val["cid"].asInt() );
	set_attr( val["attr"].asString() );
	set_rare( val["rare"].asInt() );
	set_name( LangTextVO::createWith( val["name"] ) );
	set_desc( LangTextVO::createWith( val["desc"] ) );
	set_max_lv( val["max_lv"].asInt() );
	set_min_hp( val["min_hp"].asInt() );
	set_max_hp( val["max_hp"].asInt() );
	set_min_atk( val["min_atk"].asInt() );
	set_max_atk( val["max_atk"].asInt() );
	set_min_def( val["min_def"].asInt() );
	set_max_def( val["max_def"].asInt() );
	set_speed( val["speed"].asInt() );
	set_luck( val["luck"].asInt() );

	{
		home_msg_.clear();
		const Json::Value &arr = val["home_msg"];
		for (int i=0; i<arr.size(); i++) {
			const Json::Value &v = arr[i];
			CardMsgVO *vo = CardMsgVO::create();
			vo->setData(v);
			home_msg_.pushBack(vo);
		}
	}

	{
		start_msg_.clear();
		const Json::Value &arr = val["start_msg"];
		for (int i=0; i<arr.size(); i++) {
			const Json::Value &v = arr[i];
			CardMsgVO *vo = CardMsgVO::create();
			vo->setData(v);
			start_msg_.pushBack(vo);
		}
	}

	{
		atk_msg_.clear();
		const Json::Value &arr = val["atk_msg"];
		for (int i=0; i<arr.size(); i++) {
			const Json::Value &v = arr[i];
			CardMsgVO *vo = CardMsgVO::create();
			vo->setData(v);
			atk_msg_.pushBack(vo);
		}
	}

	{
		dmg_msg_.clear();
		const Json::Value &arr = val["dmg_msg"];
		for (int i=0; i<arr.size(); i++) {
			const Json::Value &v = arr[i];
			CardMsgVO *vo = CardMsgVO::create();
			vo->setData(v);
			dmg_msg_.pushBack(vo);
		}
	}

	{
		dead_msg_.clear();
		const Json::Value &arr = val["dead_msg"];
		for (int i=0; i<arr.size(); i++) {
			const Json::Value &v = arr[i];
			CardMsgVO *vo = CardMsgVO::create();
			vo->setData(v);
			dead_msg_.pushBack(vo);
		}
	}

	{
		win_msg_.clear();
		const Json::Value &arr = val["win_msg"];
		for (int i=0; i<arr.size(); i++) {
			const Json::Value &v = arr[i];
			CardMsgVO *vo = CardMsgVO::create();
			vo->setData(v);
			win_msg_.pushBack(vo);
		}
	}

	{
		lvup_msg_.clear();
		const Json::Value &arr = val["lvup_msg"];
		for (int i=0; i<arr.size(); i++) {
			const Json::Value &v = arr[i];
			CardMsgVO *vo = CardMsgVO::create();
			vo->setData(v);
			lvup_msg_.pushBack(vo);
		}
	}
	
	{
		evo_msg_.clear();
		const Json::Value &arr = val["evo_msg"];
		for (int i=0; i<arr.size(); i++) {
			const Json::Value &v = arr[i];
			CardMsgVO *vo = CardMsgVO::create();
			vo->setData(v);
			evo_msg_.pushBack(vo);
		}
	}
}

Json::Value CardInfoVO::getData()
{
	Json::Value val;

	val["cid"] = cid_;
	val["attr"] = attr_;
	val["rare"] = rare_;
	if (name_) {
		val["name"] = name_->getData();
	}
	if (desc_) {
		val["desc"] = desc_->getData();
	}
	val["max_lv"] = max_lv_;
	val["min_hp"] = min_hp_;
	val["max_hp"] = max_hp_;
	val["min_atk"] = min_atk_;
	val["max_atk"] = max_atk_;
	val["min_def"] = min_def_;
	val["max_def"] = max_def_;
	val["speed"] = speed_;
	val["luck"] = luck_;

	{
		Json::Value arr(Json::arrayValue);
		for (int i=0; i<home_msg_.size(); i++) {
			CardMsgVO *vo = home_msg_.at(i);
			arr[i] = vo->getData();
		}
		val["home_msg"] = arr;
	}

	{
		Json::Value arr(Json::arrayValue);
		for (int i=0; i<home_msg_.size(); i++) {
			CardMsgVO *vo = home_msg_.at(i);
			arr[i] = vo->getData();
		}
		val["home_msg"] = arr;
	}

	{
		Json::Value arr(Json::arrayValue);
		for (int i=0; i<start_msg_.size(); i++) {
			CardMsgVO *vo = start_msg_.at(i);
			arr[i] = vo->getData();
		}
		val["start_msg"] = arr;
	}

	{
		Json::Value arr(Json::arrayValue);
		for (int i=0; i<atk_msg_.size(); i++) {
			CardMsgVO *vo = atk_msg_.at(i);
			arr[i] = vo->getData();
		}
		val["atk_msg"] = arr;
	}

	{
		Json::Value arr(Json::arrayValue);
		for (int i=0; i<dmg_msg_.size(); i++) {
			CardMsgVO *vo = dmg_msg_.at(i);
			arr[i] = vo->getData();
		}
		val["dmg_msg"] = arr;
	}

	{
		Json::Value arr(Json::arrayValue);
		for (int i=0; i<dead_msg_.size(); i++) {
			CardMsgVO *vo = dead_msg_.at(i);
			arr[i] = vo->getData();
		}
		val["dead_msg"] = arr;
	}

	{
		Json::Value arr(Json::arrayValue);
		for (int i=0; i<win_msg_.size(); i++) {
			CardMsgVO *vo = win_msg_.at(i);
			arr[i] = vo->getData();
		}
		val["win_msg"] = arr;
	}

	{
		Json::Value arr(Json::arrayValue);
		for (int i=0; i<lvup_msg_.size(); i++) {
			CardMsgVO *vo = lvup_msg_.at(i);
			arr[i] = vo->getData();
		}
		val["lvup_msg"] = arr;
	}

	{
		Json::Value arr(Json::arrayValue);
		for (int i=0; i<evo_msg_.size(); i++) {
			CardMsgVO *vo = evo_msg_.at(i);
			arr[i] = vo->getData();
		}
		val["evo_msg"] = arr;
	}
	
	return val;
}
