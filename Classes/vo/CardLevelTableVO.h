//
//  CardLevelTableVO.h
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/06.
//
//

#ifndef CardLevelTableVO_h
#define CardLevelTableVO_h

#include "CocosUtil.h"

USING_NS_CC;

class CardLevelTableVO : public Ref
{
public:
	CC_SYNTHESIZE(int, tno_, _tno);
	CC_SYNTHESIZE(int, lv_, _lv);
	CC_SYNTHESIZE(int, minexp_, _minexp);
	CC_SYNTHESIZE(int, maxexp_, _maxexp);
	
public:
	CREATE_FUNC(CardLevelTableVO);
	
	CardLevelTableVO() {}
	virtual ~CardLevelTableVO() {}
	virtual bool init() { return true; }
	
	void setData(const Json::Value &val);
	Json::Value getData();
};

#endif /* CardLevelTableVO_h */
