//
//  CardLevelTableVO.cpp
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/06.
//
//

#include "CardLevelTableVO.h"

void CardLevelTableVO::setData(const Json::Value &val)
{
	set_tno( val["tno"].asInt() );
	set_lv( val["lv"].asInt() );
	set_minexp( val["minexp"].asInt() );
	set_maxexp( val["maxexp"].asInt() );
}

Json::Value CardLevelTableVO::getData()
{
	Json::Value val;
	val["tno"] = tno_;
	val["lv"] = lv_;
	val["minexp"] = minexp_;
	val["maxexp"] = maxexp_;
	return val;
}
