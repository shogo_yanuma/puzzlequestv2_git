//
//  MusicInfoVO.h
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/10.
//
//

#ifndef MusicInfoVO_h
#define MusicInfoVO_h

#include "CocosUtil.h"
#include "AppDefines.h"

USING_NS_CC;


#pragma mark - MusicInfo_SummaryVO
class MusicInfo_SummaryVO : public Ref
{
public:
	CC_SYNTHESIZE(int, bpm_, _bpm);	// 1分間に何拍か
	CC_SYNTHESIZE(int, pixel_per_measure_, _pixel_per_measure);	// 1小節のピクセル数
	CC_SYNTHESIZE(int, beats_, _beats);	// 1小節が何拍か
	
public:
	CREATE_FUNC(MusicInfo_SummaryVO);
	
	MusicInfo_SummaryVO() {}
	virtual ~MusicInfo_SummaryVO() {}
	virtual bool init() { return true; }
	
	void setData(const Json::Value &val);
	Json::Value getData();
};


#pragma mark - MusicInfo_SoundVO
class MusicInfo_SoundVO : public Ref
{
public:
	CC_SYNTHESIZE(int, sid_, _sid);	// sound id
	CC_SYNTHESIZE(std::string, se_, _se);	// sound file
	
public:
	CREATE_FUNC(MusicInfo_SoundVO);
	
	MusicInfo_SoundVO() {}
	virtual ~MusicInfo_SoundVO() {}
	virtual bool init() { return true; }
	
	void setData(const Json::Value &val);
	Json::Value getData();
};


#pragma mark - MusicInfo_MovieVO
class MusicInfo_MovieVO : public Ref
{
public:
	CC_SYNTHESIZE(int, movid_, _movid);	// movie id
	CC_SYNTHESIZE(std::string, mov_, _mov);	// movie file
	CC_SYNTHESIZE(int, fps_, _fps);
	
public:
	CREATE_FUNC(MusicInfo_MovieVO);
	
	MusicInfo_MovieVO() {}
	virtual ~MusicInfo_MovieVO() {}
	virtual bool init() { return true; }
	
	void setData(const Json::Value &val);
	Json::Value getData();
};


#pragma mark - MusicInfo_StepVO
class MusicInfo_StepVO : public Ref
{
public:
	CC_SYNTHESIZE(std::string, n_, _n);	// note
	CC_SYNTHESIZE(int, s_, _s);	// speed 1:標準/2:2拍/3:3拍
	CC_SYNTHESIZE(std::string, sids_, _sids);
	CC_SYNTHESIZE(int, movid_, _movid);	// speed 1:標準/2:2拍/3:3拍
	
	char _notes[4];		// '.':ステップなし/'x':ステップあり/'|':伸ばし
	int _soundIDs[4];

	bool _visible;
	ENUM_STEP_RESULT _result;
	float _resultDiff;

	
public:
	CREATE_FUNC(MusicInfo_StepVO);

	MusicInfo_StepVO() {
		s_ = 1;
	}
	virtual ~MusicInfo_StepVO() {}
	virtual bool init() { return true; }

	void setData(const Json::Value &val);
	Json::Value getData();
};


#pragma mark - MusicInfoVO
class MusicInfoVO : public Ref
{
public:
	CC_SYNTHESIZE_RETAIN(MusicInfo_SummaryVO*, summary_, _summary);
	CC_SYNTHESIZE(cocos2d::Vector<MusicInfo_SoundVO*>, sounds_, _sounds);
	CC_SYNTHESIZE(cocos2d::Vector<MusicInfo_MovieVO*>, movies_, _movies);
	CC_SYNTHESIZE(cocos2d::Vector<MusicInfo_StepVO*>, steps_, _steps);
	
public:
	CREATE_FUNC(MusicInfoVO);
	
	MusicInfoVO() {
		summary_ = NULL;
	}
	virtual ~MusicInfoVO() {
		set_summary(NULL);
	}
	virtual bool init() { return true; }
	
	void setData(const Json::Value &val);
	Json::Value getData();

	void setupAllSoundAndMovieIds();
};

#endif /* MusicInfoVO_h */
