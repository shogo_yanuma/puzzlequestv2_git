//
//  SupportItemInfoVO.cpp
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/05.
//
//

#include "SupportItemInfoVO.h"

void SupportItemInfoVO::setData(const Json::Value &val)
{
	set_itemno( val["itemno"].asInt() );
	set_itemname( val["itemname"].asString() );
	set_detail( val["detail"].asString() );
	set_cnt( val["cnt"].asInt() );
	set_max_cnt( val["max_cnt"].asInt() );
	set_expire_time( val["expire_time"].asLargestInt() );
}

Json::Value SupportItemInfoVO::getData()
{
	Json::Value val;
	val["itemno"] = itemno_;
	val["itemanem"] = itemname_;
	val["detail"] = detail_;
	val["cnt"] = cnt_;
	val["max_cnt"] = max_cnt_;
	val["expire_time"] = expire_time_;
	return val;
}
