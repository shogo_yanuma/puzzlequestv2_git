//
//  CardMsgVO.h
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/05.
//
//

#ifndef CardMsgVO_h
#define CardMsgVO_h

#include "CocosUtil.h"
#include "LangTextVO.h"

USING_NS_CC;

class CardMsgVO : public Ref
{
public:
	CC_SYNTHESIZE_RETAIN(LangTextVO*, msg_, _msg);
//	CC_SYNTHESIZE(std::string, msg_fr_, _msg_fr);
	
public:
	CREATE_FUNC(CardMsgVO);

	CardMsgVO() {
		msg_ = NULL;
	}
	virtual ~CardMsgVO() {
		set_msg(NULL);
	}
	
	virtual bool init() { return true; }

	void setData(const Json::Value &val);
	Json::Value getData();
};

#endif /* CardMsgVO_h */
