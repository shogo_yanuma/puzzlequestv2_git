//
//  CNativeBridge.h
//  FlConv
//
//  Created by shogo yanuma on 2014/04/27.
//
//

#ifndef __FlConv__CNativeBridge__
#define __FlConv__CNativeBridge__

#include "cocos2d.h"
#include "NativeBridgeDefines.h"
//#include "PDArray.h"

#pragma mark - CFileInfoVO

class CFileInfoVO : public cocos2d::Ref
{
public:
	typedef enum {
		kInfoType_UNKNOWN	= 0,
		kInfoType_FILE,
		kInfoType_DIRECTORY,
	} ENUM_INFO_TYPE;
	
public:
	CC_SYNTHESIZE(std::string, name_, _name);
	CC_SYNTHESIZE(int, size_, _size);
	CC_SYNTHESIZE(ENUM_INFO_TYPE, type_, _type);
	
public:
	CREATE_FUNC(CFileInfoVO);

	CFileInfoVO() {
		size_ = 0;
		type_ = kInfoType_UNKNOWN;
	}
	virtual ~CFileInfoVO() {}
	virtual bool init() { return true; }
};


#pragma mark - CNativeBridge
class CNativeBridge : public cocos2d::Ref
{
public:

public:
	static CNativeBridge *getInstance();

	CREATE_FUNC(CNativeBridge);

	CNativeBridge();
	virtual ~CNativeBridge();
	virtual bool init();

	std::string getDeviceData(ENUM_DEVICE_DATA_TYPE type, const std::string &param);
	void callDeviceFunc(ENUM_DEVICE_FUNC func, const std::string &param);

	std::string getCPPData(ENUM_CPP_DATA_TYPE type, const std::string &param);
	void callCPPFunc(ENUM_CPP_FUNC func, const std::string &param);

public:
	// @return Array of CFileInfoVO
//	PDArray *enumlateDirectoryItems(const std::string &path);
	
private:
//	void _urlResponse(int tag, bool success, const std::string &response);
};

#endif /* defined(__FlConv__CNativeBridge__) */
