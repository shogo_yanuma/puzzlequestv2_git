//
//  ResourceModel.cpp
//  FlConv
//
//  Created by shogo yanuma on 2014/05/11.
//
//

#include "ResourceModel.h"
//#include "UserDataModel.h"
#include "CNativeBridge.h"
#include "json.h"

USING_NS_CC;

ResourceModel *ResourceModel::g_instance = NULL;
std::string ResourceModel::g_appDir;
std::string ResourceModel::g_resourceDir;
std::string ResourceModel::g_docDir;
std::string ResourceModel::g_cacheDir;
std::string ResourceModel::g_tempDir;

//static
ResourceModel *ResourceModel::getInstance()
{
	if (g_instance == NULL) {
		g_instance = ResourceModel::create();
	}
	return g_instance;
}

ResourceModel::ResourceModel()
{
	localizedStringMap_ = NULL;
	defaultDesignResolution_ = Size(640, 960);	// ディフォルトではiOSサイズを基準とする
}

ResourceModel::~ResourceModel()
{
	set_localizedStringMap(NULL);
	
	if (g_instance == this) {
		g_instance = NULL;
	}
}

bool ResourceModel::init()
{
	return true;
}

//static
std::string ResourceModel::appDir() {
	if (g_appDir == "") { g_appDir = CNativeBridge::getInstance()->getDeviceData(kDeviceDataType_APP_DIR, ""); }
	return g_appDir;
}

//static
std::string ResourceModel::resourceDir() {
	if (g_resourceDir == "") { g_resourceDir = CNativeBridge::getInstance()->getDeviceData(kDeviceDataType_RESOURCE_DIR, ""); }
	return g_resourceDir;
}

//static
std::string ResourceModel::docDir() {
	if (g_docDir == "") { g_docDir = CNativeBridge::getInstance()->getDeviceData(kDeviceDataType_DOC_DIR, ""); }
	return g_docDir;
}

//static
std::string ResourceModel::cacheDir() {
	if (g_cacheDir == "") { g_cacheDir = CNativeBridge::getInstance()->getDeviceData(kDeviceDataType_CACHE_DIR, ""); }
	return g_cacheDir;
}

//static
std::string ResourceModel::tempDir() {
	if (g_tempDir == "") { g_tempDir = CNativeBridge::getInstance()->getDeviceData(kDeviceDataType_TEMP_DIR, ""); }
	return g_tempDir;
}

//static
std::string ResourceModel::localFilePath(const std::string &path)
{
	const std::string resDir = ResourceModel::resourceDir();
//	std::string lang = UserDataModel::getInstance()->get_userConfig()->get_language();
	
//	std::string ret = resDir + "/" + lang + "/" + path;
	std::string ret = resDir + "/" + path;
	return ret;
}

//static
std::string ResourceModel::readFromFile(const std::string &path)
{
	std::string ret = FileUtils::getInstance()->getStringFromFile(path);
	return ret;
}

//static
void ResourceModel::writeToFile(const std::string &path, const std::string &data)
{
	Json::Value val;
	val["path"] = path;
	val["data"] = data;
	
	Json::FastWriter writer;
	std::string param = writer.write(val);
	
	CNativeBridge::getInstance()->callDeviceFunc(kDeviceFunc_SAVE_FILE, param);
}

//static
std::string ResourceModel::labelFontName(ENUM_LABEL_FONT_TYPE type)
{
	std::string ret = "Helvetica";
	switch (type)
	{
//		case kLabelFontType_DEFAULT:	ret = "Helvetica";		break;
//		case kLabelFontType_BOLD:		ret = "Helvetica-Bold";	break;
		case kLabelFontType_DEFAULT:	ret = "fonts/mplus-1c-regular.ttf";	break;
		case kLabelFontType_BOLD:		ret = "fonts/mplus-1c-bold.ttf";		break;
		case kLabelFontType_DEFAULT_BMFONT:	ret = "fonts/pq_number.fnt";	break;
		case kLabelFontType_SMALL_BMFONT:	ret = "fonts/pq_small.fnt";	break;
		default:	break;
	}
	
	return ret;
}

//static
cocos2d::Label *ResourceModel::defaultLabelTTF(const std::string &str, float fontSize)
{
//    auto label = LabelTTF::create(str, "Arial", fontSize);
	std::string fontName = labelFontName( kLabelFontType_DEFAULT );
//	auto *label = LabelTTF::create(str.c_str(), fontName, fontSize);
	Label *label = Label::createWithTTF(str, fontName, fontSize);
	return label;
}

//static
cocos2d::Label *ResourceModel::boldLabelTTF(const std::string &str, float fontSize)
{
	std::string fontName = labelFontName( kLabelFontType_BOLD );
//	auto *label = LabelTTF::create(str.c_str(), fontName, fontSize);
	Label *label = Label::createWithTTF(str, fontName, fontSize);
	return label;
}

//static
cocos2d::Label *ResourceModel::defaultLabelBMP(const std::string &str)
{
	std::string fontName = labelFontName(kLabelFontType_DEFAULT_BMFONT);
	auto *label = Label::createWithBMFont(ResourceModel::localFilePath(fontName), str);
	return label;
}

//static
cocos2d::Label *ResourceModel::smallLabelBMP(const std::string &str)
{
	std::string fontName = labelFontName(kLabelFontType_SMALL_BMFONT);
	auto *label = Label::createWithBMFont(ResourceModel::localFilePath(fontName), str);
	return label;
}


//======================= 文字タイプ判別関数 ========================
int isAlpha(int c) { return (isalpha(c) || c == '_' || c == '.'); }
int isAlphaNumeric(int c) { return isAlpha(c) || isdigit(c); }
int isSymbol(int c) { return c > ' ' && !isAlphaNumeric(c); }

PDDictionary *ResourceModel::loadLocalizedStrings(const std::string &path)
{
	PDArray *newArr = PDArray::create();

	std::string data = FileUtils::getInstance()->getStringFromFile( path );
	
	int q, c, bgn;
	int nText = data.size();
	//		char *Text[nText] = '\0';
	const char *Text = data.c_str();
	for (int n = 0; n < nText; ) {
		if (n+1 < nText && (Text[n]=='/' && Text[n+1]=='/')) {      // コメント
			for (n += 2; n < nText && Text[n] != '\n'; n++) ;
		} else if (n+1 < nText && Text[n]=='/' && Text[n+1]=='*') { /* コメント */
			for (n += 2; n-1 < nText && (Text[n-2] != '*' || Text[n-1] != '/'); n++) ;
		} else if (Text[n] <= ' ') {                                // 空白文字
			n++;
		} else if ((q=Text[n]) == '\"' || q == '\'') {              // "xxx" または 'xxx'
			for (bgn = ++n; n < nText && Text[n] != q; n++) {
				c = (unsigned char)Text[n];
				if (c == '\\' || (0x80<=c && c<=0x9F) || (0xE0<=c && c<=0xFF)) n++;
			}    // escape文字,２byte文字のとき２byte目をスキップ
			PDValue *val = PDValue::createAsString( Text+bgn-1, n-bgn+2 );
			newArr->addObject(val);
			//				setToken(TK_STRING, Text+bgn-1, n-bgn+2);
			n++;        // 後ろの '\"'または'\'' をスキップ
		} else if (isdigit(Text[n])) {                              // 数値
			for (bgn = n; n < nText && !isSymbol(Text[n]); ) {
				if ((c=Text[n++]) == 'E' || c == 'e') n++;
			}      // ↑1.23e-2 の場合, '-' で分離しないようにするための措置
			//				setToken(TK_NUM, Text+bgn, n-bgn);
			PDValue *val = PDValue::createAsString(Text+bgn, n-bgn);
			newArr->addObject(val);
		} else if (isAlpha(Text[n])) {                              // 識別子, 予約語
			for (bgn = n++; n < nText && isAlphaNumeric(Text[n]); ) n++;
			//				setToken(TK_NAME, Text+bgn, n-bgn);
			PDValue *val = PDValue::createAsString(Text+bgn, n-bgn);
			newArr->addObject(val);
		} else {                                                    // 記号
			//				setToken(TK_SYMBOL, Text+n, 1);
			PDValue *val = PDValue::createAsString( Text+n, 1 );
			newArr->addObject(val);
			n++;
		}
	}

	PDDictionary *dic = PDDictionary::create();
	int cnt = newArr->count() / 4;
	for (int i=0; i<cnt; i++) {
		if (i*4 >= newArr->count()) {
			break;
		}
		
		PDValue *key = (PDValue*)newArr->objectAtIndex(i+0);
		PDValue *eq = (PDValue*)newArr->objectAtIndex(i+1);
		PDValue *val = (PDValue*)newArr->objectAtIndex(i+2);
		PDValue *cr = (PDValue*)newArr->objectAtIndex(i+3);

		// Format error check.
		if (eq->stringValue()!="=") {
			CCLOG("WARN: Localizable.settings format error / need \"=\". - key:%s", key->stringValue().c_str());
			break;
		}
		if (cr->stringValue()!=";") {
			CCLOG("WARN: Localizable.settings format error / need \";\" - key:%s", key->stringValue().c_str());
			break;
		}

		dic->setObject(key->stringValue(), val);
	}
	
	return dic;
}

std::string ResourceModel::localizedString(const std::string &key)
{
	if (localizedStringMap_ == NULL) {
		// load localized strings.
		std::string path = ResourceModel::localFilePath("Localizable.strings");
		PDDictionary *strings = loadLocalizedStrings( path );
		
		set_localizedStringMap( strings );
	}
	std::string val = localizedStringMap_->valueAsString(key);
	return val;
}

//static
cocos2d::Size ResourceModel::getDesignResolution(ENUM_DESIGN_RESOLUTION res)
{
	cocos2d::Size ret;
	switch (res) {
		case kDesignResolution_DEFAULT: ret = getInstance()->get_defaultDesignResolution(); break;
		case kDesignResolution_320x480: ret = Size(320, 480); break;
		case kDesignResolution_480x320: ret = Size(480, 320); break;
		case kDesignResolution_640x960: ret = Size(640, 960); break;
		case kDesignResolution_960x640: ret = Size(960, 640); break;
	}
	return ret;
}

//static
void ResourceModel::autoLayout(cocos2d::Node *target, ENUM_AUTOLAYOUT_TYPE type, ENUM_DESIGN_RESOLUTION res)
{
	Size designSize = getDesignResolution( res );
	Size winSize = Director::getInstance()->getWinSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	Point layoutPos = target->getPosition();
	
//	float dx = origin.x;	//(winSize.width - designSize.width)/2;
//	float dy = origin.y;	//(winSize.height - designSize.height)/2;
	float dx = (winSize.width - designSize.width)/2;
	float dy = (winSize.height - designSize.height)/2;
	
	switch (type) {
		case kAutoLayout_TOP_LEFT:
		case kAutoLayout_MIDDLE_LEFT:
		case kAutoLayout_BOTTOM_LEFT:
//			layoutPos.x += dx;
			layoutPos.x -= dx;
			break;
		case kAutoLayout_TOP_RIGHT:
		case kAutoLayout_MIDDLE_RIGHT:
		case kAutoLayout_BOTTOM_RIGHT:
//			layoutPos.x -= dx;
			layoutPos.x += dx;
			break;
		default:
			break;
	}
	
	switch (type) {
		case kAutoLayout_TOP_LEFT:
		case kAutoLayout_TOP_CENTER:
		case kAutoLayout_TOP_RIGHT:
//			layoutPos.y -= dy;
			layoutPos.y += dy;
			break;
		case kAutoLayout_BOTTOM_LEFT:
		case kAutoLayout_BOTTOM_CENTER:
		case kAutoLayout_BOTTOM_RIGHT:
//			layoutPos.y += dy;
			layoutPos.y -= dy;
			break;
		default:
			break;
	}
	
	target->setPosition(layoutPos);
}

//static
cocos2d::Rect ResourceModel::calcStretch(cocos2d::Rect srcRect, ENUM_STRETCH_TYPE type, ENUM_DESIGN_RESOLUTION res)
{
	Size designSize = getDesignResolution( res );
	Size winSize = Director::getInstance()->getWinSize();
}

//static
void ResourceModel::setToScreenCenter(cocos2d::Node *target)
{
//	Rect rc = target->getBoundingBox();
	Point oldAnchor = target->getAnchorPoint();
	Size winSize = Director::getInstance()->getWinSize();
	target->setAnchorPoint(Point(0.5, 0.5));
	target->setPosition(Point(winSize.width/2, winSize.height/2));
//	target->setAnchorPoint(oldAnchor);
}

std::string CLocalizedString(const std::string &key)
{
	std::string val = ResourceModel::getInstance()->localizedString(key);
	return key;
}
