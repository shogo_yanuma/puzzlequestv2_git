//
//  MainDataModel.cpp
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/05.
//
//

#include "MainDataModel.h"

USING_NS_CC;

MainDataModel *MainDataModel::g_instance = NULL;

MainDataModel *MainDataModel::getInstance()
{
	if (g_instance==NULL) {
		g_instance = MainDataModel::create();
		g_instance->retain();
	}
	return g_instance;
}

bool MainDataModel::init()
{
	return true;
}
