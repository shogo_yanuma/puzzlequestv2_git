//
//  ResourceModel.h
//  FlConv
//
//  Created by shogo yanuma on 2014/05/11.
//
//

#ifndef __Urayama__ResourceModel__
#define __Urayama__ResourceModel__

#include "cocos2d.h"
#include "PDDictionary.h"

typedef enum {
	kLabelFontType_UNKNOWN	= 0,
	kLabelFontType_DEFAULT,
	kLabelFontType_BOLD,
	kLabelFontType_DEFAULT_BMFONT,
	kLabelFontType_SMALL_BMFONT,
} ENUM_LABEL_FONT_TYPE;

class ResourceModel : public cocos2d::Ref
{
public:
	typedef enum {
		kDesignResolution_DEFAULT = 0,
		kDesignResolution_320x480,
		kDesignResolution_480x320,
		kDesignResolution_640x960,
		kDesignResolution_960x640,
	} ENUM_DESIGN_RESOLUTION;
	
	typedef enum {
		kAutoLayout_TOP_LEFT,
		kAutoLayout_TOP_CENTER,
		kAutoLayout_TOP_RIGHT,
		kAutoLayout_MIDDLE_LEFT,
		kAutoLayout_MIDDLE_CENTER,
		kAutoLayout_MIDDLE_RIGHT,
		kAutoLayout_BOTTOM_LEFT,
		kAutoLayout_BOTTOM_CENTER,
		kAutoLayout_BOTTOM_RIGHT,
	} ENUM_AUTOLAYOUT_TYPE;
	
	typedef enum {
		kStretch_KEEP_TOP,
		kStretch_KEEP_BOTTOM,
		kStretch_KEEP_LEFT,
		kStretch_KEEP_RIGHT,
		kStretch_KEEP_TOPBOTTOM,
		kStretch_KEEP_LEFTRIGHT,
		kStretch_KEEP_ALL,
	} ENUM_STRETCH_TYPE;
	
private:
	static ResourceModel *g_instance;
	static std::string g_appDir;
	static std::string g_resourceDir;
	static std::string g_docDir;
	static std::string g_cacheDir;
	static std::string g_tempDir;

	CC_SYNTHESIZE_RETAIN(PDDictionary*, localizedStringMap_, _localizedStringMap);

	CC_SYNTHESIZE(cocos2d::Size, defaultDesignResolution_, _defaultDesignResolution);

public:
	static ResourceModel *getInstance();
	
public:
	CREATE_FUNC(ResourceModel);
	
	ResourceModel();
	virtual ~ResourceModel();
	virtual bool init();

	static std::string appDir();
	static std::string resourceDir();
	static std::string docDir();
	static std::string cacheDir();
	static std::string tempDir();
	
	static std::string readFromFile(const std::string &path);
	static void writeToFile(const std::string &path, const std::string &data);
	
	static std::string localFilePath(const std::string &path);

	static std::string labelFontName(ENUM_LABEL_FONT_TYPE type);
	static cocos2d::Label *defaultLabelTTF(const std::string &str, float fontSize);
	static cocos2d::Label *boldLabelTTF(const std::string &str, float fontSize);

	static cocos2d::Label *defaultLabelBMP(const std::string &str);
	static cocos2d::Label *smallLabelBMP(const std::string &str);

	PDDictionary *loadLocalizedStrings(const std::string &path);
	std::string localizedString(const std::string &key);

	static void setDefaultDesignResolution(cocos2d::Size size) {
		getInstance()->set_defaultDesignResolution(size);
	}
	static cocos2d::Size getDesignResolution(ENUM_DESIGN_RESOLUTION res);
	static void autoLayout(cocos2d::Node *target, ENUM_AUTOLAYOUT_TYPE type, ENUM_DESIGN_RESOLUTION res=kDesignResolution_DEFAULT);
	static cocos2d::Rect calcStretch(cocos2d::Rect srcRect, ENUM_STRETCH_TYPE type, ENUM_DESIGN_RESOLUTION res=kDesignResolution_DEFAULT);
	static void setToScreenCenter(cocos2d::Node *target);
	
};

std::string CLocalizedString(const std::string &key);

#define kAutoLayout_TL (ResourceModel::kAutoLayout_TOP_LEFT)
#define kAutoLayout_TC (ResourceModel::kAutoLayout_TOP_CENTER)
#define kAutoLayout_TR (ResourceModel::kAutoLayout_TOP_RIGHT)
#define kAutoLayout_ML (ResourceModel::kAutoLayout_MIDDLE_LEFT)
#define kAutoLayout_MC (ResourceModel::kAutoLayout_MIDDLE_CENTER)
#define kAutoLayout_MR (ResourceModel::kAutoLayout_TOP_RIGHT)
#define kAutoLayout_BL (ResourceModel::kAutoLayout_BOTTOM_LEFT)
#define kAutoLayout_BC (ResourceModel::kAutoLayout_BOTTOM_CENTER)
#define kAutoLayout_BR (ResourceModel::kAutoLayout_BOTTOM_RIGHT)

#endif /* defined(__Urayama__ResourceModel__) */
