//
//  ServiceModel.cpp
//  PuzzleQuest
//
//  Created by shogo yanuma on 2014/08/12.
//
//

#include "ServiceModel.h"
#include "CocosUtil.h"
#include "CNativeBridge.h"

#pragma mark - ServiceRequestInfoVO
//static
ServiceRequestInfoVO *ServiceRequestInfoVO::createWith(const std::string &url, PDDictionary *param, Invocation *invocSuccess, Invocation *invocFail)
{
	ServiceRequestInfoVO *vo = ServiceRequestInfoVO::create();
	vo->set_url(url);
	vo->set_param(param);
	vo->set_invocSuccess(invocSuccess);
	vo->set_invocFail(invocFail);
	return vo;
}

//static
ServiceRequestInfoVO *ServiceRequestInfoVO::createWith(const std::string &url, PDDictionary *param, std::function<void(ServiceRequestInfoVO *vo)> funcSuccess, std::function<void(ServiceRequestInfoVO *vo)> funcFail)
{
	ServiceRequestInfoVO *vo = ServiceRequestInfoVO::create();
	vo->set_url(url);
	vo->set_param(param);
	vo->set_funcSuccess(funcSuccess);
	vo->set_funcFail(funcFail);
	return vo;
}


#pragma mark - ServiceModel
ServiceModel *ServiceModel::g_instance = NULL;
ServiceModel *ServiceModel::getInstance()
{
	if (g_instance == NULL) {
		g_instance = ServiceModel::create();
		g_instance->retain();
	}
	return g_instance;
}

//static
std::string ServiceModel::httpURL(const std::string &path)
{
	std::string ret = "http://http://localhost/~shogo/penguindream/pq/rpc/";
}

//static
std::string ServiceModel::httpsURL(const std::string &path)
{
	std::string ret = "https://";
}

ServiceModel::ServiceModel()
{
	currentTag_ = 0;
	requestList_ = NULL;
}

ServiceModel::~ServiceModel()
{
	set_requestList(NULL);

	if (this == g_instance) {
		g_instance = NULL;
	}
}

bool ServiceModel::init()
{
	set_requestList( PDArray::create() );
	return true;
}

int ServiceModel::getNextTag()
{
	if (currentTag_ >= INT_MAX) {
		currentTag_ = 0;
	}
	currentTag_ ++;
	return currentTag_;
}

ServiceRequestInfoVO *ServiceModel::request(ServiceRequestInfoVO *req)
{
	int newTag = getNextTag();
	req->set_tag( newTag );
	requestList_->addObject(req);

	// Request

	Json::Value val(Json::objectValue);
	//	val.AddMember("tag", req->get_tag());
	
	val["tag"] = req->get_tag();
	val["url"] = req->get_url();
	val["param"] = CocosUtil::cocosObjectToJsonString(req->get_param());

//	Json::FastWriter writer;
//	std::string test = writer.write(val);
//	CCLOG("test = %s", test.c_str());
	
	std::string param = CocosUtil::jsonValueToJsonString(val);
//	// parm = { "tag":int, "url":"http://xxx.com/test.php", param:"{"param1":, "param2":, ...}" }
//	
	CNativeBridge::getInstance()->callDeviceFunc(kDeviceFunc_URL_REQUEST, param);
	
	return req;
}

ServiceRequestInfoVO *ServiceModel::requestURL(const std::string &url, PDDictionary *param, cocos2d::Ref *target, Invocation *successInvoc, Invocation *failInvoc)
{
	ServiceRequestInfoVO *req = ServiceRequestInfoVO::create();
	req->set_tag( getNextTag() );
	req->set_url(url);
	req->set_param(param);
	req->set_target(target);
	req->set_invocSuccess(successInvoc);
	req->set_invocFail(failInvoc);
	request(req);
	return req;
}

void ServiceModel::cancelWithRequest(ServiceRequestInfoVO *req)
{
	req->retain();
	requestList_->removeObject(req);
	req->autorelease();
}

void ServiceModel::cancelWithTarget(cocos2d::Ref *target)
{
//	ServiceRequestInfoVO *vo = findRequestWithTarget(target);
//	cancelWithRequest(vo);
	PDArray *arr = findRequestWithTarget(target);
	for (int i=0; i<arr->count(); i++) {
		ServiceRequestInfoVO *vo = (ServiceRequestInfoVO*)arr->objectAtIndex(i);
		cancelWithRequest(vo);
	}
}

ServiceRequestInfoVO *ServiceModel::findRequestWithTag(int tag)
{
	PDArray *arr = requestList_;
	for (int i=0; i<arr->count(); i++) {
		ServiceRequestInfoVO *vo = (ServiceRequestInfoVO*)arr->objectAtIndex(i);
		if (vo->get_tag() == tag) {
			return vo;
		}
	}
	return NULL;
}

// @return Array of ServiceRequestInfoVO
PDArray *ServiceModel::findRequestWithTarget(cocos2d::Ref* target)
//ServiceRequestInfoVO *ServiceModel::findRequestWithTarget(cocos2d::Ref* target)
{
	PDArray *ret = PDArray::create();
	PDArray *arr = requestList_;
	for (int i=0; i<arr->count(); i++) {
		ServiceRequestInfoVO *vo = (ServiceRequestInfoVO*)arr->objectAtIndex(i);
		if (vo->get_target() == target) {
			ret->addObject(vo);
//			return vo;
		}
	}
	return ret;
}

void ServiceModel::onSuccessRequest(int tag, const std::string &response)
{
	CCLOG("onSuccessRequest - tag:%d", tag);

	ServiceRequestInfoVO *vo = findRequestWithTag(tag);
	if (vo == NULL) {
		return;
	}

	vo->set_success(true);
	vo->set_rawResponse(response);
	
	PDDictionary *res = (PDDictionary*)CocosUtil::jsonStringToCocosObject(response);
	vo->set_response(res);

	if (vo->get_funcSuccess() != nullptr) {
		vo->get_funcSuccess()(vo);
	}
	else {
		if (vo->get_invocSuccess())
		{
			vo->get_invocSuccess()->invoke(vo);
		}
	}
	
	vo->retain();
	requestList_->removeObject(vo);
	vo->autorelease();
}

void ServiceModel::onFailRequest(int tag, const std::string &response)
{
	CCLOG("onFailRequest - tag:%d", tag);

	ServiceRequestInfoVO *vo = findRequestWithTag(tag);
	if (vo == NULL) {
		return;
	}

	vo->set_success(false);
	vo->set_rawResponse(response);
	
	PDDictionary *res = (PDDictionary*)CocosUtil::jsonStringToCocosObject(response);
	vo->set_response(res);

	if (vo->get_funcFail() != nullptr) {
		vo->get_funcFail()(vo);
	}
	else {
		if (vo->get_invocFail())
		{
			vo->get_invocFail()->invoke(vo);
		}
	}
	
	vo->retain();
	requestList_->removeObject(vo);
	vo->autorelease();
}
