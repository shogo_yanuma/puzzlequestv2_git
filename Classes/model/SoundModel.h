//
//  SoundModel.h
//  PuzzleQuest
//
//  Created by shogo yanuma on 2014/08/16.
//
//

#ifndef __PuzzleQuest__SoundModel__
#define __PuzzleQuest__SoundModel__

#include "cocos2d.h"

#define kSEID_TAP			"sound/se/select10.mp3"
#define kSEID_OK			"sound/se/poka01.mp3"
#define kSEID_CANCEL		"sound/se/cutting_wind1.mp3"
#define kSEID_PANEL_SWAP	"sound/se/poka01.mp3"
#define kSEID_PIECE_DELETE	"sound/se/select10.mp3"
#define kSEID_POWERUP		"sound/se/powerup05.mp3"

#define kSEID_COMBO_1		"sound/se/combo_1.mp3"
#define kSEID_COMBO_2		"sound/se/combo_2.mp3"
#define kSEID_COMBO_3		"sound/se/combo_3.mp3"
#define kSEID_COMBO_4		"sound/se/combo_4.mp3"
#define kSEID_COMBO_5		"sound/se/combo_5.mp3"
#define kSEID_COMBO_6		"sound/se/combo_6.mp3"
#define kSEID_COMBO_7		"sound/se/combo_7.mp3"
#define kSEID_COMBO_8		"sound/se/combo_8.mp3"
#define kSEID_COMBO_9		"sound/se/combo_9.mp3"
#define kSEID_COMBO_10		"sound/se/combo_10.mp3"


#define kBGMID_TITLE		"sound/bgm/1-01 Vivid.mp3"
#define kBGMID_HOME			"sound/bgm/2-18 Beatniks a GO GO.mp3"	//"sound/bgm/1-10 Erendira.mp3"
#define kBGMID_SHOP			"sound/bgm/1-14 Oval Of Cassini.mp3"
#define kBGMID_STAGELIST	"sound/bgm/1-04 Queen Charlotte.mp3"
#define kBGMID_BATTLE		"sound/bgm/1-03 1 Plus 1 Equal 1.mp3"
#define kBGMID_LAST_BATTLE	"sound/bgm/2-13 3.14159265.mp3"


class SoundModel : public cocos2d::Ref
{
private:
	static SoundModel *g_instance;
public:
	static SoundModel *getInstance();
	
public:
	CC_SYNTHESIZE(std::string, lastBGMID_, _lastBGMID);
	CC_SYNTHESIZE(float, seVolume_, _seVolume);
	CC_SYNTHESIZE(float, bgmVolume_, _bgmVolume);
	
public:
	CREATE_FUNC(SoundModel);
	
	SoundModel() {
		seVolume_ = 1.0;
		bgmVolume_ = 1.0;
	}
	virtual ~SoundModel() {
		if (this == g_instance) {
			g_instance = NULL;
		}
	}
	virtual bool init();

	void setSEVolume(float vol);
	float getSEVolume();
	void setBGMVolume(float vol);
	float getBGMVolume();

	void preloadSE(const std::string &seID);
	void preloadBGM(const std::string &bgmID);
	
	void playSE(const std::string &seID);
	void stopSE();
	void playBGM(const std::string &bgmID, bool forcePlay=false, bool loop=true);
	void stopBGM();
};

#endif /* defined(__PuzzleQuest__SoundModel__) */
