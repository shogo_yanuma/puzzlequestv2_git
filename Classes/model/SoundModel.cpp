//
//  SoundModel.cpp
//  PuzzleQuest
//
//  Created by shogo yanuma on 2014/08/16.
//
//

#include "SoundModel.h"
#include "ResourceModel.h"
#include "audio/include/SimpleAudioEngine.h"

USING_NS_CC;
using namespace CocosDenshion;

SoundModel *SoundModel::g_instance = NULL;

SoundModel *SoundModel::getInstance()
{
	if (g_instance==NULL) {
		g_instance = SoundModel::create();
		g_instance->retain();
	}
	return g_instance;
}

bool SoundModel::init()
{
	if (1) {
		preloadSE(kSEID_TAP);
		preloadSE(kSEID_OK);
		preloadSE(kSEID_CANCEL);

		preloadSE(kSEID_COMBO_1);
		preloadSE(kSEID_COMBO_2);
		preloadSE(kSEID_COMBO_3);
		preloadSE(kSEID_COMBO_4);
		preloadSE(kSEID_COMBO_5);
		preloadSE(kSEID_COMBO_6);
		preloadSE(kSEID_COMBO_7);
		preloadSE(kSEID_COMBO_8);
		preloadSE(kSEID_COMBO_9);
		preloadSE(kSEID_COMBO_10);

		preloadSE(kSEID_POWERUP);
		
		preloadBGM(kBGMID_TITLE);
		preloadBGM(kBGMID_HOME);
		preloadBGM(kBGMID_SHOP);
	}
	
	return true;
}

void SoundModel::setSEVolume(float vol)
{
	seVolume_ = vol;
	SimpleAudioEngine::getInstance()->setEffectsVolume(vol);
}

float SoundModel::getSEVolume()
{
	return seVolume_;
}

void SoundModel::setBGMVolume(float vol)
{
	bgmVolume_ = vol;
	SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(vol);
}

float SoundModel::getBGMVolume()
{
	return bgmVolume_;
}

void SoundModel::preloadSE(const std::string &seID)
{
	std::string path = ResourceModel::localFilePath(seID);
	SimpleAudioEngine::getInstance()->preloadEffect(path.c_str());
}

void SoundModel::preloadBGM(const std::string &bgmID)
{
	std::string path = ResourceModel::localFilePath(bgmID);
	SimpleAudioEngine::getInstance()->preloadBackgroundMusic(path.c_str());
}

void SoundModel::playSE(const std::string &seID)
{
	std::string path = ResourceModel::localFilePath(seID);
	SimpleAudioEngine::getInstance()->playEffect(path.c_str());
}

void SoundModel::stopSE()
{
	SimpleAudioEngine::getInstance()->stopAllEffects();
}

void SoundModel::playBGM(const std::string &bgmID, bool forcePlay, bool loop)
{
	if (!forcePlay) {
		if (lastBGMID_ == bgmID) {
			return;
		}
	}
	
	std::string path = ResourceModel::localFilePath(bgmID);
	SimpleAudioEngine::getInstance()->playBackgroundMusic(path.c_str(), loop);

	lastBGMID_ = bgmID;
}

void SoundModel::stopBGM()
{
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();

	lastBGMID_ = "";
}
