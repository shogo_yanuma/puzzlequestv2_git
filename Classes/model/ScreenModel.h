//
//  ScreenModel.h
//  PuzzleQuest
//
//  Created by shogo yanuma on 2016/04/08.
//
//

#ifndef ScreenModel_h
#define ScreenModel_h

#include "PopupBase.h"
#include "OverlayerBase.h"
#include "BlockerForPopup.h"
#include "BlockerForLoading.h"

USING_NS_CC;

class PopupInfoVO : public cocos2d::Ref
{
public:
	CC_SYNTHESIZE(bool, animated_, _animated);
	CC_SYNTHESIZE_RETAIN(PopupBase*, popup_, _popup);
	
public:
	CREATE_FUNC(PopupInfoVO);
	
	PopupInfoVO() {
		popup_ = NULL;
	}
	virtual ~PopupInfoVO() {
		set_popup(NULL);
	}
	virtual bool init() { return true; }
};

class ScreenModel : public Ref
{
public:
	CC_SYNTHESIZE(cocos2d::Vector<PopupInfoVO*>, popupList_, _popupList);
	
	CC_SYNTHESIZE_RETAIN(cocos2d::Node*, popupLayer_, _popupLayer);
	CC_SYNTHESIZE_RETAIN(BlockerForPopup*, blockerForPopup_, _blockerForPopup);
	CC_SYNTHESIZE_RETAIN(BlockerForLoading*, blockerForLoading_, _blockerForLoading);
	
	CC_SYNTHESIZE_RETAIN(cocos2d::Node*, overlayerLayer_, _overlayerLayer);

public:
	static ScreenModel *getInstance();
	
	ScreenModel() {
		popupLayer_ = NULL;
		blockerForPopup_ = NULL;
		blockerForLoading_ = NULL;
		overlayerLayer_ = NULL;
	}
	virtual ~ScreenModel() {
		set_popupLayer(NULL);
		set_blockerForPopup(NULL);
		set_blockerForLoading(NULL);
		set_overlayerLayer(NULL);
		if (this == g_instance) { g_instance=NULL; }
	}

	virtual bool init();
	
	void dispTitle();
	void dispHome();
	void dispBattle();

	void dispScene(cocos2d::Scene *scene, bool animated);
	void pushScene(cocos2d::Scene *scene);
	void popScene();

	void showPopup(PopupBase *popup, bool animated=true);
	void closePopup(PopupBase *popup, bool animated=true);
	void _showNextPopup();
	void _allPopupFinished();
	void _showBlockerForPopup();
	void _hideBlockerForPopup();
	
	void showLoadingBlocker(const std::string &msg);
	void hideLoadingBlocker();
	
	void showOverlayer(OverlayerBase *overlayer, bool animated=true);
	void closeOverlayer(OverlayerBase *overlayer, bool animated=true);
	void closeAllOverlayer();

private:
	static ScreenModel *g_instance;
};

#endif /* ScreenModel_h */
