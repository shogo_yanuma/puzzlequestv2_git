//
//  MainDataModel.h
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/05.
//
//

#ifndef MainDataModel_h
#define MainDataModel_h

#include "cocos2d.h"

#include "UserInfoVO.h"
#include "CardInfoVO.h"
#include "UserRankTableVO.h"
#include "CardLevelTableVO.h"

class MainDataModel : public cocos2d::Ref
{
private:
	static MainDataModel *g_instance;
public:
	static MainDataModel *getInstance();

public:
	CC_SYNTHESIZE_RETAIN(UserInfoVO*, myUserInfo_, _myUserInfo);
	CC_SYNTHESIZE(cocos2d::Vector<CardInfoVO*>, cardInfoList_, _cardInfoList);
	CC_SYNTHESIZE(cocos2d::Vector<UserRankTableVO*>, userRankTable_, _userRankTable);
	CC_SYNTHESIZE(cocos2d::Vector<CardLevelTableVO*>, cardLevelTable_, _cardLevelTable);
	
public:
	CREATE_FUNC(MainDataModel);
	
	MainDataModel() {
		myUserInfo_ = NULL;
	}
	virtual ~MainDataModel() {
		if (this == g_instance) {
			g_instance = NULL;
		}
		set_myUserInfo(NULL);
	}
	virtual bool init();
};

#endif /* MainDataModel_h */
