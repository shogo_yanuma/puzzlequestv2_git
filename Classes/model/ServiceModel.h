//
//  ServiceModel.h
//  PuzzleQuest
//
//  Created by shogo yanuma on 2014/08/12.
//
//

#ifndef __PuzzleQuest__ServiceModel__
#define __PuzzleQuest__ServiceModel__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "PDDictionary.h"

USING_NS_CC;
USING_NS_CC_EXT;

#pragma mark - ServiceRequestInfoVO
class ServiceRequestInfoVO : public cocos2d::Ref
{
public:
	
public:
	CC_SYNTHESIZE(int, tag_, _tag);
	CC_SYNTHESIZE_RETAIN(cocos2d::Ref*, target_, _target);

	CC_SYNTHESIZE(std::string, url_, _url);
	CC_SYNTHESIZE_RETAIN(PDDictionary*, param_, _param);

	CC_SYNTHESIZE(bool, success_, _success);
	CC_SYNTHESIZE(std::string, rawResponse_, _rawResponse);
	CC_SYNTHESIZE_RETAIN(PDDictionary*, response_, _response);
	
	CC_SYNTHESIZE_RETAIN(Invocation*, invocSuccess_, _invocSuccess);
	CC_SYNTHESIZE_RETAIN(Invocation*, invocFail_, _invocFail);

	CC_SYNTHESIZE(std::function<void(ServiceRequestInfoVO *vo)>, funcSuccess_, _funcSuccess);
	CC_SYNTHESIZE(std::function<void(ServiceRequestInfoVO *vo)>, funcFail_, _funcFail);
	
	
public:
	CREATE_FUNC(ServiceRequestInfoVO);

	static ServiceRequestInfoVO *createWith(const std::string &url, PDDictionary *param, Invocation *invocSuccess, Invocation *invocFail);
	static ServiceRequestInfoVO *createWith(const std::string &url, PDDictionary *param, std::function<void(ServiceRequestInfoVO *vo)> funcSuccess, std::function<void(ServiceRequestInfoVO *vo)> funcFail);
	
	ServiceRequestInfoVO() {
		target_ = NULL;
		param_ = NULL;
		response_ = NULL;
		invocSuccess_ = NULL;
		invocFail_ = NULL;
	}
	virtual ~ServiceRequestInfoVO() {
		set_target(NULL);
		set_param(NULL);
		set_response(NULL);
		set_invocSuccess(NULL);
		set_invocFail(NULL);
	}
	virtual bool init() { return true; }
};


#pragma mark - ServiceModel
class ServiceModel : public cocos2d::Ref
{
private:
	static ServiceModel *g_instance;
public:
	static ServiceModel *getInstance();

public:
	CC_SYNTHESIZE(int, currentTag_, _currentTag);
	CC_SYNTHESIZE_RETAIN(PDArray*, requestList_, _requestList);	// Array of ServiceRequestInfoVO
	
//	CC_SYNTHESIZE_RETAIN(ServiceRequestInfoVO*, currentRequest_, _currentRequest);
	
public:
	CREATE_FUNC(ServiceModel);

	static std::string httpURL(const std::string &path);
	static std::string httpsURL(const std::string &path);
	
	ServiceModel();
	virtual ~ServiceModel();
	virtual bool init();

	int getNextTag();
	ServiceRequestInfoVO *request(ServiceRequestInfoVO *req);
	ServiceRequestInfoVO *requestURL(const std::string &url, PDDictionary *param, cocos2d::Ref *target, Invocation *successInvoc, Invocation *failInvoc);
	void cancelWithRequest(ServiceRequestInfoVO *req);
	void cancelWithTarget(cocos2d::Ref *target);

	ServiceRequestInfoVO *findRequestWithTag(int tag);
//	ServiceRequestInfoVO *findRequestWithTarget(cocos2d::Ref* target);
	// @return Array of ServiceRequestInfoVO
	PDArray *findRequestWithTarget(cocos2d::Ref* target);
	
	void onSuccessRequest(int tag, const std::string &response);
	void onFailRequest(int tag, const std::string &response);
};

#endif /* defined(__PuzzleQuest__ServiceModel__) */
