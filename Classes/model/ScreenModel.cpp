//
//  ScreenModel.cpp
//  PuzzleQuest
//
//  Created by shogo yanuma on 2016/04/08.
//
//

#include "ScreenModel.h"
#include "TitleLayer.h"
#include "HomeLayer.h"
#include "BattleLayer.h"

ScreenModel *ScreenModel::g_instance = NULL;

ScreenModel *ScreenModel::getInstance()
{
	if (g_instance==NULL) {
		g_instance = new ScreenModel();
		g_instance->init();
	}
	return g_instance;
}

bool ScreenModel::init()
{
	Node *n;
	n = Node::create();
	set_overlayerLayer(n);
	
	n = Node::create();
	set_popupLayer(n);
	return true;
}

void ScreenModel::dispTitle()
{
	auto layer = TitleLayer::create();
	auto scene = Scene::create();
	scene->addChild(layer);
//	Director::getInstance()->replaceScene(scene);
	dispScene(scene, true);
}

void ScreenModel::dispHome()
{
	auto layer = HomeLayer::create();
	auto scene = Scene::create();
	scene->addChild(layer);
//	Director::getInstance()->replaceScene(scene);
	dispScene(scene, true);
}

void ScreenModel::dispBattle()
{
	auto layer = BattleLayer::create();
	auto scene = Scene::create();
	scene->addChild(layer);
//	Director::getInstance()->replaceScene(scene);
	dispScene(scene, true);
}

void ScreenModel::dispScene(cocos2d::Scene *scene, bool animated)
{
	closeAllOverlayer();
	
	if (overlayerLayer_->getParent()) {
		overlayerLayer_->removeFromParent();
	}
	scene->addChild(overlayerLayer_);
	
	if (popupLayer_->getParent()) {
		popupLayer_->removeFromParent();
	}
	scene->addChild(popupLayer_);
	
	if (animated) {
		TransitionFade *transition = TransitionFade::create(0.3, scene);
		Director::getInstance()->replaceScene(transition);
	}
	else {
		Director::getInstance()->replaceScene(scene);
	}
}

void ScreenModel::pushScene(cocos2d::Scene *scene)
{
	Director::getInstance()->pushScene(scene);
	
	if (overlayerLayer_->getParent()) {
		overlayerLayer_->removeFromParent();
	}
	scene->addChild(overlayerLayer_);
	
	if (popupLayer_->getParent()) {
		popupLayer_->removeFromParent();
	}
	scene->addChild(popupLayer_);
}

void ScreenModel::popScene()
{
	cocos2d::Scene *oldScene = Director::getInstance()->getRunningScene();
	
	oldScene->retain();
	
	Director::getInstance()->popScene();
	
	cocos2d::Scene *scene = Director::getInstance()->getRunningScene();
	
	if (scene != NULL) {
		if (overlayerLayer_->getParent()) {
			overlayerLayer_->removeFromParent();
		}
		scene->addChild(overlayerLayer_);
		
		if (popupLayer_->getParent()) {
			popupLayer_->removeFromParent();
		}
		scene->addChild(popupLayer_);
	}
	
	oldScene->autorelease();
}

void ScreenModel::showPopup(PopupBase *popup, bool animated)
{
	PopupInfoVO *vo = PopupInfoVO::create();
	vo->set_popup(popup);
	vo->set_animated(animated);
	
	popupList_.pushBack(vo);
	
	if (popupList_.size() == 1) {
		_showNextPopup();
	}
}

void ScreenModel::closePopup(PopupBase *popup, bool animated)
{
	popup->retain();
	popup->popupWillDisappear();
	
	for (int i=0; i<popupList_.size(); i++) {
		PopupInfoVO *vo = popupList_.at(i);
		if (vo->get_popup() == popup) {
			popupList_.eraseObject(vo);
		}
	}
	
	//TODO: アニメーションしながら消す
	
	popup->removeFromParent();
	
	popup->popupDidDisappear();
	popup->autorelease();
	
	//TODO: 消え終わったら次のポップアップを表示
	_showNextPopup();
}

void ScreenModel::_showNextPopup()
{
	int cnt = popupList_.size();
	if (cnt == 0) {
		_allPopupFinished();
		return;
	}
	
	// 暗くするバック表示
	_showBlockerForPopup();
	
	PopupInfoVO *nextVO = popupList_.at(0);
	PopupBase *popup = nextVO->get_popup();
	popup->popupWillAppear();
	
	Size winSize = Director::getInstance()->getWinSize();
	Point pos;
	pos.x = winSize.width/2 - popup->get_centerPos().x;
	pos.y = winSize.height/2 - popup->get_centerPos().y;
	popup->setPosition(pos);
	popupLayer_->addChild( popup );
	
	// アニメーションしながら表示
	
	popup->popupDidAppear();
}

void ScreenModel::_allPopupFinished()
{
	// 全てのポップアップが終了
	_hideBlockerForPopup();
	
	//stopTimerForBringPopupLayerToTop();
}

void ScreenModel::_showBlockerForPopup()
{
	if (blockerForPopup_) {
		return;
	}
	
	BlockerForPopup *blocker = BlockerForPopup::create();
	popupLayer_->addChild(blocker);
	set_blockerForPopup(blocker);
}

void ScreenModel::_hideBlockerForPopup()
{
	if (blockerForPopup_) {
		blockerForPopup_->removeFromParent();
		set_blockerForPopup(NULL);
	}
}

void ScreenModel::showLoadingBlocker(const std::string &msg)
{
	if (blockerForLoading_) {
		return;
	}
	
	BlockerForLoading *blocker = BlockerForLoading::create();
	blocker->setMessage( msg );
	popupLayer_->addChild(blocker);
	set_blockerForLoading(blocker);
}

void ScreenModel::hideLoadingBlocker()
{
	if (blockerForLoading_) {
		blockerForLoading_->removeFromParent();
		set_blockerForLoading(NULL);
	}
}

void ScreenModel::showOverlayer(OverlayerBase *overlayer, bool animated)
{
	//TODO: アニメーションしながら表示
	
	overlayerLayer_->addChild(overlayer);
	
}

void ScreenModel::closeOverlayer(OverlayerBase *overlayer, bool animated)
{
	//TODO: アニメーションしながら表示
	
	overlayer->removeFromParent();
	
}

void ScreenModel::closeAllOverlayer()
{
	auto& children = overlayerLayer_->getChildren();
	for(const auto &obj : children) {
		OverlayerBase *overlayer = (OverlayerBase*)obj;
		overlayer->closeOverlayer(false, true);
	}
}
