//
//  PDArray.cpp
//  Urayama
//
//  Created by shogo yanuma on 16/02/14.
//
//

#include "PDArray.h"

USING_NS_CC;

PDArray::PDArray()
{
}

PDArray::~PDArray()
{
}

bool PDArray::init()
{
	return true;
}

int PDArray::count()
{
	return values_.size();
}

void PDArray::addObject(cocos2d::Ref *obj)
{
	CCASSERT(obj!=NULL, "add obj in PDArray must be non-null.");
	obj->retain();

	values_.pushBack(obj);
}

void PDArray::addObjects(PDArray *objects)
{
	for (int i=0; i<objects->count(); i++) {
		Ref *obj = objects->objectAtIndex(i);
		addObject(obj);
	}
}

void PDArray::removeObject(cocos2d::Ref *obj)
{
	Vector<Ref*>::iterator it = values_.find(obj);
	if (it == values_.end()) {
		return;
	}

	obj->autorelease();
	values_.erase(it);
}

void PDArray::removeAllObjects()
{
	for (int i=0; i<values_.size(); i++) {
		Ref *obj = values_.at(i);
		obj->autorelease();
	}
	values_.clear();
}

cocos2d::Ref *PDArray::objectAtIndex(int index)
{
	cocos2d::Ref *ret = values_.at(index);
	return ret;
}

void PDArray::insertObjectAt(cocos2d::Ref *obj, int index)
{
	CCASSERT(obj!=NULL, "insert obj in PDArray must be non-null.");
	obj->retain();
	values_.insert(index, obj);
}

//PDValue *PDArray::valueAtIndex(int index)
cocos2d::Ref *PDArray::valueAtIndex(int index)
{
	Ref *obj = objectAtIndex(index);
	if (obj == NULL) {
		return NULL;
	}
	return obj;
	
//	const std::type_info &info = typeid(*obj);
//	if (info != typeid(PDValue)) {
//		return NULL;
//	}
//	return (PDValue*) obj;
}

void PDArray::reverseObjects()
{
	values_.reverse();
}

cocos2d::Ref *PDArray::firstObject()
{
	Ref *ret = objectAtIndex(0);
	return ret;
}

cocos2d::Ref *PDArray::lastObject()
{
	Ref *ret = objectAtIndex(count()-1);
	return ret;
}

