//
//  PDDictionary.cpp
//  Urayama
//
//  Created by shogo yanuma on 16/02/14.
//
//

#include "PDDictionary.h"
#include "PDValue.h"

USING_NS_CC;

PDDictionary::PDDictionary()
{
}

PDDictionary::~PDDictionary()
{
}

bool PDDictionary::init()
{
	return true;
}

std::vector<std::string> PDDictionary::allKeys()
{
	std::vector<std::string> ret = values_.keys();
	return ret;
}

cocos2d::Ref *PDDictionary::objectForKey(const std::string &key) const
{
	if (values_.find(key) == values_.end()) {
		return NULL;
	}
	cocos2d::Ref *ret = values_.at(key);
	return ret;
}

void PDDictionary::setObject(const std::string &key, cocos2d::Ref *obj)
{
	CCASSERT(obj!=NULL, "PDDictionary obj must non-null.");

	Ref *chkObj = objectForKey(key);
	if (chkObj!=NULL) {
		removeObjectWithKey(key);
	}

	obj->retain();
	values_.insert(key, obj);
}

void PDDictionary::removeObject(cocos2d::Ref *obj)
{
	for (Map<std::string, Ref*>::iterator it=values_.begin(); it!=values_.end(); it++) {
		Ref *chkObj = it->second;
		if (chkObj == obj) {
			values_.erase(it);
			break;
		}
	}
}

void PDDictionary::removeObjectWithKey(const std::string &key)
{
	Ref *obj = values_.at(key);
	if (obj == nullptr) {
		return;
	}

	obj->autorelease();
	values_.erase(key);
}

void PDDictionary::removeAllObjects()
{
	for (Map<std::string, Ref*>::iterator it=values_.begin(); it!=values_.end(); ) {
		Ref *obj = it->second;
		obj->autorelease();
		it = values_.erase(it);
	}
}

bool PDDictionary::valueAsBool(const std::string &key) const
{
	const PDValue *val = (PDValue*)objectForKey(key);
	if (val == NULL) {
		return false;
	}
	return val->boolValue();
}

std::string PDDictionary::valueAsString(const std::string &key) const
{
	const PDValue *val = (PDValue*)objectForKey(key);
	if (val == NULL) {
		return "";
	}
	return val->stringValue();
}

int PDDictionary::valueAsInt(const std::string &key) const
{
	PDValue *val = (PDValue*)objectForKey(key);
	if (val == NULL) {
		return 0;
	}
	return val->intValue();
}

float PDDictionary::valueAsFloat(const std::string &key) const
{
	PDValue *val = (PDValue*)objectForKey(key);
	if (val == NULL) {
		return 0.f;
	}
	return val->floatValue();
}

PDArray *PDDictionary::valueAsArray(const std::string &key) const
{
	PDArray *val = (PDArray*)objectForKey(key);
	return val;
}

PDDictionary *PDDictionary::valueAsDictionary(const std::string &key) const
{
	PDDictionary *val = (PDDictionary*)objectForKey(key);
	return val;
}

