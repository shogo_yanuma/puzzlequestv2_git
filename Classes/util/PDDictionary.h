//
//  PDDictionary.h
//  Urayama
//
//  Created by shogo yanuma on 16/02/14.
//
//

#ifndef __Urayama__PDDictionary__
#define __Urayama__PDDictionary__

#include "cocos2d.h"
#include "PDArray.h"
#include "PDDictionary.h"

class PDDictionary : public cocos2d::Ref
{
public:
	CREATE_FUNC(PDDictionary);
	
	PDDictionary();
	virtual ~PDDictionary();
	virtual bool init();

	std::vector<std::string> allKeys();
	
	cocos2d::Ref *objectForKey(const std::string &key) const;
	void setObject(const std::string &key, cocos2d::Ref *obj);
	void removeObject(cocos2d::Ref *obj);
	void removeObjectWithKey(const std::string &key);
	void removeAllObjects();

	bool valueAsBool(const std::string &key) const;
	std::string valueAsString(const std::string &key) const;
	int valueAsInt(const std::string &key) const;
	float valueAsFloat(const std::string &key) const;
	PDArray *valueAsArray(const std::string &key) const;
	PDDictionary *valueAsDictionary(const std::string &key) const;
	
private:
	cocos2d::Map<std::string, cocos2d::Ref*> values_;
};

#endif /* defined(__Urayama__PDDictionary__) */
