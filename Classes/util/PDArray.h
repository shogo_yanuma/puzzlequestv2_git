//
//  PDArray.h
//  Urayama
//
//  Created by shogo yanuma on 16/02/14.
//
//

#ifndef __Urayama__PDArray__
#define __Urayama__PDArray__

#include "cocos2d.h"
#include "PDValue.h"

class PDArray : public cocos2d::Ref
{
public:
	CREATE_FUNC(PDArray);

	PDArray();
	virtual ~PDArray();
	virtual bool init();

	int count();
	
	void addObject(cocos2d::Ref *obj);
	void addObjects(PDArray *objects);
	void removeObject(cocos2d::Ref *obj);
	void removeAllObjects();
	cocos2d::Ref *objectAtIndex(int index);
	void insertObjectAt(cocos2d::Ref *obj, int index);
	
//	PDValue *valueAtIndex(int index);
	cocos2d::Ref *valueAtIndex(int index);

	void reverseObjects();
	cocos2d::Ref *firstObject();
	cocos2d::Ref *lastObject();
	
private:
	cocos2d::Vector<cocos2d::Ref*> values_;
};

#endif /* defined(__Urayama__PDArray__) */
