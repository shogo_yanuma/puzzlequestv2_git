//
//  PDData.cpp
//  FlConv
//
//  Created by yanuma shogo on 2014/05/20.
//
//

#include "PDData.h"

#define PDDATA_DEFAULT_BUF_SIZE	1024

PDData::PDData()
{
	buf_ = NULL;
	currentBufSize_ = 0;
	maxBufSize_ = 0;
	currentPos_ = 0;
}

PDData::~PDData()
{
	resizeBuffer(0);
}

bool PDData::init()
{
	resizeBuffer(PDDATA_DEFAULT_BUF_SIZE);
	return true;
}

void PDData::resizeBuffer(int size)
{
	int oldSize = currentBufSize_;
	PDDATA_BUF_TYPE *oldBuf = buf_;

	int newSize = size;
	PDDATA_BUF_TYPE *newBuf = NULL;

	if (newSize > 0) {
		newBuf = new PDDATA_BUF_TYPE[newSize];

		if (oldBuf!=NULL && oldSize>0) {
			
			int copySize = MAX(oldSize, newSize);
			memcpy(newBuf, oldBuf, sizeof(PDDATA_BUF_TYPE)*copySize);
		}
	}

	maxBufSize_ = newSize;
	buf_ = newBuf;
}

void PDData::seek(int pos)
{
	currentPos_ = pos;
}

bool writeAsInt(PDDATA_INT val);
bool writeAsLong(PDDATA_LONG val);
bool writeAsFloat(PDDATA_FLOAT val);
bool writeAsString(const std::string &val);

PDDATA_INT readAsInt();
PDDATA_LONG readAsLong();
PDDATA_FLOAT readAsFloat();
std::string readAsString();
