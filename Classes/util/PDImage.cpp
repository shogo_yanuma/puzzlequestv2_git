//
//  PDImage.cpp
//  FlConv
//
//  Created by shogo yanuma on 2014/05/23.
//
//

#include "PDImage.h"
#include "CocosUtil.h"

USING_NS_CC;

//static
PDImage *PDImage::createWith(int width, int height)
{
	PDImage *img = PDImage::create();

	int bytesPerComponent = 4;
	ssize_t dataLen = width * height * bytesPerComponent;
	unsigned char *buf = (unsigned char *)malloc( dataLen );
	memset(buf, 0x00, dataLen);	// 0x00 clear.
	
	// RGBA8888
	bool res = img->initWithRawData(buf, dataLen, width, height, bytesPerComponent);

	free( buf );
	
	if (!res) {
		img = NULL;
	}
	return img;
}

PDImage::PDImage()
{
}

PDImage::~PDImage()
{
}

bool PDImage::init()
{
	return true;
}

cocos2d::Color4B PDImage::getPixel(int x, int y)
{
	Color4B ret = CocosUtil::getPixel(this, x, y);
	return ret;
}

void PDImage::setPixel(int x, int y, const cocos2d::Color4B &col)
{
	CocosUtil::setPixel(this, x, y, col);
}


void PDImage::drawImage(cocos2d::Image *srcImg, const cocos2d::Point &destPos)
{
	int w = srcImg->getWidth();
	int h = srcImg->getHeight();
	for (int y=0; y<h; y++) {
		for (int x=0; x<w; x++) {
			Color4B col = CocosUtil::getPixel(srcImg, x, y);
			CocosUtil::setPixel( this, destPos.x+x, destPos.y+y, col );
		}
	}
}

void PDImage::drawImage(cocos2d::Image *srcImg, const cocos2d::Point &destPos, const cocos2d::Rect srcRect)
{
	int w = srcImg->getWidth();
	if (w > srcRect.size.width) {
		w = srcRect.size.width;
	}
	int h = srcImg->getHeight();
	if (h > srcRect.size.height) {
		h = srcRect.size.height;
	}

	for (int y=0; y<h; y++) {
		for (int x=0; x<w; x++) {
			Color4B col = CocosUtil::getPixel( srcImg, srcRect.origin.x+x, srcRect.origin.y+y );
			CocosUtil::setPixel( this, destPos.x+x, destPos.y+y, col );
		}
	}
}

PDImage *PDImage::trimSpace()
{
	float maxWidth = 0;
	float maxHeight = 0;

	int w = getWidth();
	int h = getHeight();
	
	for (int y=0; y<h; y++) {
		for (int x=w-1; x>=0; x--) {
			Color4B col = getPixel(x, y);
			if (col.a == 0) {
				continue;
			}

			if (maxWidth > x) {
				maxWidth = x;
			}
			break;
		}
	}

	for (int x=0; x<w; x++) {
		for (int y=h-1; y>=0; y--) {
			Color4B col = getPixel(x, y);
			if (col.a == 0) {
				continue;
			}
			
			if (maxHeight > y) {
				maxHeight = y;
			}
			break;
		}
	}
	
	if (maxWidth < 4) {
		maxWidth = 4;
	}
	if (maxHeight < 4) {
		maxHeight = 4;
	}

	CCLOG("trimSpace - trim size:(%f, %f)", maxWidth, maxHeight);
	
	PDImage *ret = PDImage::createWith(maxWidth, maxHeight);
	ret->drawImage(this, Point(0, 0));
	return ret;
}
