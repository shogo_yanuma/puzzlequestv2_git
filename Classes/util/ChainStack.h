//
//  ChainStack.h
//  PuzzleQuest
//
//  Created by shogo yanuma on 2014/09/13.
//
//

#ifndef __PuzzleQuest__ChainStack__
#define __PuzzleQuest__ChainStack__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "PDArray.h"

USING_NS_CC;
USING_NS_CC_EXT;

#pragma mark - ChainStackInfoVO
class ChainStackInfoVO : public cocos2d::Ref
{
public:
	CC_SYNTHESIZE(std::string, anchorKey_, _anchorKey);
	CC_SYNTHESIZE_RETAIN(Invocation*, invoc_, _invoc);
	CC_SYNTHESIZE_RETAIN(cocos2d::Ref*, params_, _params);
	
public:
	CREATE_FUNC(ChainStackInfoVO);
	
	ChainStackInfoVO() {
		invoc_ = NULL;
		params_ = NULL;
	}
	virtual ~ChainStackInfoVO() {
		set_invoc(NULL);
		set_params(NULL);
	}
	virtual bool init() { return true; }
};


#pragma mark - ChainStack
class ChainStack : public cocos2d::Ref
{
public:
	CC_SYNTHESIZE(bool, isCanceled_, _isCanceled);
	CC_SYNTHESIZE_RETAIN(PDArray*, stacks_, _stacks);
	CC_SYNTHESIZE_RETAIN(Invocation*, invocComplete_, _invocComplete);
	CC_SYNTHESIZE_RETAIN(Invocation*, invocCancel_, _invocCancel);
	CC_SYNTHESIZE_RETAIN(cocos2d::Ref*, userInfo_, _userInfo);
	
	CC_SYNTHESIZE(int, currentIndex_, _currentIndex);
	CC_SYNTHESIZE_RETAIN(cocos2d::Ref*, currentParams_, _currentParams);

	CC_SYNTHESIZE(int, cancelCode_, _cancelCode);

public:
	CREATE_FUNC(ChainStack);

	ChainStack() {
		CCLOG("ChainStack::alloc - %p", this);
		stacks_ = NULL;
		invocComplete_ = NULL;
		invocCancel_ = NULL;
		userInfo_ = NULL;
		currentParams_ = NULL;
	}
	virtual ~ChainStack() {
		CCLOG("ChainStack::dealloc - %p", this);
		set_stacks(NULL);
		set_invocComplete(NULL);
		set_invocCancel(NULL);
		set_userInfo(NULL);
		set_currentParams(NULL);
	}
	virtual bool init();

	void addStack(Invocation *invoc, cocos2d::Ref *params=NULL);
	void addWait(float sec);
	void addAnchor(const std::string &key);

	void insertStackAfter(int index, Invocation *invoc, cocos2d::Ref *params=NULL);
	void insertWaitAfter(int index, float sec);
	void insertAnchorAfter(int index, const std::string &key);
	
	void start(Invocation *invocComplete=NULL, Invocation *invocCancel=NULL);
	
	void doNext();
	void skipUntilAnchor(const std::string &key);
	void cancel(int cancelCode=-1);

private:
	void _doWait(cocos2d::Ref *sender, Control::EventType ev);
	void _onTimer_wait(float dt);
	void _onTimer_wait_doNext(float dt);

	void _doneAllStack();
};

#define INVOC(target, sel) cocos2d::extension::Invocation::create(target, cccontrol_selector(sel), cocos2d::extension::Control::EventType::VALUE_CHANGED)

#endif /* defined(__PuzzleQuest__ChainStack__) */
