//
//  PDImage.h
//  FlConv
//
//  Created by shogo yanuma on 2014/05/23.
//
//

#ifndef __FlConv__PDImage__
#define __FlConv__PDImage__

#include "cocos2d.h"

class PDImage : public cocos2d::Image
{
public:
	static PDImage *createWith(int width, int height);

public:
	CREATE_FUNC(PDImage);
	
	PDImage();
	virtual ~PDImage();
	virtual bool init();

	cocos2d::Color4B getPixel(int x, int y);
	void setPixel(int x, int y, const cocos2d::Color4B &col);
	
	void drawImage(cocos2d::Image *srcImg, const cocos2d::Point &destPos);
	void drawImage(cocos2d::Image *srcImg, const cocos2d::Point &destPos, const cocos2d::Rect srcRect);

	PDImage *trimSpace();
};

#endif /* defined(__FlConv__PDImage__) */
