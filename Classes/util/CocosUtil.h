//
//  CocosUtil.h
//  FlConv
//
//  Created by shogo yanuma on 2014/04/27.
//
//

#ifndef __FlConv__CocosUtil__
#define __FlConv__CocosUtil__

#include "cocos2d.h"
#include "json.h"

#include "PDValue.h"
#include "PDArray.h"
#include "PDDictionary.h"

class CocosUtil : public cocos2d::Ref
{
public:
	
	static cocos2d::Ref *jsonStringToCocosObject(const std::string &str);
	static cocos2d::Ref *jsonValueToCocosObject(const Json::Value &val);
	static std::string cocosObjectToJsonString(const cocos2d::Ref *obj);
	static std::string jsonValueToJsonString(const Json::Value &val);
	static Json::Value cocosObjectToJsonValue(const cocos2d::Ref *obj);
	static Json::Value jsonStringToJsonValue(const std::string &str);
	
	static int hex2dec(const std::string &val);
	static std::string dec2hex(int val);
	static std::vector<std::string> split(const std::string& input, char delimiter);
	
	// @param col "RRGGBB" or "#RRGGBB"
	static cocos2d::Color3B hexStringToColor(const std::string &col);
	// @return "RRGGBB"
	static std::string colorToHexString(cocos2d::Color3B col);
	
	static std::string getExtensionFromPath(const std::string &path);
	// @return path="/path/to/readme.txt" -> ["/path/to/", "readme", "txt"]
	static std::vector<std::string> getPathComponents(const std::string &path);

	static cocos2d::Color4B getPixel(cocos2d::Image *img, int x, int y);
	static void setPixel(cocos2d::Image *img, int x, int y, const cocos2d::Color4B &col);
	
	static std::string stringFromFile(const std::string &fullpath);

	static std::string replaceString(const std::string &src, const std::string &from, const std::string &to);
	// @param filepath : ex) "path/to/file/[seq]/1_item@2x.png"
	// @param divUnit : ファイル分割の刻み幅
	static std::string makeSequencePath(const std::string &filepath, int divUnit=250);
};

#endif /* defined(__FlConv__CocosUtil__) */
