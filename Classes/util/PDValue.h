//
//  PDValue.h
//  Urayama
//
//  Created by shogo yanuma on 16/02/14.
//
//

#ifndef __Urayama__PDValue__
#define __Urayama__PDValue__

#include "cocos2d.h"

class PDValue : public cocos2d::Ref
{
public:
	typedef enum {
		kPDValueType_UNKNOWN	= 0,
		kPDValueType_NULL,
		kPDValueType_BOOL,
		kPDValueType_INT,
		kPDValueType_FLOAT,
		kPDValueType_STRING,
	} ENUM_PDValue_TYPE;

public:
	CREATE_FUNC(PDValue);

	static PDValue *createAsBool(bool v);
	static PDValue *createAsInt(long long v);
	static PDValue *createAsFloat(float v);
	static PDValue *createAsString(const std::string &v);
	static PDValue *createAsString(const char *buf, int length);
	
	PDValue() { type_ = kPDValueType_UNKNOWN; }
	PDValue(bool v) { setAsBool(v); }
	PDValue(long long v) { setAsInt(v); }
	PDValue(float v) { setAsFloat(v); }
	PDValue(const std::string &v) { setAsString(v); }
	virtual bool init() { return true; }
	
	ENUM_PDValue_TYPE getValueType() { return type_; }

	void setAsNull();
	void setAsBool(bool v);
	void setAsInt(long long v);
	void setAsFloat(float v);
	void setAsString(const std::string &v);

	bool boolValue() const;
	long long intValue() const;
	float floatValue() const;
	std::string stringValue() const;
	
protected:
	ENUM_PDValue_TYPE	type_;

	bool value_bool_;
	long long value_int_;
	float value_float_;
	std::string value_string_;
};

#endif /* defined(__Urayama__PDValue__) */
