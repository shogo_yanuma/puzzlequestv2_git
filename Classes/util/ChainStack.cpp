//
//  ChainStack.cpp
//  PuzzleQuest
//
//  Created by shogo yanuma on 2014/09/13.
//
//

#include "ChainStack.h"
#include "PDValue.h"

USING_NS_CC;

#pragma mark - ChainStack
bool ChainStack::init()
{
	
	set_stacks(PDArray::create());

	return true;
}

void ChainStack::addStack(Invocation *invoc, cocos2d::Ref *params)
{
	ChainStackInfoVO *vo = ChainStackInfoVO::create();
	vo->set_invoc(invoc);
	vo->set_params(params);
	stacks_->addObject(vo);
}

void ChainStack::addWait(float sec)
{
	PDValue *params = PDValue::createAsFloat(sec);
	addStack( Invocation::create(this, cccontrol_selector(ChainStack::_doWait), Control::EventType::VALUE_CHANGED), params);
}

void ChainStack::addAnchor(const std::string &key)
{
	ChainStackInfoVO *vo = ChainStackInfoVO::create();
	vo->set_anchorKey( key );
	stacks_->addObject(vo);
}

void ChainStack::insertStackAfter(int index, Invocation *invoc, cocos2d::Ref *params)
{
	ChainStackInfoVO *vo = ChainStackInfoVO::create();
	vo->set_invoc(invoc);
	vo->set_params(params);
	stacks_->insertObjectAt(vo, index+1);
}

void ChainStack::insertWaitAfter(int index, float sec)
{
	CCLOG("insertWaitAfter - index:%d, sec:%f", index, sec);
	PDValue *params = PDValue::createAsFloat(sec);
	insertStackAfter( index, Invocation::create(this, cccontrol_selector(ChainStack::_doWait), Control::EventType::VALUE_CHANGED), params );
}

void ChainStack::insertAnchorAfter(int index, const std::string &key)
{
	CCLOG("insertAnchorAfter - index:%d, key:%s", index, key.c_str());
	ChainStackInfoVO *vo = ChainStackInfoVO::create();
	vo->set_anchorKey( key );
	stacks_->insertObjectAt(vo, index+1);
}

void ChainStack::start(Invocation *invocComplete, Invocation *invocCancel)
{
	set_invocComplete(invocComplete);
	set_invocCancel(invocCancel);

	isCanceled_ = false;
	currentIndex_ = -1;
	doNext();
}

void ChainStack::doNext()
{
	CCLOG("doNext - %p", this);
	
	currentIndex_ ++;
	
	if (currentIndex_ >= stacks_->count()) {
		_doneAllStack();
		return;
	}

	ChainStackInfoVO *infoVO = (ChainStackInfoVO*)stacks_->objectAtIndex( currentIndex_ );

	if (infoVO->get_invoc() != NULL) {
		set_currentParams( infoVO->get_params() );
		infoVO->get_invoc()->invoke(this);
	}
	else {
		doNext();
	}
}

void ChainStack::skipUntilAnchor(const std::string &key)
{
	CCLOG("skipUntilAnchor - %p, key:%s", this, key.c_str());
	
	if (currentIndex_ >= stacks_->count()) {
		return;
	}

	for (int i=currentIndex_; i<stacks_->count(); i++) {
		ChainStackInfoVO *infoVO = (ChainStackInfoVO*)stacks_->objectAtIndex(i);
		if (infoVO->get_anchorKey() == key) {
			currentIndex_ = i;
			break;
		}
	}
}

void ChainStack::cancel(int cancelCode)
{
	CCLOG("cancel - %p, code:%d", this, cancelCode);
	
	set_cancelCode( cancelCode );
	
	isCanceled_ = true;
	currentIndex_ = stacks_->count();

	if (invocCancel_ != NULL) {
		invocCancel_->invoke(this);
	}
}

void ChainStack::_doWait(cocos2d::Ref *sender, Control::EventType ev)
{
	CCLOG("_doWait");
	ChainStack *chain = (ChainStack*)sender;

	float sec = ((PDValue*)chain->get_currentParams())->floatValue();
	
	Scheduler *scheduler = Director::getInstance()->getScheduler();
	scheduler->schedule(schedule_selector(ChainStack::_onTimer_wait), this, sec, 0, 0, false);
}

void ChainStack::_onTimer_wait(float dt)
{
	CCLOG("_onTimer_wait");
	Scheduler *scheduler = Director::getInstance()->getScheduler();
	scheduler->unschedule(schedule_selector(ChainStack::_onTimer_wait), this);
	scheduler->schedule(schedule_selector(ChainStack::_onTimer_wait_doNext), this, 0, 0, 0, false);
//	doNext();
}

void ChainStack::_onTimer_wait_doNext(float dt)
{
	Scheduler *scheduler = Director::getInstance()->getScheduler();
	scheduler->unschedule(schedule_selector(ChainStack::_onTimer_wait_doNext), this);
	doNext();
}

void ChainStack::_doneAllStack()
{
	CCLOG("_doneAllStack - %p", this);
	
	if (invocComplete_ != NULL) {
		invocComplete_->invoke(this);
	}
}
