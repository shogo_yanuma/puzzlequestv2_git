//
//  PDValue.cpp
//  Urayama
//
//  Created by shogo yanuma on 16/02/14.
//
//

#include "PDValue.h"

USING_NS_CC;

//static
PDValue *PDValue::createAsBool(bool v)
{
	PDValue *ret = PDValue::create();
	ret->setAsBool(v);
	return ret;
}

//static
PDValue *PDValue::createAsInt(long long v)
{
	PDValue *ret = PDValue::create();
	ret->setAsInt(v);
	return ret;
}

//static
PDValue *PDValue::createAsFloat(float v)
{
	PDValue *ret = PDValue::create();
	ret->setAsFloat(v);
	return ret;
}

//static
PDValue *PDValue::createAsString(const std::string &v)
{
	PDValue *ret = PDValue::create();
	ret->setAsString(v);
	return ret;
}

//static
PDValue *PDValue::createAsString(const char *buf, int length)
{
	PDValue *ret = PDValue::create();
	char *tmp = new char[length+1];
	strncpy(tmp, buf, length);
	tmp[length] = 0x00;
	std::string str = tmp;
	ret->setAsString(tmp);
	delete[] tmp;
	return ret;
}

void PDValue::setAsNull()
{
	type_ = kPDValueType_NULL;
}

void PDValue::setAsBool(bool v)
{
	type_ = kPDValueType_BOOL;
	value_bool_ = v;
}

void PDValue::setAsInt(long long v)
{
	type_ = kPDValueType_INT;
	value_int_ = v;
}

void PDValue::setAsFloat(float v)
{
	type_ = kPDValueType_FLOAT;
	value_float_ = v;
}

void PDValue::setAsString(const std::string &v)
{
	type_ = kPDValueType_STRING;
	value_string_ = v;
}

bool PDValue::boolValue() const
{
	bool res = false;
	switch (type_) {
		case kPDValueType_BOOL:
			res = value_bool_;
			break;
		case kPDValueType_INT:
			res = (value_int_ != 0);
			break;
		case kPDValueType_FLOAT:
			res = (value_float_ != 0);
			break;
		case kPDValueType_STRING:
			res = (atoi(value_string_.c_str()) != 0) || (value_string_=="true");
			break;
		default:
			break;
	}
	return res;
}

long long PDValue::intValue() const
{
	long long res = 0;
	switch (type_) {
		case kPDValueType_BOOL:
			res = value_bool_;
			break;
		case kPDValueType_INT:
			res = value_int_;
			break;
		case kPDValueType_FLOAT:
			res = value_float_;
			break;
		case kPDValueType_STRING:
			res = atoi(value_string_.c_str());
			break;
		default:
			break;
	}
	return res;
}

float PDValue::floatValue() const
{
	float res = 0;
	switch (type_) {
		case kPDValueType_BOOL:
			res = value_bool_;
			break;
		case kPDValueType_INT:
			res = value_int_;
			break;
		case kPDValueType_FLOAT:
			res = value_float_;
			break;
		case kPDValueType_STRING:
			res = atof(value_string_.c_str());
			break;
		default:
			break;
	}
	return res;
}

std::string PDValue::stringValue() const
{
	std::string res;
	switch (type_) {
		case kPDValueType_BOOL:
			res = StringUtils::format("%d", value_bool_);
			break;
		case kPDValueType_INT:
			res = StringUtils::format("%lld", value_int_);
			break;
		case kPDValueType_FLOAT:
			res = StringUtils::format("%f", value_float_);
			break;
		case kPDValueType_STRING:
			res = value_string_;
		default:
			break;
	}
	return res;
}
