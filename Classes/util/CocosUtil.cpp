//
//  CocosUtil.cpp
//  FlConv
//
//  Created by shogo yanuma on 2014/04/27.
//
//

#include "CocosUtil.h"
#include "PDValue.h"
#include "PDArray.h"
#include "PDDictionary.h"

USING_NS_CC;

//static
cocos2d::Ref *CocosUtil::jsonStringToCocosObject(const std::string &str)
{
	Json::Reader reader;
	Json::Value root;
	reader.parse(str, root);
	std::string errMsg = reader.getFormatedErrorMessages();
	bool hasError = (errMsg != "");
	
	Ref *ret = NULL;
	if (hasError) {
		// Json parse error.
		std::string msg = CCString::createWithFormat("WARN: json parse error. - json:\n%s", errMsg.c_str())->getCString();
#ifdef DEBUG
		CCASSERT(false, msg.c_str());
#else
		CCLOG( "%s", msg.c_str() );
#endif
	}
	else {
		ret = jsonValueToCocosObject(root);
	}
	
	return ret;
}

//static
cocos2d::Ref *CocosUtil::jsonValueToCocosObject(const Json::Value &val)
{
	if (val.isNull()) {
		return NULL;
	}

	Ref *ret = NULL;
	
	if (val.isArray()) {
		PDArray *arr = PDArray::create();
		for (int i=0; i<val.size(); i++) {
			const Json::Value &jsonVal = val[i];
			Ref *item = jsonValueToCocosObject( jsonVal );
			if (item != NULL) {
				arr->addObject( item );
			}
		}
		ret = arr;
	}
	else if (val.isObject()) {
		PDDictionary *dic = PDDictionary::create();
		Json::Value::Members keys = val.getMemberNames();
		for (int i=0; i<keys.size(); i++)
		{
			std::string key = keys[i];
			const Json::Value &jsonVal = val[key];
			Ref *item = jsonValueToCocosObject( jsonVal );

			if (item != NULL) {
				dic->setObject(key, item);
			}
		}
		ret = dic;
	}
	else if (val.isString()) {
		PDValue *v = PDValue::create();
		v->setAsString( val.asString() );
		ret = v;
	}
	else if (val.isBool()) {
		PDValue *v = PDValue::create();
		v->setAsBool( val.asBool() );
		ret = v;
	}
	else if (val.isInt()) {
		PDValue *v = PDValue::create();
		v->setAsInt( val.asInt() );
		ret = v;
	}
	else if (val.isDouble()) {
		PDValue *v = PDValue::create();
		v->setAsFloat( val.asDouble() );
		ret = v;
	}
	else if (val.isNull()) {
		// nop
	}
	else {		
		CCLOG("WARN: unknown JSON value type. - type:%d / val:%s", val.type(), val.asString().c_str());
	}
	
	return ret;
}

//static
std::string CocosUtil::cocosObjectToJsonString(const cocos2d::Ref *obj)
{
	Json::Value jsonVal = cocosObjectToJsonValue(obj);
	Json::FastWriter writer;
	std::string ret = writer.write( jsonVal );
	return ret;
}

//static
std::string CocosUtil::jsonValueToJsonString(const Json::Value &val)
{
	Json::FastWriter writer;
	std::string ret = writer.write(val);
	return ret;
}

//static
Json::Value CocosUtil::cocosObjectToJsonValue(const cocos2d::Ref *obj)
{
	Json::Value ret;

	if (obj == NULL) {
		ret = Json::Value(Json::nullValue);
		return ret;
	}
	
	const std::type_info &info = typeid(*obj);
	if (info == typeid(PDValue))
	{
		PDValue *val = (PDValue*)obj;

		switch (val->getValueType())
		{
			case PDValue::kPDValueType_NULL: {
				ret = Json::Value(Json::nullValue);
			}	break;

			case PDValue::kPDValueType_BOOL: {
				ret = val->boolValue();
			}	break;

			case PDValue::kPDValueType_INT: {
				ret = val->intValue();
			}	break;

			case PDValue::kPDValueType_FLOAT: {
				ret = val->floatValue();
			}	break;

			case PDValue::kPDValueType_STRING: {
				ret = val->stringValue();
			}	break;
				
			default:
				break;
		}
	}
	else if (info == typeid(PDArray))
	{
		PDArray *arr = (PDArray*)obj;
		Json::Value newArr(Json::arrayValue);
		for (int i=0; i<arr->count(); i++)
		{
			Ref *item = arr->objectAtIndex(i);
			Json::Value jsonVal = cocosObjectToJsonValue(item);
			newArr[i] = jsonVal;
		}
		ret = newArr;
	}
	else if (info == typeid(PDDictionary))
	{
		PDDictionary *dic = (PDDictionary*)obj;
		Json::Value newDic(Json::objectValue);
		
		std::vector<std::string> keys = dic->allKeys();
		for (int i=0; i<keys.size(); i++)
		{
			std::string key = keys[i];
			Ref *item = dic->objectForKey(key);
			Json::Value jsonVal = cocosObjectToJsonValue(item);
			newDic[key] = jsonVal;
		}
		ret = newDic;
	}
	
	return ret;
}

//static
Json::Value CocosUtil::jsonStringToJsonValue(const std::string &str)
{
	Json::Value ret;
	Json::Reader reader;
	reader.parse(str, ret);
	return ret;
}

//static
int CocosUtil::hex2dec(const std::string &val)
{
	int ret = strtol(val.c_str(), NULL, 16);
	return ret;
}

//static
std::string CocosUtil::dec2hex(int val)
{
	char buf[16];
	sprintf(buf, "%02x", val);
	std::string ret = buf;
	return ret;
}

//static
std::vector<std::string> CocosUtil::split(const std::string& input, char delimiter)
{
	std::istringstream stream(input);
	std::string field;
	std::vector<std::string> result;
	while (std::getline(stream, field, delimiter)) {
		result.push_back(field);
	}
	return result;
}

//static
cocos2d::Color3B CocosUtil::hexStringToColor(const std::string &col)
{
	std::string tmp = col;
	if (tmp.substr(0,1) == "#") {
		tmp = col.substr(1);
	}
	
	Color3B ret;
	std::string r = tmp.substr(0, 2);
	std::string g = tmp.substr(2, 2);
	std::string b = tmp.substr(4, 2);
	ret.r = CocosUtil::hex2dec(r);
	ret.g = CocosUtil::hex2dec(g);
	ret.b = CocosUtil::hex2dec(b);
	return ret;
}

//static
std::string CocosUtil::colorToHexString(cocos2d::Color3B col)
{
	std::string r = CocosUtil::dec2hex(col.r);
	std::string g = CocosUtil::dec2hex(col.g);
	std::string b = CocosUtil::dec2hex(col.b);
	std::string ret = r + g + b;
	return ret;
}

//static
std::string CocosUtil::getExtensionFromPath(const std::string &path)
{
	std::string ext;
    size_t pos1 = path.rfind('.');
    if(pos1 != std::string::npos){
        ext = path.substr(pos1+1, path.size()-pos1);
        std::string::iterator itr = ext.begin();
        while(itr != ext.end()){
            *itr = tolower(*itr);
            itr++;
        }
        itr = ext.end()-1;
        while(itr != ext.begin()){    // パスの最後に\0やスペースがあったときの対策
            if(*itr == 0 || *itr == 32){
                ext.erase(itr--);
            }
            else{
                itr--;
            }
        }
    }
	
    return ext;
}

// @return path="/path/to/readme.txt" -> ["/path/to/", "readme", "txt"]
//static
std::vector<std::string> CocosUtil::getPathComponents(const std::string &path)
{
	std::vector<std::string> ret;

//	 0123456789012345678
//	"/path/to/readme.txt"
	// 19
	size_t pos1 = path.rfind('.');	//15
	size_t pos2 = path.rfind('/');	//8
	
	std::string dir = path.substr(0, pos2+1);
	std::string file = path.substr(pos2+1, pos1-pos2-1);
	std::string ext = path.substr(pos1+1, path.size()-pos1);
	
	ret.push_back(dir);
	ret.push_back(file);
	ret.push_back(ext);
	
	return ret;
}

//static
cocos2d::Color4B CocosUtil::getPixel(cocos2d::Image *img, int x, int y)
{
	int w = img->getWidth();
	int h = img->getHeight();
	
	Color4B col;
	if (x < 0 || x >= w || y < 0 || y >= h) {
		CCLOG("WARN: getPixel out of range - pos:(%d,%d)", x, y);
		return col;
	}
	
	int bpp = img->getBitPerPixel() / 8;
	int idx = (y * w + x) * bpp;
	unsigned char *data = img->getData();
	
	Texture2D::PixelFormat fmt = img->getRenderFormat();
	switch (fmt) {
		case Texture2D::PixelFormat::BGRA8888:
			col.b = data[idx + 0];
			col.g = data[idx + 1];
			col.r = data[idx + 2];
			col.a = data[idx + 3];
			break;
		case Texture2D::PixelFormat::RGBA8888:
			col.r = data[idx + 0];
			col.g = data[idx + 1];
			col.b = data[idx + 2];
			col.a = data[idx + 3];
			break;
		case Texture2D::PixelFormat::RGB888:
			col.r = data[idx + 0];
			col.g = data[idx + 1];
			col.b = data[idx + 2];
			col.a = 0xff;
			break;
			
		default:
			break;
	}
	
	return col;
}

//static
void CocosUtil::setPixel(cocos2d::Image *img, int x, int y, const cocos2d::Color4B &col)
{
	int w = img->getWidth();
	int h = img->getHeight();
	
	if (x < 0 || x >= w || y < 0 || y >= h) {
		CCLOG("WARN: setPixel out of range - pos:(%d,%d)", x, y);
		return;
	}
	
	int bpp = img->getBitPerPixel() / 8;
	int idx = (y * w + x) * bpp;
	unsigned char *data = img->getData();
	
	Texture2D::PixelFormat fmt = img->getRenderFormat();
	switch (fmt) {
		case Texture2D::PixelFormat::BGRA8888:
			data[idx + 0] = col.b;
			data[idx + 1] = col.g;
			data[idx + 2] = col.r;
			data[idx + 3] = col.a;
			break;
		case Texture2D::PixelFormat::RGBA8888:
			data[idx + 0] = col.r;
			data[idx + 1] = col.g;
			data[idx + 2] = col.b;
			data[idx + 3] = col.a;
			break;
		case Texture2D::PixelFormat::RGB888:
			data[idx + 0] = col.r;
			data[idx + 1] = col.g;
			data[idx + 2] = col.b;
			break;
			
		default:
			break;
	}
}

//static
std::string CocosUtil::stringFromFile(const std::string &fullpath)
{
	Data dat = FileUtils::getInstance()->getDataFromFile( fullpath );
	
	int bufSize = dat.getSize();
	char *buf = new char[bufSize+1];
	memcpy(buf, dat.getBytes(), bufSize);
	buf[bufSize] = 0x00;

	std::string res = buf;
	delete[] buf;
	
	return res;
}

//static
std::string CocosUtil::replaceString(const std::string &src, const std::string &from, const std::string &to)
{
	std::string ret = src;
	std::string::size_type pos = ret.find( from );
	
	while( pos != std::string::npos ) {
		ret.replace( pos, from.length(), to );
		pos = ret.find( from, pos + to.length() );
	}
	return ret;
}

//static
std::string CocosUtil::makeSequencePath(const std::string &filepath, int divUnit)
{
	std::string ret;
	
	std::string::size_type idx = filepath.rfind("/");
	std::string path = filepath.substr(0, idx+1);
	std::string file = filepath.substr(idx+1);

	idx = file.find("_");
	std::string numStr = file.substr(0, idx);

	int num = atoi(numStr.c_str());
	if (num == 0) {
		num = 1;
	}

	int unit = ceilf((float)num/(float)divUnit);
	int seqNum = divUnit * unit;
	std::string seqNumStr = StringUtils::format("%d", seqNum);
	ret = replaceString(filepath, "[seq]", seqNumStr);
	return ret;
}
