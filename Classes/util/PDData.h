//
//  PDData.h
//  FlConv
//
//  Created by yanuma shogo on 2014/05/20.
//
//

#ifndef __FlConv__PDData__
#define __FlConv__PDData__

#include "cocos2d.h"

#define PDDATA_BUF_TYPE	char
#define PDDATA_INT		int
#define PDDATA_LONG		long long
#define PDDATA_FLOAT	float

class PDData : public cocos2d::Ref
{
protected:
	PDDATA_BUF_TYPE *buf_;
	int currentBufSize_;
	int maxBufSize_;
	int currentPos_;
	
public:
	CREATE_FUNC(PDData);

	PDData();
	virtual ~PDData();
	virtual bool init();

	inline int getMaxBufSize() { return maxBufSize_; }
	inline int getCurrentBufSize() { return currentBufSize_; }
	inline int getCurrentPos() { return currentPos_; }
	inline PDDATA_BUF_TYPE *getBuf() { return buf_; }
	
	void resizeBuffer(int size);
	void seek(int pos);

	bool writeAsInt(PDDATA_INT val);
	bool writeAsLong(PDDATA_LONG val);
	bool writeAsFloat(PDDATA_FLOAT val);
	bool writeAsString(const std::string &val);

	PDDATA_INT readAsInt();
	PDDATA_LONG readAsLong();
	PDDATA_FLOAT readAsFloat();
	std::string readAsString();
};

#endif /* defined(__FlConv__PDData__) */
