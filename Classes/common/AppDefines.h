//
//  AppDefines.h
//  PuzzleQuestV2
//
//  Created by shogo yanuma on 2016/05/11.
//
//

#ifndef AppDefines_h
#define AppDefines_h

typedef enum {
	kStepDir_UNKNOWN = 0,
	kStepDir_LEFT,
	kStepDir_DOWN,
	kStepDir_UP,
	kStepDir_RIGHT,
} ENUM_STEP_DIR;

typedef enum {
	kStepResult_UNKNOWN = 0,
	kStepResult_BAD,
	kStepResult_POOR,
	kStepResult_GOOD,
	kStepResult_GREAT,
	kStepResult_PERFECT,
} ENUM_STEP_RESULT;

#endif /* AppDefines_h */
