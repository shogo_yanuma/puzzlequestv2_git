//
//  NativeBridge_aos.h
//  PuzzleQuest
//
//  Created by shogo yanuma on 2016/04/11.
//
//


#ifndef __NativeBridge_aos__
#define __NativeBridge_aos__

#include <string>
#include "NativeBridgeDefines.h"

std::string __getDeviceData_AOS(ENUM_DEVICE_DATA_TYPE type, const std::string &param);
void __callDeviceFunc_AOS(ENUM_DEVICE_FUNC func, const std::string &param);

#endif /* defined(__NativeBridge_aos__) */
