//
//  OCServiceModel.h
//  PuzzleQuest
//
//  Created by shogo yanuma on 2014/10/18.
//
//

#import <Foundation/Foundation.h>

#pragma mark - OCServiceRequestInfoVO
@interface OCServiceRequestInfoVO : NSObject

@property (nonatomic, assign) int tag;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, retain) NSDictionary *param;
@property (nonatomic, retain) NSString *rawResponse;
@property (nonatomic, retain) NSDictionary *response;
@property (nonatomic, retain) NSURLConnection *urlConnection;

@property (nonatomic, retain) NSMutableData *receivedData;
@property (nonatomic, retain) NSError *error;

+ (OCServiceRequestInfoVO*)vo;

- (void)dealloc;
- (void)setData:(NSDictionary*)v;

@end


#pragma mark - OCServiceModel
@interface OCServiceModel : NSObject <NSURLConnectionDataDelegate>

@property (nonatomic, retain) NSMutableArray *requestList;	// Array of OCServiceRequestInfoVO

+ (OCServiceModel*)getInstance;

- (id)init;
- (void)dealloc;

- (void)request:(OCServiceRequestInfoVO*)req;
- (void)cancelRequest:(OCServiceRequestInfoVO*)req;
- (void)cancelRequestWithTag:(int)tag;

- (OCServiceRequestInfoVO*)requestVOWithConnection:(NSURLConnection*)conn;

- (void)onSuccessRequest:(OCServiceRequestInfoVO*)req;
- (void)onFailRequest:(OCServiceRequestInfoVO*)req;

#pragma mark - NSURLConnectionDelegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;

#pragma mark - NSURLConnectionDataDelegate
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response;
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data;
- (void)connectionDidFinishLoading:(NSURLConnection *)connection;

@end