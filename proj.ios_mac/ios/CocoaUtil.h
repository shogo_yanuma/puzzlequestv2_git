//
//  CocoaUtil.h
//  PuzzleQuest
//
//  Created by shogo yanuma on 2014/10/19.
//
//

#import <Foundation/Foundation.h>
#include "json.h"

@interface CocoaUtil : NSObject

+ (NSObject*)jsonValueToObject:(Json::Value)json;
+ (NSString*)urlEncoding:(NSString*)str;
+ (NSString*)urlDecoding:(NSString*)str;

@end

NSObject *jsonValueToNSObject(const Json::Value &val);
NSObject *jsonStringToNSObject(const std::string &val);