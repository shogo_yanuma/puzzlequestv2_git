//
//  NativeBridge_ios.h
//  FlConv
//
//  Created by shogo yanuma on 2014/04/27.
//
//


#ifndef __NativeBridge_ios__
#define __NativeBridge_ios__

#include "NativeBridgeDefines.h"

std::string __getDeviceData_iOS(ENUM_DEVICE_DATA_TYPE type, const std::string &param);
void __callDeviceFunc_iOS(ENUM_DEVICE_FUNC func, const std::string &param);

#endif /* defined(__NativeBridge_ios__) */
