//
//  CocoaUtil.m
//  PuzzleQuest
//
//  Created by shogo yanuma on 2014/10/19.
//
//

#import "CocoaUtil.h"

@implementation CocoaUtil

+ (NSObject*)jsonValueToObject:(Json::Value)json
{
	NSObject *ret = jsonValueToNSObject( json );
	return ret;
}

+ (NSString*)urlEncoding:(NSString*)str
{
	//TODO: URLエンコード
	
	return str;
}

+ (NSString*)urlDecoding:(NSString*)str
{
	//TODO: URLデコード

	return str;
}

@end

NSObject *jsonValueToNSObject(const Json::Value &val)
{
	NSObject *ret = nil;
	if (val.isArray()) {
		NSMutableArray *newArr = [NSMutableArray array];
		for (int i=0; i<val.size(); i++) {
			const Json::Value &item = val[i];
			NSObject *obj = jsonValueToNSObject( item );
			if (obj != nil) {
				[newArr addObject:obj];
			}
		}
		ret = newArr;
	}
	else if (val.isObject()) {
		NSMutableDictionary *newDic = [NSMutableDictionary dictionary];
		Json::Value::Members keys = val.getMemberNames();
		for (int i=0; i<keys.size(); i++) {
			std::string key = keys[i];
			const Json::Value &item = val[key];
			NSObject *obj = jsonValueToNSObject( item );
			if (obj != nil) {
				[newDic setObject:obj forKey:[NSString stringWithFormat:@"%s", key.c_str()]];
			}
		}
		ret = newDic;
	}
	else if (val.isBool()) {
		NSNumber *num = [NSNumber numberWithBool:val.asBool()];
		ret = num;
	}
	else if (val.isInt()) {
		NSNumber *num = [NSNumber numberWithInt:val.asInt()];
		ret = num;
	}
	else if (val.isDouble()) {
		NSNumber *num = [NSNumber numberWithDouble:val.asDouble()];
		ret = num;
	}
	else {
		ret = nil;
	}
	return ret;
}

NSObject *jsonStringToNSObject(const std::string &val)
{
	Json::Value jsonVal;
	Json::Reader reader;
	reader.parse(val, jsonVal);
	return jsonValueToNSObject( jsonVal );
}