//
//  OCServiceModel.mm
//  PuzzleQuest
//
//  Created by shogo yanuma on 2014/10/18.
//
//

#import "OCServiceModel.h"
#include "CNativeBridge.h"

#include "CocosUtil.h"
#import "CocoaUtil.h"

OCServiceModel *g_OCServiceModel_instance = nil;


#pragma mark - OCServiceRequestInfoVO
@implementation OCServiceRequestInfoVO

+ (OCServiceRequestInfoVO*)vo
{
	auto ret = [[OCServiceRequestInfoVO alloc] init];
	[ret autorelease];
	return ret;
}

- (void)dealloc
{
	self.url = nil;
	self.param = nil;
	self.rawResponse = nil;
	self.response = nil;
	self.urlConnection = nil;

	self.receivedData = nil;
	self.error = nil;
	[super dealloc];
}

- (void)setData:(NSDictionary*)v
{
	self.url = [v objectForKey:@"url"];
	self.param = [v objectForKey:@"param"];
}

@end


#pragma mark - OCServiceModel
@implementation OCServiceModel

+ (OCServiceModel*)getInstance
{
	if (g_OCServiceModel_instance == nil) {
		g_OCServiceModel_instance = [[OCServiceModel alloc] init];
	}
	return g_OCServiceModel_instance;
}

- (id)init
{
	self = [super init];
	if (self)
	{
		self.requestList = [NSMutableArray array];
	}
	return self;
}

- (void)dealloc
{
	self.requestList = nil;
	
	if (g_OCServiceModel_instance == self) {
		g_OCServiceModel_instance = nil;
	}
	[super dealloc];
}

- (void)request:(OCServiceRequestInfoVO*)req
{
	NSLog(@"request:");
	
	NSString *postString = @"";

	NSArray *keys = req.param.allKeys;
	for (int i=0; i<keys.count; i++) {
		NSString *key = (NSString*)[keys objectAtIndex:i];
		NSString *val = (NSString*)[req.param objectForKey:key];
		
//		CFStringRef encodedString = CFURLCreateStringByAddingPercentEscapes(
//																			kCFAllocatorDefault,
//																			(CFStringRef)val,
//																			NULL,
//																			CFSTR(":/?#[]@!$&'()*+,;="),
//																			kCFStringEncodingUTF8);
		NSString *encodedString = [CocoaUtil urlEncoding:val];
		
		if ([postString length] > 0) {
			postString = [postString stringByAppendingString:@"&"];
		}
		postString = [postString stringByAppendingFormat:@"%@=%@", key, encodedString];
	}
	
	
	NSURL *url = [NSURL URLWithString:req.url];
	NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
	urlRequest.HTTPMethod = @"POST";
	urlRequest.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
	urlRequest.timeoutInterval = 20;
	urlRequest.HTTPShouldHandleCookies = NO;
	urlRequest.HTTPBody = [postString dataUsingEncoding:NSUTF8StringEncoding];

	NSURLConnection *urlConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:NO];
	req.urlConnection = urlConnection;
	[self.requestList addObject:req];
	[urlConnection start];
}

- (void)cancelRequest:(OCServiceRequestInfoVO*)req
{
}

- (void)cancelRequestWithTag:(int)tag
{
}

- (OCServiceRequestInfoVO*)requestVOWithConnection:(NSURLConnection*)conn
{
	NSArray *arr = self.requestList;
	for (int i=0; i<arr.count; i++) {
		OCServiceRequestInfoVO *vo = (OCServiceRequestInfoVO*)[arr objectAtIndex:i];
		if (vo.urlConnection == conn) {
			return vo;
		}
	}

	return nil;
}

- (void)onSuccessRequest:(OCServiceRequestInfoVO*)req
{
	NSString *response = [[[NSString alloc] initWithData:req.receivedData encoding:NSUTF8StringEncoding] autorelease];
	NSLog(@"onSuccessRequest - url:%@, param:%@, response:%@", req.url, req.param, response);
	
	Json::Value val;
	val["tag"] = req.tag;
	val["success"] = true;
	val["url"] = [req.url cStringUsingEncoding:NSUTF8StringEncoding];

	val["response"] = [response cStringUsingEncoding:NSUTF8StringEncoding];

	std::string param = CocosUtil::jsonValueToJsonString( val );

	CNativeBridge::getInstance()->callCPPFunc( kCPPFunc_URL_RESPONSE, param );
}

- (void)onFailRequest:(OCServiceRequestInfoVO*)req
{
	NSString *response = [[[NSString alloc] initWithData:req.receivedData encoding:NSUTF8StringEncoding] autorelease];
	NSLog(@"onFailRequest - url:%@, param:%@, response:%@", req.url, req.param, response);
	
	Json::Value val;
	val["tag"] = req.tag;
	val["success"] = false;
	val["url"] = [req.url cStringUsingEncoding:NSUTF8StringEncoding];
	val["response"] = [response cStringUsingEncoding:NSUTF8StringEncoding];
	
	std::string param = CocosUtil::jsonValueToJsonString( val );
	
	CNativeBridge::getInstance()->callCPPFunc( kCPPFunc_URL_RESPONSE, param );
}


#pragma mark - NSURLConnectionDelegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	OCServiceRequestInfoVO *vo = [self requestVOWithConnection:connection];
	vo.error = error;
	[self onFailRequest:vo];
	
}

#pragma mark - NSURLConnectionDataDelegate
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	OCServiceRequestInfoVO *vo = [self requestVOWithConnection:connection];
	vo.receivedData = [NSMutableData data];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	OCServiceRequestInfoVO *vo = [self requestVOWithConnection:connection];
	[vo.receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	OCServiceRequestInfoVO *vo = [self requestVOWithConnection:connection];
	[self onSuccessRequest:vo];
}


@end
