//
//  NativeBridge_mac.m
//  FlConv
//
//  Created by shogo yanuma on 2014/04/27.
//
//

#import <Foundation/Foundation.h>

#import "NativeBridge_mac.h"
#include "CNativeBridge.h"


@interface NativeBridge_mac : NSObject

+ (NSString*)getDeviceData:(ENUM_DEVICE_DATA_TYPE)type param:(NSString*)param;
+ (void)callDeviceFunc:(ENUM_DEVICE_FUNC)func param:(NSString*)param;
+ (NSString*)getCPPData:(ENUM_CPP_DATA_TYPE)type param:(NSString*)param;
+ (void)callCPPFunc:(ENUM_CPP_FUNC)func param:(NSString*)param;

@end


@implementation NativeBridge_mac

+ (NSString*)getDeviceData:(ENUM_DEVICE_DATA_TYPE)type param:(NSString*)param
{
	NSString *ret = nil;
	
	NSData *jsonData = [param dataUsingEncoding:NSUTF8StringEncoding];
	NSError *err = nil;
	id jsonParam = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:&err];

	NSLog(@"NativeBridge_mac::getDeviceData - type:%d, param:%@", type, param);
	
	switch (type)
	{
		case kDeviceDataType_APP_DIR: {
//			ret = NSHomeDirectory();
			ret = [[NSBundle mainBundle] bundlePath];
		} break;

		case kDeviceDataType_RESOURCE_DIR: {
			ret = [[NSBundle mainBundle] resourcePath];
		} break;
			
		case kDeviceDataType_DOC_DIR: {
			NSArray *arr = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
			if ([arr count] >= 1) {
				ret = [arr objectAtIndex:0];
			}
		} break;

		case kDeviceDataType_CACHE_DIR: {
			NSArray *arr = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
			if ([arr count] >= 1) {
				ret = [arr objectAtIndex:0];
			}
		} break;

		case kDeviceDataType_TEMP_DIR: {
			ret = NSTemporaryDirectory();
		} break;
			
		case kDeviceDataType_GET_DIR_FILES: {
			NSString *path = [jsonParam objectForKey:@"path"];
			BOOL recursive = [[jsonParam objectForKey:@"recursive"] boolValue];

			NSArray *items = [self func_DIR_FILES:path recrusive:recursive];
			
			if ([NSJSONSerialization isValidJSONObject:items]) {
				NSError *err = nil;
				NSData *data = [NSJSONSerialization dataWithJSONObject:items options:NSJSONWritingPrettyPrinted error:&err];
				NSString *jsonStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
				ret = jsonStr;
			}
		} break;
			
		default: {
			// Unknown
		} break;
	}
	
	
	return ret;
}

+ (void)callDeviceFunc:(ENUM_DEVICE_FUNC)func param:(NSString*)param
{
	NSData *jsonData = [param dataUsingEncoding:NSUTF8StringEncoding];
	NSError *err = nil;
	id jsonParam = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:&err];
	
	switch (func)
	{
		case kDeviceFunc_URL_REQUEST: {
			
		} break;

		case kDeviceFunc_MAKE_DIR: {
		
		} break;

		case kDeviceFunc_SAVE_FILE: {
			
		} break;
			
		default:
			break;
	}
}

+ (NSString*)getCPPData:(ENUM_CPP_DATA_TYPE)type param:(NSString*)param
{
	NSString *ret = nil;
	std::string p = [param UTF8String];
	std::string str = CNativeBridge::getInstance()->getCPPData(type, p);
	
	ret = [NSString stringWithUTF8String:str.c_str()];
	return ret;
}

+ (void)callCPPFunc:(ENUM_CPP_FUNC)func param:(NSString*)param
{
	CNativeBridge::getInstance()->callCPPFunc(func, [param UTF8String]);
}

+ (NSArray*)func_DIR_FILES:(NSString*)path recrusive:(BOOL)recursive
{
	NSURL *url = [NSURL fileURLWithPath:path];

	NSArray *keys = [NSArray arrayWithObjects:
					 NSURLIsDirectoryKey, NSURLIsRegularFileKey, NSURLIsPackageKey, NSURLNameKey, NSURLLocalizedNameKey, nil];
	
	NSDirectoryEnumerator *enumerator = [[NSFileManager defaultManager]
										 enumeratorAtURL:url
										 includingPropertiesForKeys:keys
										 options:(NSDirectoryEnumerationSkipsSubdirectoryDescendants |
												  NSDirectoryEnumerationSkipsPackageDescendants |
												  NSDirectoryEnumerationSkipsHiddenFiles)
										 errorHandler:^(NSURL *url, NSError *error) {
											 // エラーを処理する。
											 // エラー後に列挙を続行する場合はYESを返す
											 return YES;
										 }];

	NSMutableArray *items = [NSMutableArray array];
	
	for (NSURL *url in enumerator) {
		NSNumber *isDirectory = nil;
		[url getResourceValue:&isDirectory forKey:NSURLIsDirectoryKey error:NULL];

		NSMutableDictionary *info = nil;
		
		if ([isDirectory boolValue]) {
			// directory
			NSString *localizedName = nil;
			[url getResourceValue:&localizedName forKey:NSURLLocalizedNameKey
							error:NULL];

			NSNumber *isPackage = nil;
			[url getResourceValue:&isPackage forKey:NSURLIsPackageKey error:NULL];

			info = [NSMutableDictionary dictionary];
			[info setValue:@"dir" forKey:@"type"];
			[info setValue:localizedName forKey:@"name"];
		}
		else {
			// file
			NSString *localizedName = nil;
			[url getResourceValue:&localizedName forKey:NSURLLocalizedNameKey
							error:NULL];
		
			info = [NSMutableDictionary dictionary];
			[info setValue:@"file" forKey:@"type"];
			[info setValue:localizedName forKey:@"name"];
		}

		if (info != nil) {
			[items addObject:info];
		}
	}

	return items;

	/*
	 NSArray *keys = [NSArray arrayWithObjects:
	 NSURLNameKey,
	 NSURLIsDirectoryKey,
	 NSURLCreationDateKey,
	 NSURLContentModificationDateKey,
	 nil];
	 NSDirectoryEnumerator *enumerator = [fm enumeratorAtURL:url
	 includingPropertiesForKeys:keys
	 options:NSDirectoryEnumerationSkipsHiddenFiles | NSDirectoryEnumerationSkipsSubdirectoryDescendants
	 errorHandler:nil];
	 
	 NSMutableArray *items = [NSMutableArray array];
	 
	 NSString *file = nil;
	 while (file = [enumerator nextObject])
	 {
	 NSDictionary *attrs = [enumerator fileAttributes];
	 //				NSString *fileType = [attrs objectForKey:NSFileType];
	 //NSURLIsDirectoryKey
	 
	 NSDictionary *info = nil;
	 
	 if ([fileType isEqualToString:NSFileTypeDirectory])
	 {
	 // directory
	 info = [NSMutableDictionary dictionary];
	 [info setValue:@"dir" forKey:@"type"];
	 [info setValue:file forKey:@"file"];
	 [info setValue:[attrs objectForKey:NSFileCreationDate] forKey:@"createtime"];
	 [info setValue:[attrs objectForKey:NSFileModificationDate] forKey:@"updatetime"];
	 }
	 else if ([fileType isEqualToString:NSFileTypeRegular])
	 {
	 // file
	 info = [NSMutableDictionary dictionary];
	 [info setValue:@"file" forKey:@"type"];
	 [info setValue:file forKey:@"file"];
	 [info setValue:[attrs objectForKey:NSFileSize] forKey:@"size"];
	 [info setValue:[attrs objectForKey:NSFileCreationDate] forKey:@"createtime"];
	 [info setValue:[attrs objectForKey:NSFileModificationDate] forKey:@"updatetime"];
	 }
	 else
	 {
	 NSLog(@"WARN: unknown file type - type:%@, file:%@, path:[%@]", fileType, file, path);
	 }
	 
	 if (info != nil) {
	 [items addObject:info];
	 }
	 }
	 */
}

std::string __getDeviceData_mac(ENUM_DEVICE_DATA_TYPE type, const std::string &param)
{
	std::string ret;
	
	NSString *p = [NSString stringWithUTF8String:param.c_str()];
	NSString *str = [NativeBridge_mac getDeviceData:type param:p];
	if (str != nil) {
		ret = [str UTF8String];
	}

	return ret;
}

void __callDeviceFunc_mac(ENUM_DEVICE_FUNC func, const std::string &param)
{
	NSString *p = [NSString stringWithUTF8String:param.c_str()];
	[NativeBridge_mac callDeviceFunc:func param:p];
}

@end
