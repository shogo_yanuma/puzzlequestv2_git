//
//  NativeBridge_mac.h
//  FlConv
//
//  Created by shogo yanuma on 2014/04/27.
//
//

#ifndef __NativeBridge_mac__
#define __NativeBridge_mac__

#include "NativeBridgeDefines.h"

std::string __getDeviceData_mac(ENUM_DEVICE_DATA_TYPE type, const std::string &param);
void __callDeviceFunc_mac(ENUM_DEVICE_FUNC func, const std::string &param);

#endif /* defined(__NativeBridge_mac__) */
